<?php

namespace pocketbarn\Listeners;
use JavaScript;
use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
class SubscribeToMailChimp
{
    private $mailchimp;

    public function __construct(\Mailchimp $mailchimp)
    {
      $this->mailchimp = $mailchimp;
    }

    public function handle(Registered $event)
    {

      // $this->mailchimp->lists->subscribe(
      //     env('MAILCHIMP_USER_LIST_ID'),
      //     ['email' => $event->user->email],
      //     ['FNAME' => $event->user->name],
      //     null,
      //     false
      // );

      try {
          $this->mailchimp
              ->lists
              ->subscribe(
                  env('MAILCHIMP_USER_LIST_ID'),
                  ['email' => $event->user->email],
                  ['FNAME' => $event->user->name],
                  null,
                  false
              );
        } catch (\Mailchimp_List_AlreadySubscribed $e) {
            // do something
        } catch (\Mailchimp_Error $e) {
            // do something
        }

    }
}
