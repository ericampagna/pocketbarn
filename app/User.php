<?php

namespace pocketbarn;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function store()
    {
        return $this->belongsTo('pocketbarn\Store');
    }

    public function conversations()
    {
        return $this->belongsToMany('pocketbarn\Conversation')->withPivot('unread')->latest();
    }

    public function messages()
    {
        return $this->hasMany('pocketbarn\Message', 'sender_id', 'id');
    }

    public function myPocketItems()
    {
        return $this->belongsToMany('pocketbarn\Item');
    }

    public function myPocketStores()
    {
        return $this->belongsToMany('pocketbarn\Store');
    }
}
