<?php

namespace pocketbarn;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;
class Store extends Model
{
	use Billable;

	// public function plans()
	// {
	// 	return $this->hasMany(Subscription::class)->orderby('created_at', 'desc');
	// }
    public function items()
    {
    	return $this->hasMany('pocketbarn\Item');
    }

    public function boostedItems()
    {
        return $this->hasManyThrough('pocketbarn\Item', 'pocketbarn\BoostedItem');
    }

    public function users()
    {
    	return $this->hasMany('pocketbarn\User');
    }

    public static function getPlan($store)
    {
        $plans = Plan::all();
        foreach($plans as $plan)
        {
            if($store->subscribedToPlan($plan->stripe_id, 'main') || $store->subscribedToPlan($plan->stripe_id.'_year', 'main'))
            {
                return $plan;
            }
        }
    }

    public static function limitReached($store)
    {
        //Check of unlimited plan
          if($store->getPlan($store)->stripe_id == 'est' || $store->getPlan($store)->stripe_id == 'est_year' ) return false;

          if($store->items->count() == $store->getPlan($store)->item_limit)
          {
            return true;
          }
          else{
            return false;
          }
    }
}
