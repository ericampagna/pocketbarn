<?php

namespace pocketbarn;

use Illuminate\Database\Eloquent\Model;

class Affiliate extends Model
{
   public function users()
    {
    	return $this->belongsToMany('pocketbarn\User');
    }

    public function stores()
    {
    	return $this->belongsToMany('pocketbarn\Store');
    }
}
