<?php

namespace pocketbarn;

use Illuminate\Database\Eloquent\Model;

class BoostedItem extends Model
{
    protected $table = 'boosted_items';
    protected $fillable = ['item_id', 'ends_at'];

    public function item()
    {
    	return $this->hasOne('pocketbarn\Item');
    }
}
