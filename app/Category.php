<?php

namespace pocketbarn;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function items()
    {
    	return $this->hasMany('pocketbarn\Item');
    }
}
