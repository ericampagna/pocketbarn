<?php

namespace pocketbarn;

use Illuminate\Database\Eloquent\Model;
use pocketbarn\Image;
use pocketbarn\Store;
use pocketbarn\User;
use pocketbarn\Plan;
use Storage;

class Item extends Model
{
    public function store()
    {
		return $this->belongsTo('pocketbarn\Store');
    }

    public function images()
    {
    	return $this->hasMany('pocketbarn\Image');
    }

    public function boosted()
    {
        return $this->hasOne('pocketbarn\BoostedItem');
    }

    public static function featuredImage($id)
    {
    	$image =  Image::where('item_id', '=', $id)->where('is_featured', '=', '1')->first();
        if(!$image) return false;
        // $featured_image = Storage::get('storage/'.$image->item->store_id.'/pb_300xN_'.$image->file);
        return 'pb_400x400_'.$image->file;
    }

     public static function shareImage($id)
    {
        $image =  Image::where('item_id', '=', $id)->where('is_featured', '=', '1')->first();
        // $featured_image = Storage::get('storage/'.$image->item->store_id.'/pb_300xN_'.$image->file);
        return 'pb_200x200_'.$image->file;
    }

     public static function sellers($store_id)
    {
        return User::where('store_id', '=', $store_id)->get();
    }

    public function isBoosted()
    {
        $now = date("Y-m-d H:i:s");
        if($this->boosted)
            if($this->boosted->ends_at > $now)
            {
                return true;
            }
            else{
                return false;
            }
        else{
            return false;
        }
    }

}
