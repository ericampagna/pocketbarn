<?php

namespace pocketbarn;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public function conversation(){
    	return $this->belongsTo('pocketbarn\Conversation');
    }

    public function sender(){
    	return $this->belongsTo('pocketbarn\User', 'sender_id');
    }
}
