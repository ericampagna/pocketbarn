<?php

namespace pocketbarn\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use pocketbarn\Store;

class WelcomeStore extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * The order instance.
     *
     * @var Store
     */
    public $store;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Store $store)
    {
        $this->store = $store;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.store.WelcomeStore');
    }
}
