<?php

namespace pocketbarn;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    public function users()
    {
    	return $this->belongsToMany('pocketbarn\User');
    }

    public function latestMessage()
    {
    	return $this->hasOne('pocketbarn\Message')->latest();
    }

    public function messages()
    {
    	return $this->hasMany('pocketbarn\Message');
    }

    public function store()
    {
    	return $this->belongsTo('pocketbarn\Store');
    }
}
