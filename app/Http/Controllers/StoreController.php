<?php

namespace pocketbarn\Http\Controllers;

use Illuminate\Http\Request;
use pocketbarn\Mail\WelcomeStore;
use pocketbarn\Mail\NewSeller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use pocketbarn\Store;
use pocketbarn\User;
use pocketbarn\Plan;
use pocketbarn\Affiliate;
use JavaScript;
use Image;

class StoreController extends Controller
{



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get Current User
        $user = auth()->user();
        //Get Store info
        $store = $user->store;
        if(!$store)
        {
            return redirect('/store/create');
        }
        //Get all Store owners
        $store->users;
        if($store->profile_img){
            $store->profile_img = Storage::url('stores/'.$store->id.'/'.$store->profile_img);
        }
        if($store->cover_img){
            $store->cover_img = Storage::url('stores/'.$store->id.'/'.$store->cover_img);
        }

        return view('store.edit')->with(['user' => $user, 'store' => $store, 'states' => $this->states]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        if($user->store_id)
        {
            return redirect('/store');
        }
        $plans = Plan::orderBy('monthly', 'asc')->get();
        $plans = $plans->reject(function ($plan) {
                return $plan->stripe_id == 'free';
            });
        //Add store_id as a javascript variable to use in Vue
        JavaScript::put([
            'store' => $user->store_id,
            'stripeKey' => env('STRIPE_KEY')
        ]);
        return view('store.create')->with(['user'=> $user, 'states' => $this->states, 'plans' => $plans, 'years' => $this->years, 'months' => $this->months]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        //array_push($request, $request->file('avatar'));

        $this->validate($request, [
            'store_name' => 'required|unique:stores,name',
            'address_line1' => 'required',
            'address_city' => 'required',
            'address_state' => 'required',
            'address_zip' => 'required',
            'plan' => 'required',
            'stripeToken' => 'required_unless:plan,free_new'
        ]);

        $cordinates = $this->get_lat_lng($request->input('address_zip'));

        $store = new Store;

        $store->name = $request->input('store_name');
        $store->slug = $this->make_slug($request->input('store_name'));
        $store->street = $request->input('address_line1');
        $store->city = $request->input('address_city');
        $store->state = $request->input('address_state');
        $store->zip = $request->input('address_zip');
        $store->lat = $cordinates['lat'];
        $store->lng = $cordinates['lng'];
        $store->owned_by = $user->id;

        $store->save();
        $plan =  $request->input('plan');
        $plan_type = $request->input('plan_type');
        $stripeToken = $request->input('stripeToken');
        if($plan_type == 'yearly'){
            $plan = $plan.'_year';
        }
        $store->newSubscription('main', $plan)->create($stripeToken);

        session()->flash('status', 'Your Marketplace has been created');
        session()->flash('status_type', 'type=success');

        //Send welcome email to user
        Mail::to($user->email)->queue(new WelcomeStore($store));

        $mailchimp = app('Mailchimp');
        try {
            $mailchimp
                ->lists
                ->subscribe(
                    env('MAILCHIMP_MARKETPLACE_LIST_ID'),
                    ['email' => $user->email],
                    ['FNAME' => $user->name],
                  null,
                  false
                );
                } catch (\Mailchimp_List_AlreadySubscribed $e) {
                   
                } catch (\Mailchimp_Error $e) {
                    
                }

        //Set User as sotre owner
        $user->store_id = $store->id;
        $user->save();

        //Give credit to the Affiliate is exsits
        if($request->cookie('pb_aff'))
        {
            $aff = Affiliate::where('key', '=', $request->cookie('pb_aff'))->firstOrFail();
            //dd($aff);
            $aff->stores()->attach($store->id, ['plan' => $plan]);
            $aff->save;
        }

        return redirect('/store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = auth()->user();

         $this->validate($request, [
            'store_name' => 'required|unique:stores,name,'.$id,
            'street' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'phone' => 'string',
            'story' => 'string',
            'policies' => 'string',
            'website' => 'url',
            'show_address' => 'boolean',
            'profile_img' => 'image|dimensions:min_width=200,min_height=200|mimes:jpg,jpeg,png,gif',
            'cover_img' => 'image|dimensions:min_width=960,min_height=400|mimes:jpg,jpeg,png',

        ]);

          $store = Store::find($id);

         if($request->file('cover_img'))
        {
            $cover_img = $request->file('cover_img');
            //Hash name and compile with origianl extention
            $cover_ext = $cover_img->getClientOriginalExtension();
            $cover_storage_name = md5($cover_img->getClientOriginalName().time());
            $coverPath = $cover_storage_name.'.'.$cover_ext;

            //Make and resize the Image
            $cover_img = Image::make($cover_img)->orientate()->fit(1920, 800, function ($constraint) {
                $constraint->upsize();
            }, 'top');

            //Convert to stream
            $cover_img = $cover_img->stream();
            //Save to S3
            Storage::put('stores/'.$store->id.'/'.$coverPath, $cover_img->__toString());
            //Save the file name for database
            $store->cover_img = $coverPath;
        }
        if($request->file('profile_img'))
        {
            $profile_img = $request->file('profile_img');
            //Hash name and compile with origianl extention
            $profile_ext = $profile_img->getClientOriginalExtension();
            $profile_storage_name = md5($profile_img->getClientOriginalName().time());
            $profilePath = $profile_storage_name.'.'.$profile_ext;

            //Make and resize the Image
            $profile_img = Image::make($profile_img)->orientate()->fit(200, 200, function ($constraint) {
                $constraint->upsize();
            });

            //Convert to stream
            $profile_img = $profile_img->stream();
            //Save to S3
            Storage::put('stores/'.$store->id.'/'.$profilePath, $profile_img->__toString());
            //Save the file name for database
            $store->profile_img = $profilePath;
        }

        $cordinates = get_lat_lng($request->input('zip'));



        $store->name = $request->input('store_name');
        $store->street = $request->input('street');
        $store->city = $request->input('city');
        $store->state = $request->input('state');
        $store->zip = $request->input('zip');
        $store->lat = $cordinates['lat'];
        $store->lng = $cordinates['lng'];
        $store->phone = $request->input('phone');
        $store->show_address = $request->input('show_address');
        $store->website = $request->input('website');
        $store->story = $this->make_paragraphs($request->input('story'));
        $store->policies = $this->make_paragraphs($request->input('policies'));


        $store->save();

        session()->flash('status', 'Your Marketplace has been created');
        session()->flash('status_type', 'type=success');

        return redirect('store')->with(['user' => $user, 'store' => $store, 'states' => $this->states]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find Store
        $store = Store::find($id);

        //Cancel Subscription
        $store->subscription('main')->cancelNow();
        foreach($store->users as $user) {
            $user->store_id = 0;
            $user->save();
        }

        $store->delete();

        return response('success', 200);
    }


    public function sellers()
    {
        //Get sotre info and all sellers associated
        $store = Store::find(auth()->user()->store_id);
        $store->users;
        foreach($store->users as $user)
        {
            if($user->avatar)
            {
                $user->avatar = Storage::url('users/'.$user->id.'/'.$user->avatar);
            }
        }

        //Add store_id as a javascript variable to use in Vue
        JavaScript::put([
            'store' => $store->id,
            'itemName' => ''
        ]);

        //Return view of sellers
        return view('store.sellers')->with('store', $store);
    }

    public function changePayment()
    {
        $store = Store::find(auth()->user()->store_id);

        JavaScript::put([
            'store' => $store->id,
            'itemName' => '',
            'stripeKey' => env('STRIPE_KEY')
        ]);


        return view('store.changePayment')->with(['store' => $store, 'months' => $this->months, 'years' => $this->years]);

    }

    public function updatePayment(Request $request, $store)
    {
        $store = Store::find($store);

         $this->validate($request, [
            'stripeToken' => 'required'
        ]);


        $store->updateCard($request->stripeToken);

        return redirect('/store/payment');
    }

    public function changePlan()
    {
       //Get sotre info and all sellers associated
        $store = Store::find(auth()->user()->store_id);
        $plans =  Plan::orderBy('monthly', 'asc')->get();


        if($store->subscribedToPlan('free', 'main'))
        {
            $store->current_plan = 'Beginner\'s Marketplace';

            $plans = $plans->reject(function ($plan) {
                return $plan->stripe_id == 'free_new';
            });
        }
        else{
            $plans = $plans->reject(function ($plan) {
                return $plan->stripe_id == 'free';
            });
        }


        //Add store_id as a javascript variable to use in Vue
        JavaScript::put([
            'store' => $store->id,
            'itemName' => ''
        ]);



        return view('store.subscription_plan')->with(['store' => $store, 'plans' => $plans]);
    }

    public function updatePlan($store, Request $request)
    {
        $store = Store::find($store);
        if(!$store->card_last_four || !$store->card_brand)
        {
            return redirect()->back()->withErrors(['payment' => 'You do not have any payment information']);
        }
        $store->items;
        $new_plan = $request->input('plan');
        $store->subscription('main')->swap($new_plan);

        return redirect('/store/plan');
    }

    public function inviteSeller($email)
    {
        //Get Store and invitee information
        $store = Store::find(auth()->user()->store_id);
        $invitee = User::where('email', '=', $email)->first();

        //Check to see if invitee is in database
        if($invitee){

            //Check to see if invitee already has a store
            if($invitee->store_id != null)
            {
                return response('User is already part of another Marketplace', 422);
            }

            //Asign store_id to user
            $invitee->store_id = $store->id;
            $invitee->save();

            //Send email to invitee
            Mail::to($email)->queue(new NewSeller($store));

            //Return with success
            return response('The user has been added to your store', 200);
        }
        else{
            //User not in Database
            return response('Could not find User', 422);
        }


    }

    public function removeSeller($id)
    {
        //Get user from DB
        $user = User::find($id);
        $store = Store::find($user->store_id);
        if($store->owned_by == $id)
        {
            return response('You can not delete the owner of the store.', 402);
        }
        //Remove thier store_id
        $user->store_id = null;
        $user->save();

        //Return Success
        return response('success', 200);
    }



    public function showStore($id, $slug)
    {
        $store = Store::find($id);
        if(!$store)
        {
            return abort(404);
        }
         JavaScript::put([
            'store' => $store->id,
            'itemName' => ''
        ]);
        if($store->profile_img){
            $store->profile_img = Storage::url('stores/'.$store->id.'/'.$store->profile_img);
        }
        if($store->cover_img){
            $store->cover_img = Storage::url('stores/'.$store->id.'/'.$store->cover_img);
        }
        foreach($store->users as $user)
        {
            if($user->avatar){
                $user->avatar = Storage::url('users/'.$user->id.'/'.$user->avatar);
            }
        }
        return view('store')->with('store', $store);
    }


    //  public function mailTest()
    // {
    //     $store = Store::find(1);
    //     Mail::to('eric@brushfirecreative.com')->queue(new WelcomeStore($store));
    // }
}
