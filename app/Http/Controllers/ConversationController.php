<?php

namespace pocketbarn\Http\Controllers;

use Illuminate\Http\Request;
use JavaScript;
use pocketbarn\User;
use pocketbarn\Conversation;

use Storage;

class ConversationController extends Controller
{


    public function index()
    {
    	$user = auth()->user();
    	JavaScript::put([
            'store' => '',
            'itemName' => ''
 		]);
        if($user->avatar)
        {
            $user->avatar = Storage::url('users/'.$user->id.'/'.$user->avatar);
        }
    	return view('user.conversations')->with('user', $user);

    }


    public function getUsersConversations(Request $request)
    {

    	$user = User::find($request->user);
    	$results = $user->conversations;

    	 $filtered = $results->reject(function ($convo) use($user) {
            return $convo->store_id == $user->store_id ;
        });
    	foreach($filtered as $result)
    	{
    		$result->users;
    		$result->latestMessage->sender;
            $result->latestMessage->content = strlen($result->latestMessage->content ) > 30 ? substr($result->latestMessage->content ,0,30)."..." : $result->latestMessage->content;
    		$result->store;
            if($result->store->profile_img)
            {
                $result->store->profile_img = Storage::url('stores/'.$result->store->id.'/'.$result->store->profile_img);
            }
    	}


	    return $filtered;
    }


    public function getStoreConversations(Request $request)
    {
    	$user = User::find($request->user);
    	$results = $user->conversations()->get();

    	 $filtered = $results->reject(function ($convo) use($user) {
            return $convo->store_id != $user->store_id ;
        });

    	foreach($filtered as $result)
    	{
    		$result->users;
    		$result->latestMessage->sender;
    		$result->store;
            if($result->store->profile_img)
            {
                $result->store->profile_img = Storage::url('stores/'.$result->store->id.'/'.$result->store->profile_img);
            }
    	}

	    return $filtered;
    }

    public function getConversation($id)
    {
    	$user = auth()->user();
    	//Check to see if the user is part of this conversation
    	$partOfConversation = $user->conversations->contains($id);
    	//If $countCheck is sitll 0 there don't have any conversations that match
    	if(!$partOfConversation)
    	{
    		return redirect('/conversations');
    	}
        if($user->avatar)
        {
            $user->avatar = Storage::url('users/'.$user->id.'/'.$user->avatar);
        }

    	JavaScript::put([
            'store' => '',
            'itemName' => ''
 		]);

 		$conversation = Conversation::find($id)
 			->users()
 			->updateExistingPivot($user->id, array('unread' => 0));

    	return view('user.conversation')->with('user', $user)->with('id', $id);
    }

    public function getMessages(Request $request)
    {
    	$conversation = Conversation::find($request->conversation);

    	$conversation->messages;
    	$conversation->store;
        if($conversation->store->profile_img)
        {
            $conversation->store->profile_img = Storage::url('stores/'.$conversation->store->id.'/'.$conversation->store->profile_img);
        }
    	foreach($conversation->messages as $m)
    	{
    		$m->sender;
            if($m->sender->avatar)
            {
                $m->sender->avatar = Storage::url('users/'.$m->sender->id.'/'.$m->sender->avatar);
            }
    		$m->created_at_readable =  date('m/d/y',strtotime($m->created_at));
    	}

    	return $conversation;
    }

    public function getUnreadCount()
    {
    	$user = auth()->user();
    	//Get all conversations
    	$Convos = $user->conversations;

    	//Filter only conversations between user and other stores
    	 $userConvos = $Convos->reject(function ($convo) use($user) {
            return $convo->store_id == $user->store_id ;
        	});

    	 //Filter only unread from $userConvos
    	  $userConvos_unread = $userConvos->reject(function ($convo) use($user) {
            return $convo->pivot->unread == 0 ;
        	});

    	  //Count and add number to returned dataset
    	  $data['userUnread'] = count($userConvos_unread);

    	 //If user has a marketplace
    	if($user->store_id)
    	{
    		//Filter only conversations for user marketplace
    		$storeConvos = $Convos->reject(function ($convo) use($user) {
            	return $convo->store_id != $user->store_id ;
        	});
    		//Filter only conversations with unread from $storeConvos
    	  $storeConvos_unread = $storeConvos->reject(function ($convo) use($user) {
            return $convo->pivot->unread == 0 ;
        	});

    	  $data['storeUnread'] = count($storeConvos_unread);
    	}
    	//If user doesn't have a marketplace
    	else{
    		$data['storeUnread'] = 0;
    	}

    	return $data;
    }


    public function leaveConversation($id)
    {
    	$user = auth()->user();
    	$conversation = Conversation::find($id);

    	//Remove this user from Conversation
    	$user->conversations()->detach($id);

    	$users = $conversation->users();

    	if(count($users) <= 1)
    	{
    		$conversation->archived = 1;
    		$conversation->save();
    	}

    	return response('success', 200);
    }

}
