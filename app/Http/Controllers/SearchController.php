<?php

namespace pocketbarn\Http\Controllers;

use Illuminate\Http\Request;
use pocketbarn\Item;
use pocketbarn\Store;
use pocketbarn\Category;
use Illuminate\Support\Facades\DB;
use pocketbarn\Http\Requests;
use JavaScript;
use Storage;

class SearchController extends Controller
{
     /**
     * Search Page View
     *
     * @return Serach View
     */
    public function search(Request $request)
    {
         JavaScript::put([
            'store' => '',
            'itemName' => '',
            'terms' => $request->get('terms'),
            'loc' => $request->get('loc'),
            'cat' => $request->get('cat'),
            'p' => $request->get('p'),
            'l' => $request->get('l'),

        ]);
        return view('search');
    }

     /**
     * Use search terms to search location
     *
     * @param  Request $request, string $location
     * @return array $results
     */
    public function searchItems(Request $request) {
        $this->validate($request, [
            'terms' => '',
            'lat' => '',
            'lng' => '',
        ]);
        //Setting Variables
        $terms = $request->get('terms');
        $lat = $request->get('lat');
        $lng = $request->get('lng');
        $cat = $request->get('cat');
        $per_page = $request->get('per_page');

        if($lat == 0 && $lng == 0) // No Location is set
        {

            if($terms) //Search Terms are used
            {
                 $results = $this->keywordSearch($request, $per_page);
                // $results = $results->reject(function ($item) {
                //     return $item->status != 'active';
                // });
                $response = ['data' => $results,];

                return $results;

            }
            elseif ($cat) {
                $results = $this->catSearch($request, $per_page);
                // $results = $results->reject(function ($item) {
                //     return $item->status != 'active';
                // });
                $response = ['data' => $results, 'pagination' => true];

                return $response;

            }
            else
            {
                //Get All the items, order them by latest and Paginate
                $results = $this->homeSearch($per_page);
                $results = $this->addBoosted($results);
                $response = ['data' => $results, 'pagination' => true];

                return $response;
            }
        }
        else //Location is Set
        {
            if($terms) //Search Terms are used
            {
                $results = $this->keywordSearchLocation($request);

            }
            elseif ($cat) {
                $results = $this->catSearchLocation($request);
            }
            else
            {
                $results = $this->locationSearch($request);
            }
        }
            // $results = $results->reject(function ($item) {
            //     return $item->status != 'active';
            // });
        //Format for returning
        $response = ['data' => $results];

        return $response;
    }


     /**
     * Search only the lastest items regardless of location
     *
     * @param  Request $request, string $location
     * @return array $results
     */
    function homeSearch($per_page)
    {
        $seed = session('seed');
            if (empty($seed)) {
              $seed = rand();
              session(['seed' => $seed]);
            }
        $results = Item::inRandomOrder($seed)->where('status', '=', 'active')->paginate($per_page);
        foreach($results as $result){
            if($result->featuredImage($result->id))
            {
                $result->featured_image = Storage::url('stores/'.$result->store_id.'/'.$result->featuredImage($result->id));
            }
            else{
                $result->featured_image = '/assets/images/no-featured.jpg';
            }
            if($result->store->profile_img) $result->store->profile_img = Storage::url('stores/'.$result->store_id.'/'.$result->store->profile_img);
            $result->city = $result->store->city;
            $result->state = $result->store->state;
            //$result->title = str_limit($result->title, 30, '...');
             $result->seed = $seed;
        }
       
        return $results;
    }

     /**
     * Search Using keywords regards of location
     *
     * @param  Request $request, string $location
     * @return array $results
     */
    function keywordSearch(Request $request, $per_page)
    {
        //Establish Variables
        $terms = $request->get('terms');

        //Query
        $results = Item::where(function($query) use ($terms)
            {
                    $query->where('title', 'LIKE', '%'.$terms.'%');
                    $query->orWhere('description', 'LIKE', '%'.$terms.'%');
                    $query->orWhere('short_desc', 'LIKE', '%'.$terms.'%');
                    $query->orWhere('tags', 'LIKE', '%'.$terms.'%');
            })->where('status', '=', 'active')->orderBy('id', 'desc')->paginate($per_page);
        //Iterate though query results
        //dd($results);             
        foreach($results as $result){
            if($result->featuredImage($result->id))
            {
                $result->featured_image = Storage::url('stores/'.$result->store_id.'/'.$result->featuredImage($result->id));
            }
            else{
                $result->featured_image = '/assets/images/no-featured.jpg';
            }
            if($result->store->profile_img) $result->store->profile_img = Storage::url('stores/'.$result->store_id.'/'.$result->store->profile_img);
            $result->city = $result->store->city;
            $result->state = $result->store->state;
            //$result->title = str_limit($result->title, 30, '...');
            //$response[] = $result;
        }
        $results = $this->addBoosted($results);

        if(!$results->count()) return $results;
        return $results;
    }

     /**
     * Search Using Category regards of location
     *
     * @param  Request $request, string $location
     * @return array $results
     */
    function catSearch(Request $request, $per_page)
    {
        //Establish Variables
        $cat = $request->get('cat');
        $cat = Category::where('slug', '=', $cat)->first();

        //Query
        $results = Item::where('category_id', '=', $cat->id)->where('status',  '=', 'active')->orderBy('id', 'desc')->paginate($per_page);
        //Iterate though query results
        //dd($results);

        
        foreach($results as $result){
            if($result->status == 'active'){
                if($result->featuredImage($result->id))
                {
                    $result->featured_image = Storage::url('stores/'.$result->store_id.'/'.$result->featuredImage($result->id));
                }
                else{
                    $result->featured_image = '/assets/images/no-featured.jpg';
                }

                if($result->store->profile_img) $result->store->profile_img = Storage::url('stores/'.$result->store_id.'/'.$result->store->profile_img);
                $result->city = $result->store->city;
                $result->state = $result->store->state;
                //$result->title = str_limit($result->title, 30, '...');
                //$response[] = $result;
            }
        }
        $results = $this->addBoosted($results);


        return $results;
    }

     /**
     * Search Location without search Terms
     *
     * @param  Request $request, string $location
     * @return array $results
     */
    function locationSearch(Request $request)
    {
        if($request->get('location')){
            $cords = $this->get_lat_lng($request->get('location'));
            $terms = $request->get('terms');
        }
        if($request->get('lat') && $request->get('lng'))
        {
            $cords['lat'] = $request->get('lat');
            $cords['lng'] = $request->get('lng');
        }

            $results = DB::select(DB::raw('SELECT id, ( 3959 * acos( cos( radians(' . $cords['lat'] . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $cords['lng']. ') ) + sin( radians(' . $cords['lat'].') ) * sin( radians(lat) ) ) ) AS distance FROM stores HAVING distance < 50 ORDER BY distance') );

            $response = array();
            foreach ($results as $result ) {
                $store = Store::find($result->id);

                $items= $store->items->reject(function ($item) {
                    return $item->status != 'active';
                 });
                
                foreach ($items as $item) {
                    if($item->featuredImage($item->id))
                    {
                        $item->featured_image = Storage::url('stores/'.$item->store_id.'/'.$item->featuredImage($item->id));
                    }
                    else{
                        $item->featured_image = '/assets/images/no-featured.jpg';
                    }
                    if($item->store->profile_img) $item->store->profile_img = Storage::url('stores/'.$item->store_id.'/'.$item->store->profile_img);
                    $item->city = $item->store->city;
                    $item->state = $item->store->state;
                    //$item->title = str_limit($item->title, 30, '...');
                    $response[] = $item;

                }
                $items = $this->addBoosted($items);
            }

            return $response;
    }

     /**
     * Search Using keywords and Location
     *
     * @param  Request $request, string $location
     * @return array $results
     */
    function keywordSearchLocation(Request $request)
    {
            //Establish Variables
            $cords['lat'] = $request->get('lat');
            $cords['lng'] = $request->get('lng');
            $terms = $request->get('terms');

            $results = DB::select(DB::raw('SELECT id, ( 3959 * acos( cos( radians(' . $cords['lat'] . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $cords['lng']. ') ) + sin( radians(' . $cords['lat'].') ) * sin( radians(lat) ) ) ) AS distance FROM stores HAVING distance < 50 ORDER BY distance') );

             //dd($results);

            //Establish Repsonse Array
            $response = array();

            // Iterate though the stores found in query
            foreach ($results as $result ) {
                $store = Store::find($result->id);

                $items= $store->items->reject(function ($item) {
                    return $item->status != 'active';
                 });
                
                //Iterate though items for each store and add to the response array
                foreach ($items as $item) {
                    //dd($item->tags);
                    if($this->keywordTest($item, $terms))
                    {
                        if($item->featuredImage($item->id))
                        {
                            $item->featured_image = Storage::url('stores/'.$item->store_id.'/'.$item->featuredImage($item->id));
                        }
                        else{
                            $item->featured_image = '/assets/images/no-featured.jpg';
                        }
                        if($item->store->profile_img) $item->store->profile_img = Storage::url('stores/'.$item->store_id.'/'.$item->store->profile_img);
                        $item->city = $item->store->city;
                        $item->state = $item->store->state;
                        //$item->title = str_limit($item->title, 30, '...');
                        $response[] = $item;
                    }


                }
                $items = $this->addBoosted($items);
            }

            return $response;
    }


    /**
     * Search Using keywords and Location
     *
     * @param  Request $request, string $location
     * @return array $results
     */
    function catSearchLocation(Request $request)
    {
            //Establish Variables
            $cords['lat'] = $request->get('lat');
            $cords['lng'] = $request->get('lng');
            $cat = Category::where('slug', '=', $request->get('cat'))->first();

            $results = DB::select(DB::raw('SELECT id, ( 3959 * acos( cos( radians(' . $cords['lat'] . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $cords['lng']. ') ) + sin( radians(' . $cords['lat'].') ) * sin( radians(lat) ) ) ) AS distance FROM stores HAVING distance < 50 ORDER BY distance') );

             //dd($results);

            //Establish Repsonse Array
            $response = array();

            // Iterate though the stores found in query
            foreach ($results as $result ) {
                $store = Store::find($result->id);

                $items= $store->items->reject(function ($item) {
                    return $item->status != 'active';
                 });
                

                //Iterate though items for each store and add to the response array
                foreach ($items as $item) {
                    //dd($item->tags);
                    if($item->category_id == $cat->id )
                    {
                        if($item->featuredImage($item->id))
                        {
                            $item->featured_image = Storage::url('stores/'.$item->store_id.'/'.$item->featuredImage($item->id));
                        }
                        else{
                            $item->featured_image = '/assets/images/no-featured.jpg';
                        }
                        if($item->store->profile_img) $item->store->profile_img = Storage::url('stores/'.$item->store_id.'/'.$item->store->profile_img);
                        $item->city = $item->store->city;
                        $item->state = $item->store->state;
                        //$item->title = str_limit($item->title, 30, '...');
                        $response[] = $item;
                    }


                }
                $items = $this->addBoosted($items);
            }
            //dd($response);
            return $response;
    }


    public function searchStores() {
    	// $this->validate($request, [
    	// 	'lat' => 'required',
    	// 	'lng' => 'required',
    	// 	'distance'
    	// ]);

		// $lat = $request->input('lat');
		// $lng = $request->input('lng');
		// $distance = 50;

    	// $results = DB::select(DB::raw('SELECT id, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(lat) ) ) ) AS distance FROM stores HAVING distance < ' . $distance . ' ORDER BY distance') );

        $results = Store::all();

    	foreach ($results as $store ) {
    		$response[$store->id] = array('position' => array('lat' => $store->lat, 'lng' => $store->lng) );

    	}

  		return $response;
    }


    //
    function keywordTest($item, $terms)
    {
        $terms = str_replace('%27', '', $terms);
        $title = str_replace('\'', '', $item->title);
        $short_desc = str_replace('\'', '', $item->short_desc);
        $description = str_replace('\'', '', $item->description);
        $tags = str_replace('\'', '', $item->tags);

        if(stripos($title, $terms) !== false)
        {
          $response = true;
        }
        elseif(stripos($description, $terms) !== false)
        {
            $response = true;
        }
        elseif(stripos($short_desc, $terms) !== false)
        {
            $response = true;
        }
        elseif(stripos($tags, $terms) !== false)
        {
            $response = true;
        }
        else{
            $response = false;
        }

        return $response;
    }

    public function randomTest()
    {
        $arr_a = array('1','2','3','4','5','6');
        $arr_b = array('a','b','c','d','e','f','a','b','c','d','e','f');

        // How many items are their
        $count = count($arr_a);


        // Loop through each of our boosted itmes to insert
        foreach($arr_b as $value)
        {
            // Generate a random value, between 0 and the # of items
            $rand = rand(0, $count);

            // Insert our value into $arr_a at the random position
            array_splice($arr_a, $rand, 0, $value);
        }

        echo "<pre>".print_r($arr_a, true)."</pre>";
    }

    public function addBoosted($results)
    {
        $boosted = $results->reject(function ($item) {
                return !$item->isBoosted();
            });
            $count = count($results);
            foreach($boosted as $b)
            {
                $b->promoted = true;
                $rand = rand(0, $count);
                //$b = $b->toArray();
                $results->splice($rand, 0, [$b]);
            }
            return $results;
    }

}
