<?php

namespace pocketbarn\Http\Controllers;
use JavaScript;
use Illuminate\Http\Request;

use pocketbarn\Http\Requests;

use pocketbarn\Item;
use pocketbarn\Store;
use pocketbarn\Category;
use pocketbarn\Image;
use Storage;

class ItemController extends Controller
{


 	public function getItem($storeName, $id, $itemName)
 	{

 		$item = Item::find($id);
 		$item->sellers = $item->sellers($item->store_id);
        $item->shareImage = $item->shareImage($item->id);
        $item->shareImage = Storage::url('stores/'.$item->store_id.'/'.$item->shareImage);
        foreach($item->sellers as $seller)
        {
            if($seller->avatar)
            {
                $seller->avatar = Storage::url('users/'.$seller->id.'/'.$seller->avatar);
            }
        }

        foreach($item->images as $image)
        {
            $image->file = Storage::url('stores/'.$item->store_id.'/'.$image->file);
        }
        if($item->store->profile_img)
        {
            $item->store->profile_img = Storage::url('stores/'.$item->store_id.'/'.$item->store->profile_img);
        }
 		//Add Slider Images to the page as JS var
 		JavaScript::put([
 			'sliderImages' => $item->images,
            'store' => $item->store_id,
            'itemName' => $item->title
 		]);


 		return view('singleitem')->with('item', $item);


 	}
	/*
	Move to API Contoller
	*/
 	public function getMoreItems(Request $request){
 		$current_item = $request->get('currentItem');

 		//Check id returning "More from Store"
 		if($request->get('store'))
 		{
            $store = Store::find($request->get('store'));
            if($request->get('page') == 'store')
            {
                $items = $store->items;
            }
 			else
            {
               $items = $store->items->take(3);
            }


 		}
 		//Check if returning "Similiar near you"
 		if($request->get('category'))
 		{
 			$category = Category::find($request->get('category'));

 			$items = $category->items->take(5);
 		}

        $items = $items->reject(function ($item) {

            return $item->status != 'active';
        });

 		//Remove Current Item from reuturning results
 		$items = $items->reject(function ($item) use( &$current_item) {

 			return $item->id == $current_item;
 		});

 		//Add Featured image and store info to returning results
 		foreach ($items as $item){
			if($item->featuredImage($item->id))
            {
                $item->featured_image = Storage::url('stores/'.$item->store_id.'/'.$item->featuredImage($item->id));
            }
            else{
                $item->featured_image = '/assets/images/no-featured.jpg';
            }
            //$item->featured_image = Storage::url('stores/'.$item->store_id.'/'.$item->featured_image);
			$item->store;
 		}
 		//Format for Json respons
 		 $response = [
            'data' => $items
        ];
 		return $response;
 	}

 	  /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$store = auth()->user()->store;

        $categories = Category::all();
        //Add Slider Images to the page as JS var
        JavaScript::put([
            'store' => $store->id,
            'itemName' => 'new',
            'limitReached' => $store->limitReached($store)
        ]);

        $contact_types = array('Contact to Purchase', 'Buy on Our Website');

    	return view('items.create')->with(['store'=> $store, 'categories' => $categories, 'contact_types' => $contact_types]);
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user();
        $item = Item::find($id);

        // If Item is not found
        if(!$item)
        {
            return redirect('/store/items');
        }

        // Check to see if user owns item
        if($item->store_id != $user->store->id )
        {
            return redirect('/store/items');
        }

        
        $categories = Category::all();
        $contact_types = array('Contact to Purchase', 'Buy on Our Website');
        JavaScript::put([
            'store' => $item->store_id,
            'itemName' => $item->name
        ]);

        return view('items.edit')->with([
            'store' => $item->store,
            'categories' => $categories,
            'item' => $item,
            'contact_types' => $contact_types
            ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();

        if($user->store->limitReached($user->store)) return false;

        //array_push($request, $request->file('avatar'));

        $this->validate($request, [
            'title' => 'required',
            'price' => 'required_if:status,"active"',
            'category' => 'required_if:status,"active"',
            'quantity' => '',
            'contact' => 'required_if:status,"active"',
            'item_url' => 'required_if:contact,"Buy on Our Website"|url',
            'description' => 'required_if:status,"active"',
            'short_description' => '',
            'tags' => '',
            'formImage1' => 'required_if:status,"active"',
            'status' => 'required',

        ]);

        $item = New Item;

        $item->title = $request->title;
        $item->slug = $this->make_slug($request->title);
        $item->price = str_replace('$', '', $request->price);
        $item->category_id = $request->category;
        $item->quantity = $request->quantity;
        $item->contact_button = $request->contact;
        $item->item_url = $request->item_url;
        $item->description = $request->description;
        $item->short_desc = $request->short_description;
        $item->tags = $request->tags;
        $item->store_id = $user->store->id;
        $item->delivery = $request->delivery;
        $item->shipping = $request->shipping;
        $item->pickup = $request->pickup;
        $item->hasStore = $request->hasStore;
        $item->status = $request->status;
        $item->save();

        $images = array($request->formImage1, $request->formImage2,$request->formImage3);

        for ($i=0; $i < 3 ; $i++) {
            if($images[$i])
            {
                $file = str_replace(Storage::url('stores/'.$item->store_id.'/pb_400x400_'), '', $images[$i]);
                $image = New Image;
                $image->file = $file;
                $image->item_id = $item->id;

                if($i == 0) {
                    $image->is_featured = 1;
                }
                $image->save();

                unset($image);
            }
        }
       session()->flash('status', 'Your item has been saved');
       session()->flash('status_type', 'type=success');
    }


     /**
     * Update a resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $user = auth()->user();
        //array_push($request, $request->file('avatar'));

        $this->validate($request, [
            'title' => 'required',
            'price' => 'required_if:status,"active"',
            'category' => 'required_if:status,"active"',
            'quantity' => '',
            'contact' => 'required_if:status,"active"',
            'item_url' => 'required_if:contact,"Buy on Our Website"|url',
            'description' => 'required_if:status,"active"',
            'short_description' => '',
            'tags' => '',
            'formImage1' => 'required_if:status,"active"',
            'status' => 'required',

        ]);

        $item = Item::find($id);

        $item->title = $request->title;
        $item->slug = $this->make_slug($request->title);
        $item->price = str_replace('$', '', $request->price);
        $item->category_id = $request->category;
        $item->quantity = $request->quantity;
        $item->contact_button = $request->contact;
        $item->item_url = $request->item_url;
        $item->description = $request->description;
        $item->short_desc = $request->short_description;
        $item->tags = $request->tags;
        $item->store_id = $user->store->id;
        $item->delivery = $request->delivery;
        $item->shipping = $request->shipping;
        $item->pickup = $request->pickup;
        $item->hasStore = $request->hasStore;
        $item->status = $request->status;
        $item->save();

        $images = array($request->formImage1, $request->formImage2,$request->formImage3);

        for ($i=0; $i < 3 ; $i++) {
            if($images[$i])
            {
                $file = str_replace(Storage::url('stores/'.$item->store_id.'/pb_400x400_'), '', $images[$i]);
                if(!Image::where('file', '=', $file)->first())
                {
                    $image = New Image;
                    $image->file = $file;
                    $image->item_id = $item->id;

                    if($i == 0) {
                        $image->is_featured = 1;
                    }
                    $image->save();

                    unset($image);
                }
            }
        }
       session()->flash('status', 'Your item has been saved');
        session()->flash('status_type', 'type=success');

    }

 	public function storeItems()
 	{
 		$user = auth()->user();

 		$store = $user->store;
        $store->slug = $this->make_slug($store->name);

 		$store->plan = $store->getPlan($store);
        $items = $store->items;
        foreach($items as $item){
            if($item->featuredImage($item->id))
            {
                $item->featured_image = Storage::url('stores/'.$item->store_id.'/'.$item->featuredImage($item->id));
            }
            else{
                $item->featured_image = '/assets/images/no-featured.jpg';
            }
            $item->slug = $this->make_slug($item->title);
        }

        $limitReached = $store->limitReached($store);

        $blanks = $store->plan->item_limit - count($items);

        if($blanks > 0)
        {
           $addBlanks = $blanks;
        }
        else{
            $addBlanks = 0;
        }

 		return view('store.items')->with(['store' => $store, 'items' => $items, 'blanks' => $addBlanks, 'limitReached' => $limitReached ]);
 	}


    public function destroy($id)
    {
        $item = Item::destroy($id);

        session()->flash('status',  'Your item has been removed');
        session()->flash('status_type', 'type=success');

        return response('success', 200);

    }







}
