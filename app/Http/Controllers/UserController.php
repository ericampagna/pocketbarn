<?php

namespace pocketbarn\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use pocketbarn\Http\Requests;
use Validator;
use Image;

class UserController extends Controller
{
    public function profile()
    {
    	$user = auth()->user();
        if($user->avatar)
        {
            $user->avatar = Storage::url('users/'.$user->id.'/'.$user->avatar);
        }
    	return view('user.profile')->with('user', $user);
    }


    public function update(Request $request)
    {

    	$user = auth()->user();
    	//array_push($request, $request->file('avatar'));

    	$this->validate($request, [
    		'name' => 'required',
    		'email' => 'required|email|unique:users,email,'.$user->id,
    		'gender' => 'required',
    		'location' => '',
    		'avatar' => 'image|dimensions:min_width=100,min_height=100|mimes:jpg,png,gif,jpeg'
    	]);

    	if($request->file('avatar'))
    	{
            //Get File for avatar
            $avatar = $request->file('avatar');
            //get extention of original file
            $ext = $avatar->getClientOriginalExtension();
            //created hased name for image
            $storageName = md5($avatar->getClientOriginalName().time());
            //Make Image object
            $image = Image::make($avatar);
            //Resize the image to profile size
            $image = $image->orientate()->fit(300, 300, function ($constraint) {
                $constraint->upsize();
            });
            //Set Image stream
            $image = $image->stream();
            //Check to see if a directory for this store exists, if not make one
            if(!Storage::disk('s3')->exists('users/'.$user->id))
            {
                Storage::makeDirectory('users/'.$user->id);
            }
            //Save the image to the s3 disk and save the location
    		Storage::disk('s3')->put('users/'.$user->id.'/'.$storageName.'.'.$ext, $image->__toString());
            //Save the filename in database
    		$user->avatar = $storageName.'.'.$ext;

    	}

        $location = $this->get_lat_lng($request->location);

    	$user->gender = $request->input('gender');
    	$user->name = $request->input('name');
    	$user->email = $request->input('email');
        $user->location = $request->input('location');
        $user->lat = $location['lng'];
        $user->lng = $location['lat'];

    	$user->save();
    	$request->session()->flash('status', 'Your Profile has been updated');
    	$request->session()->flash('status_type', 'type=success');
    	return redirect()->route('my-profile');
    }

}
