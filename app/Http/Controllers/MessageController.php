<?php

namespace pocketbarn\Http\Controllers;

use Illuminate\Http\Request;
use pocketbarn\Store;
use pocketbarn\User;
use pocketbarn\Conversation;
use pocketbarn\Message;
use Mail;


class MessageController extends Controller
{


    public function newMessage(Request $request)
    {
    	$this->validate($request, [

    		    'store' => 'required|exists:stores,id',
            'content' => 'required',
        ]);

        $store = Store::find($request->store);

       $conversation = new Conversation;
       $conversation->store_id = $request->store;
       $conversation->save();

       $user = auth()->user();
       $user->conversations()->save($conversation, ['unread' => 0]);

       foreach($store->users as $seller)
       {
          if($user->id != $seller->id)
          {
         	  $seller->conversations()->save($conversation, ['unread' => 1]);
            $data = array('receiver' => $seller, 'content' => $request->content, 'sender' => $user);
            Mail::send('emails.message', $data, function($message) use ($data)
            {
                $message->from('no-reply@mypocketbarn.com', "pocketbarn");
                $message->subject("You have a message from a user");
                $message->to($data['receiver']->email);
            });
          }
       }

       $message = new Message;

       $message->content = $request->content;
       $message->sender_id = $user->id;
       $message->conversation_id = $conversation->id;
       $message->save();



       return response("Message has been sent. Check your conversations under 'My Profile'.", 200);

    }

    public function sendMessage(Request $request)
    {
      $user = auth()->user();
      $this->validate($request, [

            'conversation' => 'required|exists:conversations,id',
            'content' => 'required',
        ]);

        $conversation = Conversation::find($request->conversation);

       $message = new Message;

       $message->content = $request->content;
       $message->sender_id = $user->id;
       $message->conversation_id = $conversation->id;
       $message->save();

       $conversation->users;

       foreach($conversation->users as $convo_user)
       {
          if($user->id != $convo_user->id)
          {
            $convo_user->conversations()->updateExistingPivot($conversation->id, ['unread' => 1]);
            $data = array('receiver'=>$convo_user, 'content' => $request->content, 'sender' => $user);
            Mail::send('emails.message', $data, function($message) use ($data)
            {
                $message->subject("You have a message from a user");
                $message->to($data['receiver']->email);
            });
          }
       }

       return response("Message has been sent. Check your conversations under 'My Profile'.", 200);
    }


    public function checkForConversation($user_id, $store_id)
    {
    	$store = Store::find($store_id);
    	$user = User::find($user_id);
    	//Check to see if the user has any conversations
    	if(count($user->conversations) > 0)
    	{

    		foreach($user->conversations as $user_convo)
    		{
    			foreach($store->users as $seller)
    			{
    				foreach($seller->conversations as $seller_convo)
    				{
    					if($user_convo->id == $seller_convo->id) return $user_convo->id;
    				}
    			}
    		}

    		return false;
    	}
    	//This user has no conversations
    	else
    	{
    		return false;
    	}
    }

}
