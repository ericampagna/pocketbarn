<?php

namespace pocketbarn\Http\Controllers;

use Illuminate\Http\Request;
use JavaScript;

class AboutController extends Controller
{
    

    public function aboutPage()
    {
    	JavaScript::put([
            'showModal' => session()->has('showModal'),
            'store' => '',
            'itemName' => '',
        ]);
    	return view('about');
    }
}
