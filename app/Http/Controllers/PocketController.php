<?php

namespace pocketbarn\Http\Controllers;

use Illuminate\Http\Request;
use JavaScript;
use Storage;

class PocketController extends Controller
{


    public function index()
    {
    	$user = auth()->user();
		JavaScript::put([
            'store' => '',
            'itemName' => ''
 		]);
    	return view('user.pocket')->with('user', $user);


    }

    public function getPocket()
    {
    	$user = auth()->user();

    	if(count($user->myPocketItems) > 0)
    	{
    		foreach($user->myPocketItems as $item)
    		{
    			$item->store;
    			$item->featured_image = Storage::url('stores/'.$item->store->id.'/'.$item->featuredImage($item->id));
    		}
    		$data['items'] = $user->myPocketItems;
    	}
    	else
    	{
    		$data['items'] = Null;
    	}

    	if(count($user->myPocketStores) > 0)
    	{
    		// foreach($user->myPocketStores as $store)
    		// {
    		// 	$store
    		// }
    		$stores = $user->myPocketStores;
            foreach($stores as $store)
            {
                $store->profile_img = Storage::url('stores/'.$store->id.'/'.$store->profile_img);
            }
            $data['stores'] = $stores;
    	}

    	return response($data, 200);
    }
    public function removeFromPocket($type, $id)
    {
    	$user = auth()->user();
    	if($type == 'item')
    	{
    		$user->mypocketItems()->detach($id);
    	}
        if($type == 'store')
        {
            $user->mypocketStores()->detach($id);
        }

    	return response('success', 200);

    }
    public function addToPocket($type, $id)
    {
    	$user = auth()->user();
        if(!$user) return response('You must be logged into save item or Marketplaces to you pocket', 409);
    	if($type == 'item')
    	{

            if($user->myPocketItems->contains($id)) return response('Item is already in your pocket', 409);

            $user->mypocketItems()->attach($id);

    	}
    	if($type == 'store')
    	{
            $exsists = $user->whereHas('myPocketStores', function ($query) use ($id) {
                    $query->where('id', '=', $id);
                })->first();

            if($user->myPocketStores->contains($id)) return response('Marketplace is already in your pocket', 409);

    		$user->myPocketStores()->attach($id);
    	}

    	return response('success', 200);

    }
}
