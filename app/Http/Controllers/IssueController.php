<?php

namespace pocketbarn\Http\Controllers;

use Illuminate\Http\Request;
use pocketbarn\Issue;
use Mail;

class IssueController extends Controller
{
    public function create(Request $request )
    {
    	$this->validate($request, [
            'date' => 'required',
            'user_agent' => '',
            'page' => '',
            'type' => '',
            'description' => 'required',
        ]);

        $issue = new Issue;
        $issue->page = $request->page;
        $issue->type = $request->type;
        $issue->description = $request->description;
        $issue->user_agent = $request->user_agent;
        $issue->status = 'open';
        $issue->priority = 'normal';
        if(auth()->user())
        {
        	$issue->user_id = auth()->user()->id;
        }


        $issue->save();

    }
}
