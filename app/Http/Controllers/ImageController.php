<?php

namespace pocketbarn\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use File;
use Validator;
use Storage;
use pocketbarn\Item;
use pocketbarn\Image as PB_Image;



class ImageController extends Controller
{


    /**
     *
     * Upload Item Image Ajax
     *
     */

    public function upload(Request $request)
    {

        $messages = [
            'dimensions' => 'Opps! It looks like your image is too small. Please check the recomended sizes.',
            'mimes' => 'Opps! It looks like you didn\'t upload an image.',
            'image' => 'Opps! It looks like you didn\'t upload an image.',
        ];
        //Get Variables need to Make and Save websites
       $validator = Validator::make($request->all(), [
            'file' => 'bail|image|mimes:jpg,jpeg,png',
        ], $messages);
       if ($validator->fails()) {
        $data['error'] = $validator->errors()->all()[0];
        $data['status'] = 'error';
        return response($data, 422);
       }
        $store = auth()->user()->store;

        $file_path = 'stores/'.$store->id;

        $file = $request->file('file');

        //Get File name and extention
        $ext = $file->getClientOriginalExtension();
        $storageName = md5($file->getClientOriginalName().time());

        //Check to see if a directory for this store exists, if not make one
        // if(!Storage::disk('s3')->exists($file_path))
        // {
        //     Storage::makeDirectory($file_path;
        // }

        //Save Full size Image
    	$image = Image::make($file)->orientate();
        $image_stream = $image->stream();
        Storage::disk('s3')->put($file_path.'/'.$storageName.'.'.$ext, $image_stream->__toString());
        $data['pb_fullxfull'] = Storage::url($file_path.'/fullxfull_'.$storageName.'.'.$ext);
        //$image->orientate();

         //Save Each additional sizes
        foreach($this->image_sizes as $name => $vars){
            $i = Image::make($file)->orientate();
            $i_stream = $i->stream();
            $varsArray = explode(',',$vars);
            $func = $varsArray[2];
            $w_size = $varsArray[0];
            $h_size = $varsArray[1];
            $img = $i->$func($w_size, $h_size, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $img_stream = $img->stream();
            Storage::disk('s3')->put($file_path.'/'.$name.'_'.$storageName.'.'.$ext, $img_stream->__toString());
            $data[$name] = Storage::url($file_path.'/'.$name.'_'.$storageName.'.'.$ext);

            reset($varsArray);
            reset($i);
            reset($i_stream);
            reset($img);
        }

        $data['storageName'] = $storageName.'.'.$ext;

        $data['status'] = 'ok';




        return response($data, 200);

    }
    /* Profile Upload*/
     public function uploadProfile(Request $request)
    {
       
        $user = auth()->user();

        $file_path = 'users/'.$user->id;

        //Get image data
        $file = $request->input('img');
        list($type, $file) = explode(';', $file);
        list(, $file)      = explode(',', $file);
        $file = base64_decode($file);

        //Storeage Name
        $storageName = 'avatar_fullxfull_'.md5(time()).'.png';

        //Upload new profile to S3 bucket
        Storage::put($file_path.'/'.$storageName, $file);

        //Save new profile to User
        $user->avatar = $storageName;
        $user->save();

        //Compile data to return
        $data['imageURL'] = Storage::url($file_path.'/'.$storageName);
        $data['storageName'] = $storageName;
        $data['status'] = 'ok';

        return response($data, 200);

    }

    /* Store Upload*/
     public function uploadStore(Request $request)
    {
       
        $user = auth()->user();
        $store = $user->store;

        $file_path = 'stores/'.$store->id;

        //Get image data
        $file = $request->input('img');
        list($type, $file) = explode(';', $file);
        list(, $file)      = explode(',', $file);
        $file = base64_decode($file);

        //Storeage Name
        $storageName = 'profile_fullxfull_'.md5(time()).'.png';

        //Upload new profile to S3 bucket
        Storage::put($file_path.'/'.$storageName, $file);

       
        //Save new profile to User
        $store->profile_img = $storageName;
        $store->save();

        //Compile data to return
        $data['imageURL'] = Storage::url($file_path.'/'.$storageName);
        $data['storageName'] = $storageName;
        $data['status'] = 'ok';

        return response($data, 200);

    }
    /* Cover Photo Upload*/
     public function uploadCover(Request $request)
    {
       
        $user = auth()->user();
        $store = $user->store;

        $file_path = 'stores/'.$store->id;

        //Get image data
        $file = $request->input('img');
        list($type, $file) = explode(';', $file);
        list(, $file)      = explode(',', $file);
        $file = base64_decode($file);

        //Storeage Name
        $storageName = 'cover_fullxfull_'.md5(time()).'.png';

        //Upload new profile to S3 bucket
        Storage::put($file_path.'/'.$storageName, $file);

       
        //Save new profile to User
        $store->cover_img = $storageName;
        $store->save();

        //Compile data to return
        $data['imageURL'] = Storage::url($file_path.'/'.$storageName);
        $data['storageName'] = $storageName;
        $data['status'] = 'ok';

        return response($data, 200);

    }

    public function getItemImages($id)
    {
        $item = Item::find($id);

        $images = array(
            'img1' => array('num' => 1, 'src' => '', 'loading' => false),
            'img2' => array('num' => 2, 'src' => '', 'loading' => false),
            'img3' => array('num' => 3, 'src' => '', 'loading' => false)
            );
       $i = 1;
       foreach($item->images as $image)
       {
            $images['img'.$i] = array(
                'num' => $i,
                'src' => Storage::url('stores/'.$item->store->id.'/pb_400x400_'.$image->file),
                'loading' => 'false',
                'id' => $image->id
            );
             $i++;
        }

        return $images;
    }

    public function remove($id)
    {
        $image = PB_Image::find($id);

        if($image)
        {
            $image->delete();

            $data['status'] = 'ok';

            session()->flash('status', 'The Image has been removed');
            session()->flash('status_type', 'type=success');

            return response($data, 200);

        }
        else
        {
            $data['status'] = 'error';

            session()->flash('status', 'There was a problem removing image');
            session()->flash('status_type', 'type=error');

            return response($data, 422);
        }

    }

}
