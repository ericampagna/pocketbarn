<?php

namespace pocketbarn\Http\Controllers;

use pocketbarn\Http\Requests;
use Illuminate\Http\Request;
use JavaScript;
use Cookie;
use pocketbarn\Affiliate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if(!$request->get('p')) $request->session()->forget('seed');

     JavaScript::put([
            'showModal' => session()->has('showModal'),
            'store' => '',
            'itemName' => '',
            'p' => $request->get('p'),
            'l' => $request->get('l')
        ]);


        return view('home');
    }

    public function ref(Request $request, $aff)
    {
         if(!$request->get('p')) $request->session()->forget('seed');

        JavaScript::put([
            'showModal' => session()->has('showModal'),
            'store' => '',
            'itemName' => '',
            'p' => $request->get('p'),
            'l' => $request->get('l')
        ]);


        $aff = Affiliate::where('key' , '=', $aff)->first();
        if($aff)
        {
           Cookie::queue(Cookie::make('pb_aff', $aff, 1440));
        }
            
        return view('home');
    }


}
