<?php

namespace pocketbarn\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use pocketbarn\BoostedItem;

class BoostedItemController extends Controller
{
	public function getBoostedItems()
	{
		$store = auth()->user()->store;
        $store->plan = $store->getPlan($store);
		$items = $store->items;
		$boosted_items = $items->reject(function ($item) {
		    return !$item->isBoosted();
		});
		foreach($boosted_items as $item){
            $item->featured_image = Storage::url('stores/'.$item->store_id.'/'.$item->featuredImage($item->id));
            $item->slug = $this->make_slug($item->title);
            $item->boosted->ends_at_readable =  date('M d, Y',strtotime($item->boosted->ends_at));
        }
    	return view('boosted.boosted_items')->with(['store'=> $store, 'boosted_items' => $boosted_items]);
	}

    public function create() {

    	$store = auth()->user()->store;
    	$items = $store->items;
    	$items = $items->reject(function ($item) {
		    return $item->status == 'in-active';
		});
		$items = $items->reject(function ($item) {
		    return $item->isBoosted();
		});
    	foreach($items as $item){
            $item->featured_image = Storage::url('stores/'.$item->store_id.'/'.$item->featuredImage($item->id));
        }



    	return view('boosted.create')->with(['store'=> $store, 'items' => $items]);

    }

    public function store(Request $request)
    {
    	$store = auth()->user()->store;
        if(!$store->card_last_four || !$store->card_brand)
        {
            return redirect()->back()->withErrors(['payment' => 'You do not have any payment information']);
        }
    	$this->validate($request, [
            'days' => 'required|integer',
            'item' => 'required|integer'
        ]);
    	$now = time();
        $endDate = date('Y-m-d H:i:s', strtotime("+".$request->days." days"));

        $boosted_item = BoostedItem::updateOrCreate(
        	['item_id' => $request->item],
        	['ends_at' => $endDate]
    	);
        $amt = $request->days * 300;

        if($store->invoiceFor('Boost Item for '.$request->days.' days', $amt))
        {
        	$boosted_item->save();
        	$request->session()->flash('status', 'Your item will be boosted for ' .$request->days .' days');
        	return response(200);
        }
        else{
        	return response(422, 'We could not process your payment. Please try again.');
        }


    }
}
