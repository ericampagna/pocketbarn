<?php

namespace pocketbarn\Http\Controllers;

use Illuminate\Http\Request;

use pocketbarn\Http\Requests;
use pocketbarn\SocialAccountService;
use Socialite;

class SocialAuthController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback(SocialAccountService $service, $provider)
    {
         try {
            $user = Socialite::driver($provider);
        } catch (Exception $e) {
            return Redirect::to('auth/'.$provider);
        }
        // when facebook call us a with token
        $authUser = $service->createOrGetUser($user);

        auth()->login($authUser);

        return redirect()->to('/');
    }


}
