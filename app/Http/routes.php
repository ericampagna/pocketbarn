<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => ['web']], function (){

//Used to test eamils working
// Route::get('api/v1/mailTest', 'StoreController@mailTest');

Route::get('api/v1/search', 'SearchController@searchItems');
Route::get('api/v1/getMore', 'ItemController@getMoreItems');
Route::post('api/v1/issues/new', 'IssueController@create');

//Routes for Auth

Route::auth();

Route::get('', 'HomeController@index');
Route::get('ref/{aff}', 'HomeController@ref');
Route::get('home', 'HomeController@index');
Route::get('about', 'AboutController@aboutPage');
Route::get('redirect/{provider}', 'SocialAuthController@redirect');
Route::get('callback/{provider}', 'SocialAuthController@callback');
Route::get('mypocket', 'PocketController@index');

Route::get('privacy-policy', function(){
	return view('privacy-policy');
});

Route::get('search', 'SearchController@search');
Route::get('support', 'SupportController@index');
Route::group(['middleware' => 'auth'], function() {

	Route::get('logout', 'Auth\LoginController@logout');
	//User Routes
	Route::get('my-profile', 'UserController@profile')->name('my-profile');
	Route::patch('my-profile', 'UserController@update');

	//Boosted Items Routes
	Route::get('store/boosted-items', 'BoostedItemController@getBoostedItems');
	Route::get('store/boost-an-item', 'BoostedItemController@create');
	Route::put('api/v1/item/boost', 'BoostedItemController@store');

	//Store Routes
	Route::get('store/sellers', 'StoreController@sellers');
	Route::get('api/v1/seller/remove/{id}', 'StoreController@removeSeller');
	Route::get('api/v1/seller/invite/{email}', 'StoreController@inviteSeller');
	Route::get('store/items', 'ItemController@storeItems');
	Route::get('store/add-item', 'ItemController@create');
	Route::get('store/payment', 'StoreController@changePayment');
	Route::patch('store/payment/{store}', 'StoreController@updatePayment');
	Route::get('store/plan', 'StoreController@changePlan');
	Route::patch('store/plan/{store}', 'StoreController@updatePlan');
	Route::resource('store', 'StoreController');
	Route::put('store', 'StoreController@store');
	Route::resource('item', 'ItemController');
	//Route::get('store/conversations', 'ConversationController@getStoreConverstaions');



	//Images Routes
	Route::post('store/image/upload', 'ImageController@upload');
	Route::post('api/v1/image/upload/profile', 'ImageController@uploadProfile');
	Route::post('api/v1/image/upload/store', 'ImageController@uploadStore');
	Route::post('api/v1/image/upload/cover', 'ImageController@uploadCover');
	Route::get('api/v1/image/getItemImages/{id}','ImageController@getItemImages');
	Route::get('api/v1/image/remove/{id}', 'ImageController@remove');

	//Message Routes
	Route::post('api/v1/messages/new', 'MessageController@newMessage');
	Route::get('conversations', 'ConversationController@index');
	Route::get('api/v1/conversations/getUsersConversations', 'ConversationController@getUsersConversations');
	Route::get('api/v1/conversations/getStoreConversations', 'ConversationController@getStoreConversations');
	Route::get('api/v1/conversations/leaveConversation/{id}', 'ConversationController@leaveConversation');
	Route::get('api/v1/conversations/getUnreadCount', 'ConversationController@getUnreadCount');
	Route::get('conversation/{id}', 'ConversationController@getConversation');
	Route::get('api/v1/getMessages', 'ConversationController@getMessages');
	Route::post('api/v1/messages/sendMessage', 'MessageController@sendMessage');
	//Route::get('conversations', 'ConversationController@')

	//My Pocket Routes
	Route::get('api/v1/mypocket/getPocket', 'PocketController@getPocket');
	Route::get('api/v1/mypocket/removeFromPocket/{type}/{id}', 'PocketController@removeFromPocket');
	Route::get('api/v1/mypocket/addToPocket/{type}/{id}', 'PocketController@addToPocket');
});
//Route::post('api/searchStores', 'SearchController@searchStores');

Route::get('marketplace/{id}/{store}', 'StoreController@showStore');
Route::get('item/{store}/{id}/{item}', 'ItemController@getItem');

});

// Route::get('mobilelistings', function () {
//     return view('mobilelistings');
// });

// Route::get('store', function () {
//     return view('store');
// });









