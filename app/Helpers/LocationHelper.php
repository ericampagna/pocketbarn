<?php

// function to get  the address
    function get_lat_lng($address){

        if($address)
        {
            $address = str_replace(" ", "+", $address);

            $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
            $json = json_decode($json);

            $cords['lat'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $cords['lng'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};

            return $cords;
        }
    }