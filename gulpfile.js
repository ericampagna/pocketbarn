var elixir = require('laravel-elixir');
require('laravel-elixir-minify-html');
elixir.config.sourcemaps = true;
require('laravel-elixir-vueify');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.sass(['main.scss'], 'public/assets/css/main.css');

    mix.browserify('main.js');

    mix.version(['assets/css/main.css', 'js/main.js']);

    
});

