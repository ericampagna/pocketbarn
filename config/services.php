<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-west-2',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => pocketbarn\Store::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => '1800242720253879',
        'client_secret' => 'bd794473b83ea32a314ea99e6e5ea161',
        'redirect' => 'https://www.'. env('APP_DOMAIN') .'/callback/facebook',
    ],
    'twitter' => [
        'client_id' => 'plmjYvRkZvOtNQRHYfGDzCihP',
        'client_secret' => 'ebrWJyuup30tj4dLbepqcNX1sipWAblUXg0Ho3vIJWv2ZdBFWg',
         'redirect' => 'https://www.'. env('APP_DOMAIN') .'/callback/twitter',
    ],
    'google' => [
        'client_id' => '149577537149-kt8o27m3rm2911f5ajblqe95er71b0gt.apps.googleusercontent.com',
        'client_secret' => 'nN7QjEHnD0pBe79fwd2WSTus',
        'redirect' => 'https://www.' .env('APP_DOMAIN') .'/callback/google',
    ]

];
