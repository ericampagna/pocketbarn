@extends('layouts.app')

@section('content')
<div class="container">
    @if(session()->has('status'))
        <alert {{ session('status_type') }}>{{ session('status') }}</alert>
    @endif
        <form method="POST" action="/item/22" v-ajax complete="Item has been deleted">

            {{ method_field('DELETE') }}
            {{ csrf_field() }}

            <button type="submit">Delete Item</button>

        </form>
</div>
<script type="text/javascript" src="/js/main.js"></script>
@endsection
