@extends('layouts.mainlayout')

@section('title', 'Boosted Items')

@section('content')
<section class="container account-page" id="pb-app-store">
	@include('store.sidebar')
		@if (session('status'))
		   <alert>
		        {{ session('status') }}
		    </alert>
		@endif
		<div class="Store Items">
		@if(!$store->card_last_four || !$store->card_brand)
			<div class="needPayment">
				<div class="item-alert">
					 		<h4>You do not have any payment information. Please add a payment option before proceeding.</h4>
					 		<a href="/store/payment" class="btn"><i class="pb-icon credit ico-small"></i> Add Payment Info</a>
					 	</div>
			</div>
		@endif
		<h3>{{ $store->name }} Boosted Items</h3>
			<a href="/store/boost-an-item" class="btn"><i class="pb-icon star-white ico-small"></i> Boost an Item</a>
			<div class="Cards">
				@foreach($boosted_items as $item)
					<div class="Card animated fadeInUpBig" style="width: 23.5%;" transition="slideUp">
						@if($item->status == 'active')
			      			<p class="Card--status active"><i class="pb-icon check ico-small"></i> Active</p>
			      		@elseif($item->status == 'in-active')
			      			<p class="Card--status in-active"><i class="pb-icon diskette ico-small"></i> Saved</p>
			      		@endif
						<a href="/item/{{$item->slug}}" class="Card--link" >
					    	<div class="Card--image">
					    		@if($item->featured_image)
					      			<img src="{{$item->featured_image}}">
					      		@else
					      			<img src="/assets/images/no-featured.jpg">
					      		@endif
					      	</div>
					      </a>

					    	<div class="Card--action">
					      		<a href="/{{$store->slug}}/{{$item->slug}}" class="Card--link" >
					      			<span class="Card--title">{{$item->title}}</span>
					      		</a>
					      		<p>Boosted until: {{ $item->boosted->ends_at_readable }}</p>
					    	</div>
					  	</div>

					 @endforeach
					 @if($boosted_items->count() < 1)
					 	<div class="item-alert">
					 		<h4>You do not have any boosted items</h4>
					 		<a href="/store/boost-an-item" class="btn"><i class="pb-icon star-white ico-small"></i> Boost an Item</a>
					 	</div>
					 @endif
			</div>

		</div>

</section>


@endsection

@section ('footer')
@endsection