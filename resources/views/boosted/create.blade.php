@extends('layouts.mainlayout')

@section('title', 'Boost an Item')

@section('content')
<section class="container account-page" id="pb-app-store" v-cloak>
	@include('store.sidebar')
		<div class="Store Items">
		<h3>Choose item to boost...</h3>
			<h5> You can boost any active item for $3 a day</h5>
			<div class="Cards">
				@foreach($items as $item)
					<div class="Card animated fadeInUpBig" style="width: 23.5%;" transition="slideUp">
				    	<div class="Card--image">
				    		@if($item->featured_image)
				      			<img src="{{$item->featured_image}}">
				      		@else
				      			<img src="/assets/images/no-featured.jpg">
				      		@endif
				      	</div>
				      </a>

				    	<div class="Card--action">
				      			<span class="Card--title">{{$item->title}}</span>
				      			<button @click="boostItemModal({{$item}})" class="btn">Boost Item</button>
				    	</div>
				  	</div>

					 @endforeach
					 @if($items->count() < 1)
						 <div class="item-alert">
						 	<h4>You do not have any items that can be boosted</h4>
						 	<h5>Please add a new item or active a saved item</h5>
						 </div>
					 @endif
			</div>
		</div>
		<modal v-show="showModal">

		  	<h3 slot="header">Boost @{{item.title}}</h3>

		  	<div slot="body">
			  	 <alert :important="true" v-if="alert.show" type=error>@{{alert.message}}</alert>
		  		<div class="Boosted">
			  		<div class="Card" style="width: 300px;" >
				    	<div class="Card--image">
				      		<img :src="item.featured_image">
				      	</div>
				      </a>
				    	<div class="Card--action">
			      			<a href="#" class="Card--link"><span class="Card--title">@{{item.title}}</span></a>
			      			<a href="#" class="Card--location"><i class="pe-7s-map-marker"></i>{{ $store->city }}, {{ $store->state }}</a>
				    	</div>
				  	</div>
				  	<div class="Boosted--options">
				  		<h6>How many days would you like to boost this item?</h6>
				  		<input v-model="days" type="number" min="1" name="boostedDays" id="boostedDays">
				  		<h5>Total Boosted Cost:</h5>
				  		<h5>$@{{totalCost}}</h5>
				  		<div class="Boosted--authorize">
					  		<p>By boosting this item you are authorizing the one time payment of <strong>$@{{totalCost}}</strong> charge to  your card ending in: <strong>{{$store->card_last_four}}</strong>
					  		</p>
				  		</div>
				  	</div>
			  	</div>
		  	</div>
		  	<div slot="footer">
		  	<a class="btn" @click="$root.boostItem()" >@{{{boostBtn.button}}}</a>
		  	<a class="btn btn-purple" @click="$root.showModal = false" ><i class="pe-7s-close"></i> Cancel</a>
	        </div>
	  </modal>

</section>


@endsection

@section ('footer')
@endsection