@extends('layouts.mainlayout')
@section('title', 'Reset Password')
@section('meta_description', 'Reset you pocketbarn password')
@section('content')
<section class="Reset">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <form class="Reset--form" role="form" method="POST" action="{{ url('/password/email') }}">
        <h3>Reset your Password</h3>
            {{ csrf_field() }}

           <input id="email" type="email" class="input-full" name="email" value="{{ old('email') }}" placeholder="E-Mail Address">

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
       <button type="submit" class="btn btn-purple">
        Send Password Reset Link
       </button>
       <p>
        <a href="/login#login"><i class="pe-7s-angle-left"></i> Back to Login</a>
        </p>
    </form>

</section>
@endsection
