@extends('layouts.mainlayout')
@section('title', 'Reset Password')
@section('meta_description', 'Reset you pocketbarn password')
@section('content')
<section class="Reset">
    <form class="Reset--form" role="form" method="POST" action="{{ url('/password/reset') }}">
        <h3>Reset Password</h3>
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">
            <input id="email" type="email" class="input-full" name="email" value="{{ old('email') }}" placeholder="E-Mail Address">

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
            <input id="password" type="password" class="input-full" name="password" placeholder="Password">

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            <input id="password_confirmation" type="password" class="input-full" name="password_confirmation" placeholder="Password">

            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
            <div>
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-sign-in"></i> Reset Password
            </button>
            </div>

        </form>

</section>
@endsection
