@extends('layouts.mainlayout')
@section('title', 'Login ')
@section('meta_description', 'Signin to start your pocket today.')
@section('content')
<section class="Login" id="login">
    <div class="Login--form">
        <form class="" role="form" method="POST" action="/login">
            {{ csrf_field() }}
            <h3>Login</h3>
            <input id="email" type="email" class="input-full" name="email" value="{{ old('email') }}" placeholder="E-Mail Address">

            @if ($errors->has('email'))
                <span class="help-block error">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
            <input id="password" type="password" class="input-full" name="password" placeholder="Password">

            @if ($errors->has('password'))
                <span class="help-block error">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            <label class="remember_me">
                <input type="checkbox" name="remember"> Remember Me
            </label>
            <div>
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-sign-in"></i> Login
            </button>
            <a class="btn-link" href="/password/reset">Forgot Your Password?</a>
            </div>
        </form>
    </div>
    <div class="Login--social">
        <h4 class="text-center"><i>Or Login with...</i></h4>
        <a href="{{ url('/redirect/facebook/') }}" class="btn btn-large facebook-login"><i class="socicon-facebook"></i> Facebook</a>
        <a href="{{ url('/redirect/twitter/') }}" class="btn btn-large twitter-login"><i class="socicon-twitter"></i> Twitter</a>
        <a href="{{ url('/redirect/google/') }}" class="btn btn-large google-login"><i class="socicon-googleplus"></i> Google</a>
    </div>
    <div class="goToSignup">
        <a href="/register">
            <p>Sign up!</p>
        </a>
    </div>
</section>
@endsection
