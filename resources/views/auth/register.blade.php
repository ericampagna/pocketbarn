@extends('layouts.mainlayout')
@section('title', 'Register')
@section('meta_description', 'Signup to start your pocket today.')
@section('content')
<section class="Signup" id="signup">
    <form class="Signup--form" role="form" method="POST" action="/register">
        {{ csrf_field() }}
        <h3>Sign up! Start finding your pocket today!</h3>
         <input id="name" type="text" class="input-full" name="name" value="{{ old('name') }}" placeholder="Full Name">
         @if ($errors->has('name'))
            <span class="help-block error">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
        <input id="email" type="email" class="input-full" name="email" value="{{ old('email') }}" placeholder="E-Mail Address">

        @if ($errors->has('email'))
            <span class="help-block error">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        <input id="password" type="password" class="input-full" name="password" placeholder="Password">

        @if ($errors->has('password'))
            <span class="help-block error">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        <input id="password_confirmation" type="password" class="input-full" name="password_confirmation" placeholder="Confirm Password">

        @if ($errors->has('password_confirmation'))
            <span class="help-block error">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif

        <div>
            <button type="submit" class="btn btn-purple">
                Sign Up!
            </button>
        </div>
    </form>
    <div class="Login--social white">
        <h4 class="text-center"><i>Or Continue with...</i></h4>
        <p class="text-center">We will never post without your permission</p>
        <a href="/redirect/facebook/" class="btn btn-large facebook-login"><i class="socicon-facebook"></i> Facebook</a>
        <a href="/redirect/twitter/" class="btn btn-large twitter-login"><i class="socicon-twitter"></i> Twitter</a>
        <a href="/redirect/google/" class="btn btn-large google-login"><i class="socicon-googleplus"></i> Google</a>
    </div>
    <div class="goToLogin">
        <a href="/login">
            <p>Login now</p>
        </a>
    </div>
</section>
@endsection
