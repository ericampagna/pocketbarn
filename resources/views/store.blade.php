@extends('layouts.mainlayout')

@section('title', 'mypocket')
@section('meta_desc', $store->story)
@section('ogUrl', secure_url($store->slug))
@section('ogImage', $store->profile_img)

@section('content')

<section class="Jumbotron">
	@if($store->cover_img)
		<img src="{{$store->cover_img}}">
	@else
		<div class="Jumbotron--Cover-Fill"><h1>{{$store->name}}</h1></div>
	@endif
</section>
<section class="container " id="pb-app">
<alert v-ref:alert-child  v-if="alert.show" :type="alert.type">@{{alert.message}}</alert>
	<div class="Store">
		<div class="Store--Info-Box">
			<div class="Store--Profile">
				<div class="Store--Profile-Pic">
					@if($store->profile_img)
						<img src="{{$store->profile_img}}" >
					@else
						<i class="pb-icon user ico-small"></i>
					@endif
				</div>

				<div class="Store--Contact-Info">
					<h5>{{$store->name}}</h5>
					<i class="pb-icon map-marker ico-small" style="float: left;"></i><p>{{$store->city}}, {{$store->state}}</p>
					<p @click='addToPocket("store", {{$store->id}})' class="addToPocketStore" >
						<img class="pocket-ico"  src="/assets/images/addtopocket.svg">
						Save Marketplace to my pocket
					</p>



				</div>
			</div>

			<div class="Store--Contact">
				<div style="float: right;">
					<a class="btn" @click="showModal = true" >Send Message</a><br />
					@if($store->phone)
					 	<a class="btn Store--Phone-Button" >Call Now</a>
						<i class="pb-icon phone ico-small" style="float: left;"></i><p>{{$store->phone}}</p>
					@endif
				</div>
				<div class="social-icons">
					{{-- <div class="fb-share-button" data-href="{{secure_url($item->store->slug .'/'.$item->slug)}}" data-layout="button" data-size="small" data-mobile-iframe="false"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div> --}}
					<!-- AddToAny BEGIN -->
					<!-- AddToAny BEGIN -->
					<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
						<a class="a2a_dd" href="https://www.addtoany.com/share?linkurl=www.mypocketbarn.com&amp;linkname="></a>
						<a class="a2a_button_facebook"></a>
						<a class="a2a_button_twitter"></a>
						<a class="a2a_button_google_plus"></a>
						<a class="a2a_button_pinterest"></a>
						<a class="a2a_button_email"></a>
					</div>
					<script>
					var a2a_config = a2a_config || {};
					a2a_config.linkurl = "{{secure_url($store->slug)}}";
					a2a_config.onclick = 1;
					</script>
					<script async src="https://static.addtoany.com/menu/page.js"></script>
							<!-- AddToAny END -->
				</div>
			</div>
		</div>

		<div class="Store--Story">
			<button class="accordion"><h4>Story</h4><i class="pb-icon angle-down"></i></button>
			<div class="panel">
			  {!! nl2br(e($store->story)) !!}
			</div>

			<button class="accordion"><h4>Policies</h4><i class="pb-icon angle-down"></i></button>
			<div class="panel">
			  {{ nl2br(e($store->policies)) }}
			</div>
		</div>

		<div class="Store--Gtk">
			<h6>Get to know the sellers</h6>
			@foreach($store->users as $seller)
				<div class="Store--Seller">
					@if($seller->avatar)
					<img src="{{$seller->avatar}}" class="avatar" />
					@else
						<i class="pb-icon user-white ico-xlarge ico-center avatar-filler"></i>
					@endif
					<p>{{$seller->name}}</p>
				</div>
			@endforeach
		</div>
	</div>

	<h3 class="our-items" style="display: block;">Our Items</h3>
	<section class="container home-listings store-listings" id="columns">

		<more-items store="{{ $store->id }}" page="store"></more-items>
	</section>
	@if(Auth::user())
		<modal v-show="showModal" send-message="sendMessage()">
		    <!--
		      you can use custom content here to overwrite
		      default content
		    -->
		  	<h3 slot="header">Send Seller A Message</h3>

		  	<div slot="body">
		  		<p>What whould you like to say to the user?</p>
		  		<textarea v-model.sync="messageToSeller"></textarea>
		  	</div>
		  	<div slot="footer">
		  		<button class="btn" @click="sendMessage">
	            Send
	          </button>
	          <button class="btn btn-tan" @click="$root.showModal = false">
	                Cancel
	              </button>
	        </div>
		  </modal>
	@else
		<modal v-show="showModal">
	    <!--
	      you can use custom content here to overwrite
	      default content
	    -->
	  	<h3 slot="header">Send Seller A Message</h3>

	  	<div slot="body">
	  		<p class="text-center">You must be logged in to start a conversation.</p>
	  		<br>
	  		<p class="text-center">Please <a href="/login">Login</a></p>
	  		<p class="text-center">Or</p>
	  		<p class="text-center">You can <a href="/register">Register</a>
	  		</p>
	  	</div>
	  	<div slot="footer">
          <button class="btn btn-tan" @click="$root.showModal = false">
                Ok
              </button>
        </div>
	  </modal>
	@endif
</section>

@endsection

@section ('footer')
<script>
	// Accordian
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
    }
}
</script>
@endsection