@extends('layouts.mainlayout')

@section('title', 'Support')

@section('content')
<section class="container account-page" id="pb-app-support" v-cloak>
<alert v-ref:alert-child  v-if="alert.show" :type="alert.type" >@{{alert.message}}</alert>
	<h1 id="top">Pocketbarn Support</h1>
	<div class="sidebar">
		<ul>
			<li @click="pageView = 'faq'"><i class="pb-icon help1 ico-small"></i> FAQs</li>
			<li @click="pageView = 'issue'"><i class="pb-icon attention ico-small"></i> Report an Issue</li>
			{{-- <li @click="pageView = 'contact'"><i class="pe-7s-mail"></i> Contact Pocketbarn</li> --}}
		</ul>
	</div>
	<section class="FAQs" v-show="pageView == 'faq'" transition="fade">
		<h3>FAQs</h3>
		<ul class="FAQs--list">
			<li><a href="#what-is-pocketbarn">What is Pocketbarn?</a></li>
			<li><a href="#does-it-cost-anything-to-buy-and-sell-on-pocketbarn">Does it cost anything to buy and sell on Pocketbarn?</a></li>
			<li><a href="#does-pocketbarn-get-a-percentage-of-my-marketplace-profits">Does Pocketbarn get a percentage of my marketplace profits?</a></li>
			<li><a href="#hat-is-the-my-pocket-icon">What is the “My Pocket” icon?</a></li>
			<li><a href="#how-do-i-purchase-something-on-pocketbarn">How do I purchase something on Pocketbarn?</a></li>
			<li><a href="#how-do-i-set-up-a-marketplace-to-sell-my-items-on-pocketbarn">How do I set up a marketplace to sell my items on Pocketbarn?</a></li>
			<li><a href="#how-do-i-find-items-that-are-for-sale-in-my-area">How do I find items that are for sale in my area?</a></li>
			<li><a href="#why-would-you-only-want-to-see-items-that-are-for-sale-nearby">Why would you only want to see items that are for sale nearby?</a></li>
			<li><a href="#can-i-share-items-to-other-social-media-sites">Can I share items to other social media sites?</a></li>
			<li><a href="#what-do-the-icons-on-posted-pictures-represent">What do the icons on posted pictures represent?</a></li>
			<li><a href="#how-do-i-get-back-to-the-home-screen">How do I get back to the home screen?</a></li>
			<li><a href="#how-do-i-get-people-to-see-the-items-i-am-selling-in-my-marketplace">How do I get people to see the items I am selling in my marketplace?</a></li>
			<li><a href="#what-does-it-mean-to-boost-a-post">What does it mean to boost a post?</a></li>
			<li><a href="#how-do-i-respond-to-messages-from-buyers-and-sellers">How do I respond to messages from buyers and sellers?</a></li>
			<li><a href="#what-items-will-i-see-in-my-feed-if-i-do-not-search-in-a-certain-location-or-for-something-specific">What items will I see in my feed if I do not search in a certain location or for something specific?</a></li>
		</ul>
		<div class="faq" id="what-is-pocketbarn">
			<h4>What is Pocketbarn?</h4>
			<p>Pocketbarn is a platform to connect buyers and sellers in local communities  and all across the nation.  Pocketbarn is a service that we have created to be a tool for people who make and sell handmade or homegrown items, and also for people who want to purchase those things. Pocketbarn is NOT a yard sale site, and we do not allow “yard sale”  type items to be advertised on the site.</p>
			<a href="#top">^ Back to Top</a>
		</div>
		<div class="faq" id="does-it-cost-anything-to-buy-and-sell-on-pocketbarn">
			<h4>Does it cost anything to buy and sell on Pocketbarn?</h4>
			<p>Pocket barn is a free service to you.  Every Pocketbarn marketplace is equipped with three selling spots for you to post your items that you have for sale.  Those items are interchangeable and can be replaced as often as you would like at no charge.  If you would like to expand your selling space, we offer inexpensive plans that will help you create a marketplace to fit your needs.  You can locate the plan options under the “marketplace” bag icon where it says “my plan”.</p>
			<a href="#top">^ Back to Top</a>
		</div>
		<div class="faq" id="does-pocketbarn-get-a-percentage-of-my-marketplace-profits">
			<h4>Does Pocketbarn get a percentage of my marketplace profits?</h4>
			<p>Pocketbarn will NEVER take your profits!  We want everyone’s small business to be a success, and we believe your profits belong to you. </p>
			<a href="#top">^ Back to Top</a>
		</div>
		<div class="faq" id="what-is-the-my-pocket-icon">
			<h4>What is the “My Pocket” icon?</h4>
			<p>The “My Pocket” icon, located at the top of the screen, is a place for you to store items that you would like to look at later.  No one, other than you, can see what you put in your pocket.  Place items in your pocket and remove them as often as you would like. </p>
			<a href="#top">^ Back to Top</a>
		</div>
		<div class="faq" id="how-do-i-purchase-something-on-pocketbarn">
			<h4>How do I purchase something on Pocketbarn?</h4>
			<p>If you would like to purchase something on pocketbarn you will need to contact the seller.  No transactions will be made through the actual pocketbarn site.  The seller will let you know their preferred method of payment. For example, if it is a local seller, you might pay upon pickup, or they might be able to send you an invoice through your email.  Sellers that ship across the United States might have you pay in advance through a service they use, such as PayPal or Stripe. </p>
			<a href="#top">^ Back to Top</a>
		</div>
		<div class="faq" id="how-do-i-set-up-a-marketplace-to-sell-my-items-on-pocketbarn">
			<h4>How do I set up a marketplace to sell my items on Pocketbarn?</h4>
			<p>Setting up a marketplace on Pocketbarn is easy.  Find the “Marketplace” tab, and then follow the directions from there.  You will need to have a name that you want your business to be called, pictures of the sellers, and photos of your items that you have for sale.  You can post items that are already made and items that are made to order.  </p>
			<a href="#top">^ Back to Top</a>
		</div>
		<div class="faq" id="how-do-i-find-items-that-are-for-sale-in-my-area">
			<h4>How do I find items that are for sale in my area?</h4>
			<p>If you only want to view items that are for sale in your area, you can simply put in your zip code.  All items for sale within a 50 mile radius will appear.</p>
			<a href="#top">^ Back to Top</a>
		</div>
		<div class="faq" id="why-would-you-only-want-to-see-items-that-are-for-sale-nearby">
			<h4>Why would you only want to see items that are for sale nearby?</h4>
			<p>Some items that people create, are not items that can be shipped.  For example, if you would like to purchase hand made furniture, you might be able to find someone in your area that makes furniture, but who is not equipped to ship those items across the country.  </p>
			<p>Many people like to buy handmade items from people they know and take pride in items that are available in their own communities.  At Pocketbarn, we love to see communities grow and change.  With the use of Pocketbarn services, you can find people in your neighborhoods that create or grow items that you need and want.  This is a great way to support community growth and involvement. </p>
			<a href="#top">^ Back to Top</a>
		</div>
		<div class="faq" id="can-i-share-items-to-other-social-media-sites">
			<h4>Can I share items to other social media sites?</h4>
			<p>YES!  We want you to share your items that you post or love to other social media sites.  You will find social media buttons when you click on a posted item.  Sharing to social media will get more traffic to your marketplace and let your friends know where they can find your items.</p>
			<a href="#top">^ Back to Top</a>
		</div>
		<div class="faq" id="what-do-the-icons-on-posted-pictures-represent">
			<h4>What do the icons on posted pictures represent?</h4>
			<p>When you click on an item to view it, you will see several different icons.  Here is what each one represents:
			<ul>
				<li><i class="pb-icon truck"></i> -  local delivery</li>
				<li><i style="font-size: 30px;" class="pe-7s-box1"></i> - shipping available
				<li><i class="pb-icon shoe"></i> - local pickup
				<li><i class="pb-icon storefront"></i> - brick and mortar store location
			</ul>
			<a href="#top">^ Back to Top</a>
		</div>
		<div class="faq" id="how-do-i-get-back-to-the-home-screen">
			<h4>How do I get back to the home screen?</h4>
			<p>From anywhere in the site, you can always click on the Pocketbarn logo to take you back to the beginning.  </p>
			<a href="#top">^ Back to Top</a>
		</div>
		<div class="faq" id="how-do-i-get-people-to-see-the-items-i-am-selling-in-my-marketplace">
			<h4>How do I get people to see the items I am selling in my marketplace?</h4>
			<p>When you post an item for sale in your marketplace, that item becomes a part of the feed.  Your post will appear at the top of the feed until other posts are made.  If you would like your item to appear more often in the feed, we offer a “boost post” option where your item will appear multiple times in the feed.  We offer this service for just $3 a day per post.  You can choose the length of time each post is boosted.</p>
			<a href="#top">^ Back to Top</a>
		</div>
		<div class="faq" id="what-does-it-mean-to-boost-a-post">
			<h4>What does it mean to boost a post?</h4>
			<p>When you boost a post, it means that you are giving buyers opportunities to see your post more often than others.  This is a great advertising tool because you can use it as often as you like to get as much visibility as you choose. </p>
			<a href="#top">^ Back to Top</a>
		</div>
		<div class="faq" id="how-do-i-respond-to-messages-from-buyers-and-sellers">
			<h4>How do I respond to messages from buyers and sellers?</h4>
			<p>If you are a buyer, you will find your messages under the “my profile” icon on the home screen.  They are listed under “conversations”.    If you are a seller, you can also find your messages under the “my profile” icon, but they are also located in your “marketplace”. </p>
			<a href="#top">^ Back to Top</a>
		</div>
		<div class="faq" id="what-items-will-i-see-in-my-feed-if-i-do-not-search-in-a-certain-location-or-for-something-specific">
			<h4>What items will I see in my feed if I do not search in a certain location or for something specific?</h4>
			<p>When your feed comes up, it shows everything that has been posted to the Pocketbarn site, regardless of the topic or area.  You are able to choose specific locations and items to optimize your search.  For example, if you want to search for cupcakes, simply search “cupcakes” and any cupcakes that have been posted to Pocketbarn will show up.  But, if you want to search for cupcakes in the St. Louis area, then you would need to fill in both fields to narrow your search to that specific area.  Now, only cupcakes that were posted in the St. Louis area will appear in your feed.</p>
			<a href="#top">^ Back to Top</a>
		</div>
	</section>

	<section class="Issue" v-show="pageView == 'issue'" transition="fade">
		<h3><i class="pb-icon attention ico-large"></i> Report an Issue</h3>
        <p>Date: @{{issue.date}}</p>
        <p>What page is the issue on?</p>
        <input type="text" v-model.sync="issue.page">
        <p>What type of issue are you experiencing?</p>
        <select v-model.sync="issue.type">
            <option value="">Choose a type...</option>
            <option value="design">Design Issue</option>
            <option value="form">Form not working</option>
            <option value="error">Error Screen</option>
            <option value="other">Other</option>
        </select>
        <p>Please describe the issue...</p>
        <textarea v-model.sync="issue.description"></textarea>
        <button class="btn" @click="submitIssue()">
        Submit
      </button>
	</section>
	{{-- <section class="Contact" v-show="pageView == 'contact'" transition="fade"> --}}


</section>

@endsection

@section ('footer')
@endsection