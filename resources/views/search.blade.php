@extends('layouts.mainlayout')

@section('title', 'Search')

@section('content')
<section id="pb-app" v-cloak>
	<alert v-ref:alert-child  v-if="alert.show" :type="alert.type">@{{alert.message}}</alert>
	<search></search>
</section>


@endsection
