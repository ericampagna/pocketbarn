
<div class="sidebar">
		<ul>
			<li><a href="/store/"><i style="width: 1rem; height: 1rem; opacity: 50%;" class="pb-icon shopbag"></i> Marketplace Info</a></li>
			<li><a href="/store/items"><i class="pb-icon ticket ico-small"></i> Items</a></li>
			<li><a href="/conversations"><i class="pb-icon comment ico-small"></i> Conversations</a><notification type="store"></notification></li>
			{{-- <li><a href="/store/account" ><i class="pe-7s-config"></i> Account Settings</a></li> --}}
			<li><a href="/store/sellers" ><i class="pb-icon users ico-small"></i> My Sellers</a></li>
			{{-- <li><a href="/store/conversations"><i class="pe-7s-comment"></i> Marketplace Conversations</a></li> --}}
			<li><a href="/store/plan"><i class="pb-icon wallet ico-small"></i> My Plan</a></li>
			<li><a href="/store/payment"><i class="pb-icon credit ico-small"></i> Update Payment Info</a></li>
			<li><a href="/store/boosted-items"><i class="pb-icon star ico-small"></i> Boosted Items</a></li>
			<li><a href="/support"><i class="pb-icon help1 ico-small"></i> Support</a></li>
		</ul>
	</div>
