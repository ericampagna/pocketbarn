@extends('layouts.mainlayout')

@section('title', 'Create Marketplace')
@section('meta_desc', 'Create your marketplace on pocketbarn')
@section('content')
<section class="container account-page" id="pb-app" v-cloak>
	@include('user.sidebar')
		<div class="Store Form">
			<h1>Create my Marketplace</h1>
			<form method="POST" action="{{route('store.store')}}" class="" id="marketplace-form">
				{{ method_field('PUT') }}
				{{ csrf_field() }}
				<div class="Form--input-group">
					*<input class="Form--input" type="text" name="store_name" placeholder="Marketplace Name" value="{{ old('store_name') }}">
					<span class="Form__error">{{ $errors->first('store_name') }}</span>
				</div>
				<div class="Form--input-group">
					*<input class="Form--input" type="text" name="address_line1" placeholder="Steet Address" value="{{ old('address_line1') }}">
					<span class="Form__error">{{ $errors->first('address_line1') }}</span>
				</div>
				<div class="Form--input-group">
					*<input class="Form--input" type="text" name="address_city" placeholder="City" value="{{ old('address_city') }}">
					<span class="Form__error">{{ $errors->first('address_city') }}</span>
				</div>
				<div class="Form--input-group">
					*<select class="Form--input" type="text" name="address_state">
						@foreach($states as $ab => $name)
							<option {!! (old('address_state') == $ab ? 'selected' : '' ) !!} value="{{ $ab }}">{{ $name }}</option>
						@endforeach
					</select>
					<span class="Form__error">{{ $errors->first('address_state') }}</span>
				</div>
				<div class="Form--input-group">
					*<input class="Form--input" type="text" name="address_zip" placeholder="Zipcode" value="{{ old('address_zip') }}">
					<span class="Form__error">{{ $errors->first('address_zip') }}</span>
				</div>
				<h3>Choose Your Plan</h3>
				<div class="Plans">
					@foreach($plans as $plan)

						<div class="Plan">
							<div class="Plan--top">
								<h5 class="Plan--name">{{ $plan->name }}</h5>
								<p class="Plan--price">@if($plan->monthly) ${{ $plan->monthly}}/mo  @else FREE @endif</p>
							</div>
							<div class="Plan--Description">
							<ul>
								{!! $plan->description !!}
							</ul>
							</div>
							<div class="Plan__choose">
								<div class="Plan--option">
									<input type="radio" name="plan" v-model="plan" value="{{$plan->stripe_id}}" {!! (old('plan') == $plan->stripe_id ? 'checked' : '' ) !!} >
									<label>@if($plan->monthly) ${{ $plan->monthly}}/mo @else FREE @endif</label>
								</div>
								<div class="Plan--option">
									@if($plan->stripe_id != 'free_new' )
										<input type="radio" name="plan" v-model="plan" value="{{$plan->stripe_id}}_year" {!! (old('plan') == $plan->stripe_id ? 'checked' : '' ) !!} >
										<label>${{ $plan->yearly}}/yr (Save ${!! round(($plan->monthly*12) - $plan->yearly) !!})</label>
									@endif
								</div>

							</div>
						</div>


					@endforeach
				</div>
				<div class="Form--input-group Plan--type"  v-if="plan != 'free_new'" >
					<h5>How would you like to pay?</h5>
					@foreach($plans as $plan)
						@if($plan->stripe_id != 'free_new')
							<div v-if="plan === '{{$plan->stripe_id}}' || plan === '{{$plan->stripe_id}}_year'" >
								<div class="Form--radio-group">
									<input class="Form--radio" type="radio" name="plan_type"  {!! (old('plan_type') == true ? 'checked' : '' ) !!} value="monthly">
									<label>Monthly ${{$plan->monthly}}</label>
								</div>
								<div class="Form--radio-group">
									<input class="Form--radio" type="radio"  name="plan_type" {!! (old('plan_type') == true ? 'checked' : '' ) !!} value="yearly">
									<label>Yearly ${{$plan->yearly}} (Save ${!! round(($plan->monthly*12) - $plan->yearly) !!})</label>
								</div>
							<span class="Form__error">{{ $errors->first('show_address') }}</span>
							</div>
						@endif
					@endforeach
				</div>
				<div v-if="plan != 'free_new'">
					<h3>Payment Info</h3>
					<p class="payment-errors error animated"></p>
					<div class="Form--input-group">
						<label>Credit card number</label>
						<input class="Form--input masked" type="tel" id="ccn" name="ccn" placeholder="XXXX XXXX XXXX XXXX" data-stripe="number" value="{{ old('ccn') }}">
						<span class="Form__error">{{ $errors->first('ccn') }}</span>
					</div>
					<div class="Form--input-group exp">
						{{-- <label>Expriation date</label> --}}
						<select class="Form--input exp-month" type="text" name="exp-month" data-stripe="exp-month">
							@foreach($months as $month => $month_ab)
								<option {!! (old('exp-month') == $month_ab ? 'selected'  : '' ) !!} value="{{ $month }}">{{ $month_ab }}</option>
							@endforeach
						</select>
						<select class="Form--input exp-year" type="text" name="exp-year" data-stripe="exp-year">
							@foreach($years as $year)
								<option {!! (old('exp-year') == $year ? 'selected'  : '' ) !!} value="{{ $year }}">{{ $year }}</option>
							@endforeach
						</select>
						<span class="Form__error">{{ $errors->first('exp-year') }}</span>
					</div>
					<div class="Form--input-group">
						<label>CVC</label>
						<input class="Form--input" type="text" name="cvc" placeholder="123" data-stripe="cvc" value="{{ old('cvc') }}">
						<span class="Form__error">{{ $errors->first('cvc') }}</span>
					</div>
				</div>
				<div class="Form--input-group">
					<button class="btn" @click.prevent="createStore()" type="submit">Create Marketplace</button>
				</div>
				<small>* required information</small>
			</form>
			<div class="Form--hints">
			</div>
		</div>

</section>


@endsection

@section ('footer')
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
@endsection