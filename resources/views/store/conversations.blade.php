@extends('layouts.mainlayout')

@section('title', 'My Marketplace Conversations ')

@section('content')
<section class="container account-page" id="pb-app-conversations">
 @if(session()->has('status'))
        <alert {{ session('status_type') }}>{{ session('status') }}</alert>
    @endif
	@include('store.sidebar')
	<div class="Store">
	<a href="/conversations"><i class="pe-7s-angle-left"></i> Back to Conversations</a>
			<conversations :user="{{ $user }}" :store="{{ $user->store_id }}"></messages>

	</div>
</section>

@endsection

@section ('footer')
<script>
	// Accordian
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
    }
}
</script>
@endsection