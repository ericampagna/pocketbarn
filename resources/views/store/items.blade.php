@extends('layouts.mainlayout')

@section('title', 'Marketplace Items')

@section('content')
<section class="container account-page" id="pb-app-store">
	@include('store.sidebar')
	@if($store->subscribedToPlan('est', 'main') || $store->subscribedToPlan('est_year', 'main'))
		@if($items->count() > 0 )
			<a href="/store/add-item" class="btn"><i class="pb-icon plus ico-small"></i> New Item</a>
		@endif
	@endif
		@if (count($errors) > 0)
		   <alert type=error>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </alert>
		@endif
		<div class="Store Items">
		<h3>{{ $store->name }} Items</h3>
			{{-- @if(count($items) > 0) --}}
			<div class="Cards">
				@foreach($items as $item)

					<div class="Card storeCard animated fadeInUpBig"  transition="slideUp">
						@if($item->status == 'active')
			      			<p class="Card--status active"><i class="pb-icon check ico-small"></i> Active</p>
			      		@elseif($item->status == 'in-active')
			      			<p class="Card--status in-active"><i class="pb-icon diskette ico-small"></i> Saved</p>
			      		@endif
						
					    	<div class="Card--image">
					    		<a href="/item/{{$item->id}}" class="Card--link" >
						    		@if($item->featured_image)
						      			<img src="{{$item->featured_image}}">
						      		@else
						      			<img src="/assets/images/no-featured.jpg">
						      		@endif
					      		</a>
					      	</div>
					      </a>

					    	<div class="Card--action">
					      		<a href="/item/{{$store->slug}}/{{$item->id}}/{{$item->slug}}" class="Card--link" >
					      			<span class="Card--title">{{$item->title}}</span>
					      		</a>
					      		<a href="#" class="Card--location"><i class="pb-icon map-marker ico-small"></i>{{ $store->city }}, {{ $store->state }}</a>
					    	</div>
					  	</div>

					 @endforeach
					 @for ($i = 0; $i < $blanks; $i++)
					 	<div class="Card storeCard animated fadeInUpBig"  transition="slideUp">
						
					    	<div class="Card--image">
					    		<a href="/store/add-item" class="Card--link" >
					      			<img src="/assets/images/new-item.jpg">
					      		</a>
					      	</div>
					     

					    	<div class="Card--action">
					      		<a href="/store/add-item" class="Card--link" >
					      			<span class="Card--title">Add New Item</span>
					      		</a>
					      		<a href="#" class="Card--location"><i class="pb-icon map-marker ico-small"></i>{{ $store->city }}, {{ $store->state }}</a>
					    	</div>
					  	</div>
					 @endfor
			</div>
			@if($limitReached)
			<div class="Items--limit-reached">
				<p><i class="pe-7s-attention"></i></p>
				<div class="item-alert">
					<h4>You have reached your item Limit.</h4>
					<h5>To add more items, please <a href="/store/plan">upgrade your account</a></h5>
				</div>
			</div>
			@endif
			{{-- For Unlimted Plans --}}
			@if($store->subscribedToPlan('est', 'main') || $store->subscribedToPlan('est_year', 'main'))
				@if($items->count() <= 0 )
				<div class="item-alert">
					<h4>You do not have any items yet. <a href="/store/add-item">Add some now!</a></h4>
				</div>
				@endif
				<a href="/store/add-item" class="btn"><i class="pb-ico check ico-small"></i> New Item</a>
			@endif
		</div>

</section>


@endsection

@section ('footer')
@endsection