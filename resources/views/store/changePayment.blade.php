@extends('layouts.mainlayout')

@section('title', 'Update Payment Type')

@section('content')
<section class="container account-page" id="pb-app-store" v-cloak>
	@include('store.sidebar')
		@if (count($errors) > 0)
		   <alert type=error>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </alert>
		@endif
		<div class="Store Form">
			<h3>Change your Payment</h3>
			<div class="CreditCard">
			<h4>Current Payment </h4>
			<p class="cc_num">XXXX XXXX XXXX {{$store->card_last_four}}</p>
			<p class="cc_type">{{$store->card_brand}}</p>
			</div>
			<form method="POST" action="/store/payment/{{$store->id}}" class="Plans--Form" id="marketplace-form" enctype="multipart/form-data">
				{{ method_field('PATCH') }}
				{{ csrf_field() }}
				<h3>New Payment Info</h3>
				<p class="payment-errors error animated"></p>
				<div class="Form--input-group">
					<label>Credit card number</label>
					<input class="Form--input masked" type="tel" id="ccn" name="ccn" placeholder="XXXX XXXX XXXX XXXX" data-stripe="number" value="{{ old('ccn') }}">
					<span class="Form__error">{{ $errors->first('ccn') }}</span>
				</div>
				<div class="Form--input-group exp">
					{{-- <label>Expriation date</label> --}}
					<select class="Form--input exp-month" type="text" name="exp-month" data-stripe="exp-month">
						@foreach($months as $month => $month_ab)
							<option {!! (old('exp-month') == $month_ab ? 'selected'  : '' ) !!} value="{{ $month }}">{{ $month_ab }}</option>
						@endforeach
					</select>
					<select class="Form--input exp-year" type="text" name="exp-year" data-stripe="exp-year">
						@foreach($years as $year)
							<option {!! (old('exp-year') == $year ? 'selected'  : '' ) !!} value="{{ $year }}">{{ $year }}</option>
						@endforeach
					</select>
					<span class="Form__error">{{ $errors->first('exp-year') }}</span>
				</div>
				<div class="Form--input-group">
					<label>CVC</label>
					<input class="Form--input" type="text" name="cvc" placeholder="123" data-stripe="cvc" value="{{ old('cvc') }}">
					<span class="Form__error">{{ $errors->first('cvc') }}</span>
				</div>
				<div class="Form--input-group">
					<button class="btn" type="submit">Update Payment</button>
				</div>
			</form>

		</div>

</section>


@endsection

@section ('footer')
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
Stripe.setPublishableKey(window.stripeKey);
    jQuery(function($) {
        $('#marketplace-form').submit(function(event) {
        	event.preventDefault();
            var $form = $(this);
            $form.find('button').prop('disabled', true);
            Stripe.card.createToken($form, stripeResponseHandler);

            return false;
        });
    });

    var stripeResponseHandler = function(status, response) {
        var $form = $('#marketplace-form');

        if (response.error) {
            $form.find('.payment-errors').text(response.error.message).addClass('flash');
            $form.find('button').prop('disabled', false);
        } else {
            var token = response.id;
            $form.append($('<input type="hidden" name="stripeToken" />').val(token));
            $form.get(0).submit();
        }
    };
   </script>
@endsection