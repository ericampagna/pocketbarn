@extends('layouts.mainlayout')

@section('title', 'Marketplace Sellers')

@section('content')
<section class="container account-page" id="pb-app-store">
	<alert v-ref:alert-child  v-if="alert.show" :type="alert.type">@{{alert.message}}</alert>
	@include('store.sidebar')
		<div class="Store Sellers">
			<h3>{{ $store->name }} Sellers</h3>
			@foreach($store->users as $user)
				<div class="Seller">
					@if($user->avatar)
						<img src="{{ $user->avatar }}" class="Avatar">
					@else
						<i class="pe-7s-user avatar-filler"></i>
					@endif
					<h5>{{ $user->name }}</h5>
					@if($user->id == $store->owned_by)
					<span>Owner</span>
					@else
					<a class="Seller__make-owner" @click="makeOwner">Make Owner</a>
					@endif
					<button class="btn Seller__remove" @click="removeSeller({{$user->id}})" ><i class="pb-icon close-circle ico-small"></i> Remove Seller</button>
				</div>
			@endforeach
			<br>
			<button class="btn" @click="showModal = true" ><i class="pb-icon plus ico-small"></i> Add Seller</button>
		</div>
		{{-- Show if Submitting  --}}
		<div v-if="submitted">
			<div class="loading_overlay">
				<i class="pe-7s-refresh pe-spin"></i>
			</div>
		</div>
		<modal v-if="showModal" add-seller="addSeller()">
	    <!--
	      you can use custom content here to overwrite
	      default content
	    -->
	  	<h3 slot="header">Add New Seller</h3>

	  	<div slot="body">
	  		<p>Type the email of the user you would like to invite as a seller in your Marketplace</p>
	  		<input v-model.sync="sellerEmail" placeholder="user@email.com">
	  	</div>
	  	<div slot="footer">
	  		<button class="btn" @click="addSeller">
            Send
          </button>
          <button class="btn btn-tan" @click="$root.showModal = false">
                Cancel
              </button>
        </div>
	  </modal>
</section>


@endsection

@section ('footer')
@endsection