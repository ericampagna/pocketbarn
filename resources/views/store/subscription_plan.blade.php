@extends('layouts.mainlayout')

@section('title', 'Update Marketplace Plan')

@section('content')
<section class="container account-page" id="pb-app-store">
	@include('store.sidebar')
		@if (count($errors) > 0)
		   <alert type=error>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </alert>
		@endif
		<div class="Store Form">
		@if(!$store->card_last_four || !$store->card_brand)
			{{-- <div class="needPayment"> --}}
				<div class="item-alert">
					 		<h4>You will need to add a payment option before upgrading your plan.</h4>
					 		<a href="/store/payment" class="btn"><i class="pe-7s-credit"></i> Add Payment Info</a>
					 	</div>
			{{-- </div> --}}
		@endif
			<h3>Change your Plan</h3>
			<form method="POST" action="/store/plan/{{$store->id}}" class="Plans--Form" enctype="multipart/form-data">
				{{ method_field('PATCH') }}
				{{ csrf_field() }}
				<div class="Plans">
					@foreach($plans as $plan)

						<div class="Plan @if($store->subscribedToPlan($plan->stripe_id, 'main') || $store->subscribedToPlan($plan->stripe_id.'_year', 'main'))Current @endif">
							<div class="Plan--top">
								<h5 class="Plan--name">{{ $plan->name }}</h5>
								<p class="Plan--price">@if($plan->monthly) ${{ $plan->monthly}}/mo @else FREE @endif</p>
							</div>
							<div class="Plan--Description">
							<ul>
								{!! $plan->description !!}
							</ul>
							</div>
							<div class="Plan__choose">
								<div class="Plan--option">
									<input type="radio" name="plan" value="{{$plan->stripe_id}}" @if($store->subscribedToPlan($plan->stripe_id, 'main'))disabled @endif">
									<label>@if($plan->monthly) ${{ $plan->monthly}}/mo @else FREE @endif</label>
								</div>
								<div class="Plan--option">
									@if($plan->stripe_id != 'free')
										<input type="radio" name="plan" value="{{$plan->stripe_id}}_year" @if($store->subscribedToPlan($plan->stripe_id.'_year', 'main'))disabled @endif">
										<label>${{ $plan->yearly}}/yr (Save ${!! round(($plan->monthly*12) - $plan->yearly) !!})</label>
									@endif
								</div>

							</div>
						</div>
						@if($store->subscribedToPlan($plan->stripe_id, 'main') || $store->subscribedToPlan($plan->stripe_id.'_year', 'main'))<input type="hidden" name="current_plan" value="{{$plan->stripe_id}}"> @endif

					@endforeach
				</div>
				<div class="Form--input-group">
					<button class="btn" type="submit">Update Plan</button>
				</div>
				<a style="float:right;" href="#" @click="cancelModal">Close Marketplace</a>
			</form>

		</div>
		<modal v-show="showModal" >
		    <!--
		      you can use custom content here to overwrite
		      default content
		    -->
		  	<h3 slot="header"><i class="pe-7s-attention"></i> Close Marketplace</h3>

		  	<div slot="body">
		  		<p>Are you sure you want close you marketplace? Your Marketpalce will be permantly removed.</p>
		  		<br>
		  		<p>You could choose to change your plan to free plan</p>
		  		<br>
		  		<button class="btn" @click="goToFree">
	              Change to Free Plan
	           </button>
		  	</div>
		  	<div slot="footer">
		  		<button class="btn btn-red" @click="deleteMarketplace">
	            Yes, Remove my Marketplace
	          </button>
	          <button class="btn btn-tan" @click="$root.showModal = false">
	               No, Cancel
	           </button>
	        </div>
		  </modal>
</section>


@endsection

@section ('footer')
@endsection