@extends('layouts.mainlayout')

@section('title', 'Update Marketplace')

@section('content')
<section class="container account-page" id="pb-app-store">
	@include('store.sidebar')
		@if (count($errors) > 0)
		   <alert type=error>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </alert>
		@endif
		<div class="Store Form">
			<a href="/marketplace/{{$store->id}}/{{$store->slug}}" class="btn Store__view-store"><i class="pe-7s-back-2"></i> View Store Page</a>
			<h3>{{ $store->name }} Info</h3>
			<form method="POST" action="/store/{{$store->id}}" class="" enctype="multipart/form-data">
				{{ method_field('PATCH') }}
				{{ csrf_field() }}
				<div class="Form--input-group">
					*<input class="Form--input" type="text" name="store_name" value="{{ (old('store_name')) ?: $store->name }}">
					<span class="Form__error">{{ $errors->first('store_name') }}</span>
				</div>
				<div class="Form--input-group">
					*<input class="Form--input" type="text" name="street"  value="{{ (old('street')) ?: $store->street }}">
					<span class="Form__error">{{ $errors->first('street') }}</span>
				</div>
				<div class="Form--input-group">
					*<input class="Form--input" type="text" name="city"  value="{{ (old('city')) ?: $store->city }}">
					<span class="Form__error">{{ $errors->first('city') }}</span>
				</div>
				<div class="Form--input-group">
					*<select class="Form--input" type="text" name="state">
						@foreach($states as $ab => $name)
							<option {!! ($store->state == $ab || old('state') == $ab ? 'selected'  : '' ) !!} value="{{ $ab }}">{{ $name }}</option>
						@endforeach
					</select>
					<span class="Form__error">{{ $errors->first('state') }}</span>
				</div>
				<div class="Form--input-group">
					*<input class="Form--input" type="text" name="zip" placeholder="Zipcode" value="{{ (old('zip')) ?: $store->zip }}">
					<span class="Form__error">{{ $errors->first('zip') }}</span>
				</div>
				<div class="Form--input-group">
					*<input class="Form--checkbox" type="checkbox" name="show_address" {!! ($store->show_address == 1 || old('show_address') == true ? 'checked' : '' ) !!} value="1">
					<span class="Form__error">{{ $errors->first('show_address') }}</span>
					Show my address on marketplace page
				</div>
				<div class="Form--input-group">
					<input class="Form--input" type="text" name="phone" placeholder="Phone Number" value="{{ (old('phone')) ?: $store->phone }}">
					<span class="Form__error">{{ $errors->first('phone') }}</span>
				</div>
				<div class="Form--input-group">
					<input class="Form--input" type="text" name="website" placeholder="http://mysite.com" value="{{ (old('website')) ?: $store->website }}">
					<span class="Form__error">{{ $errors->first('website') }}</span>
				</div>
				<div class="Form--input-group">
					<h5>Tell us your story...</h5>
					<textarea rows="10" cols="100" class="Form--textarea" name="story">{{ (old('story')) ?: $store->story }} </textarea>
					<span class="Form__error">{{ $errors->first('story') }}</span>
				</div>
				<div class="Form--input-group">
					<h5>Your Polices</h5>
					<textarea rows="10" cols="100" class="Form--textarea" name="policies">{{ (old('policies')) ?: $store->policies }} </textarea>
					<span class="Form__error">{{ $errors->first('policies') }}</span>
				</div>
				<div class="Form--input-group Form--file">
					<h5>Marketplace Profile Photo</h5>
					<single-image-upload dbimage="{{$store->profile_img}}" shape="circle", width=300 height=300 place="store"></single-image-upload>
					<p>An image 300px X 300px will work the best</p>
				</div>
				<div class="Form--input-group Form--file">
					<h5>Marketplace Photo</h5>
					<single-image-upload dbimage="{{$store->cover_img}}" shape="square", width=1920 height=800 place="cover"></single-image-upload>
					<p>An image 1920px X 800px will look the best</p>
				</div>
				<div class="Form--input-group">
					<button class="btn" type="submit" v-show="imageSaved">Update Marketplace</button>
				</div>
				<small>* required information</small>
			</form>
			<div class="Form--hints">
			</div>
		</div>

</section>


@endsection

@section ('footer')
@endsection