@extends('emails.layout')

@section('title', 'You have a message from a pocketbarn user')
@section('content')


  <body class="">
    <table border="0" cellpadding="0" cellspacing="0" class="body">
      <tr>
        <td>&nbsp;</td>
        <td class="container">
          <div class="content">

            <!-- START CENTERED WHITE CONTAINER -->
            <span class="preheader">You have a message from a pocketbarn user</span>
            <table class="main">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper">
                  <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td>
                        <img src="https://s3-us-west-2.amazonaws.com/pocketbarn/assets/Pocketbarn_logo.png" class="logo">
                        <p>Hi {{ $receiver->name }},</p>
                        <p>A pocketbarn user has sent you a message.</p>
                        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                          <tbody>
                            <tr>
                              <td align="left">
                                <table border="0" cellpadding="0" cellspacing="0">
                                  <tbody>
                                    <tr>
                                      <p style="font-weight: bold;">From: {{ $sender->name }}</p>
                                      <p>{!! $content !!}</p>
                                      <a href="https://www.mypocketbarn.com/conversations" target="_blank">Reply</a>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

              <!-- END MAIN CONTENT AREA -->
              </table>

            <!-- START FOOTER -->
            <div class="footer">
              <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="content-block">
                    <span class="apple-link">pocketbarn, 306 N Market St, Mount Carmel, IL 62863</span>
                  </td>
                </tr>
              </table>
            </div>

            <!-- END FOOTER -->

<!-- END CENTERED WHITE CONTAINER --></div>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </body>
</html>