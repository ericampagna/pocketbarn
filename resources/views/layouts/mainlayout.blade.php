<!DOCTYPE html>
<html>
    <head>
    <script async  src="https://use.typekit.net/ywq2vat.js"></script>
        <script>
          (function(d) {
            var config = {
              kitId: 'ywq2vat',
              scriptTimeout: 3000,
              async: true
            },
            h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
          })(document);
        </script>
        <title>@yield('title') - pocketbarn</title>
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" value="{{ csrf_token() }}" id="token" />
        <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/icons/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="/assets/images/icons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/assets/images/icons/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="/assets/images/icons/manifest.json">
        <link rel="mask-icon" href="/assets/images/icons/safari-pinned-tab.svg" color="#4a4a56">
        <meta name="apple-mobile-web-app-title" content="pocketbarn">
        <meta name="application-name" content="pocketbarn">
        <meta name="theme-color" content="#fcfdfc">
        <meta name="description" content="@yield('meta_desc')">
        <meta name="keywords" content="@yield('meta_keywords')">
        <meta property="og:url" content="@yield('ogUrl', 'https://www.mypocketbarn.com/')" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="@yield('title')" />
        <meta property="og:description" content="@yield('meta_desc')" />
        <meta property="og:image" content="@yield('ogImage', 'https://www.mypocketbarn.com/assets/images/pocketbarn.jpg')" />
        <meta property="fb:app_id" content="1800242720253879">
        <link href="https://fonts.googleapis.com/css?family=Muli:300,400|Handlee" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ elixir('/assets/css/main.css')}}">

        @yield('header')
    </head>
    <body>
       @include('layouts.googleAnalytics')
       @include('layouts.facebookShare')
        <header>
        <div class="logo-wrapper">
        <a class="logo" href="/">
            <img src="/assets/images/pocketbarn-icon.svg">
            <span>pocketbarn</span>
        </a>
    </div>
    <nav class="icon-menu" id="pb-app-header">
        <div class="menu-wrapper">
            <!-- <a href="/"><span class="pe-7s-home"></span><span class="menu-text">Home</span></a> -->
            <a href="/search"><span class="pb-icon search-icon"></span><span class="menu-text">Search</span><div class="link_grow"></div></a>
            @if(Auth::guest())
                <a href="/mypocket"><span class="pb-icon my-pocket"></span><span class="menu-text">My Pocket</span><div class="link_grow"></div></a>
                <a href="/login"><span class="pb-icon user"></span><span class="menu-text">Login</span><div class="link_grow"></div></a>
                <a href="/register"><span class="pb-icon signin"></span><span class="menu-text">Sign up</span><div class="link_grow"></div></a>
            @else
                <a href="/mypocket" id="mypocket" class="animated"><span class="pb-icon my-pocket"></span><span class="menu-text">My Pocket</span><div class="link_grow"></div></a>
                <a href="/my-profile"><span class="pb-icon user"></span><span class="menu-text">My Profile</span><div class="link_grow"></div><notification type="user"></notification></a>
                @if(Auth::user()->store_id)
                    <a href="/store/items"><span class="pb-icon shopbag"></span><span class="menu-text">Marketplace</span><div class="link_grow"></div><notification type="store"></notification></a>
                    @else
                    <a href="/store/create"><span class="pb-icon shopbag"></span><span class="menu-text">Marketplace</span><div class="link_grow"></div></a>
                @endif
            @endif
           {{--  <a href="#">
                <span class="menu-ico">
                    <i class="line"></i>
                    <i class="line"></i>
                    <i class="line"></i>
                </span>
                <span class="menu-text">More</span>
            </a> --}}
        </div>
    </nav>
        </header>
            <nav class="desktop-menu">
                <ul>
                    <li class="fromthe">From the...&nbsp; &nbsp; &nbsp; &nbsp;</li>
                    <li><a href="/search?cat=kitchen">Kitchen</a></li>
                    <li><a href="/search?cat=garden">Garden</a></li>
                    <li><a href="/search?cat=woodshop">Woodshop</a></li>
                    <li><a href="/search?cat=craft">Craft Table</a></li>
                    <li><a href="/search?cat=studio">Studio</a></li>
                    <li><a href="/search?cat=home">Home</a></li>
                    <li><a href="/search?cat=boutique">Boutique</a></li>
                </ul>
            </nav>
            @yield('content')
            {{-- <div id="pb-app-issue" v-cloak>
                <a href="#" class="submitIssueBtn" @click="issueModal = true"><i class="pe-7s-attention"></i><br>Issues</a>
                <modal v-show="issueModal" send-message="submitIssue()" class="issueModal">

                <h3 slot="header"><i class="pe-7s-attention"></i> Report an Issue</h3>

                <div slot="body">
                    <p>Date: @{{issue.date}}</p>
                    <p>What page is the issue on?</p>
                    <input type="text" v-model.sync="issue.page">
                    <p>What type of issue are you experiencing?</p>
                    <select v-model.sync="issue.type">
                        <option value="">Choose a type...</option>
                        <option value="design">Design Issue</option>
                        <option value="form">Form not working</option>
                        <option value="error">Error Screen</option>
                        <option value="other">Other</option>
                    </select>
                    <p>Please describe the issue...</p>
                    <textarea v-model.sync="issue.description"></textarea>
                </div>
                <div slot="footer">
                    <button class="btn" @click="submitIssue()">
                    Submit
                  </button>
                  <button class="btn btn-tan" @click="$root.issueModal = false">
                        Cancel
                   </button>
                </div>
              </modal>
            </div> --}}
            {{-- Javascript --}}
            <!-- template for the modal component -->
            <script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
            <script src="https://unpkg.com/lodash@4.13.1/lodash.min.js"></script>
            {{-- <script type="text/javascript" src="/assets/js/app.js"></script> --}}
            <script src="/js/masking-input.js" data-autoinit="true"></script>
             {{-- <script type="text/javascript" src="/js/croppie.min.js"></script> --}}
            <script type="text/javascript" src="{{ elixir('/js/main.js') }}"></script>
            <script type="text/javascript">var switchTo5x=true;</script>
{{-- <script type="text/javascript" id="st_insights_js" src="https://ws.sharethis.com/button/buttons.js?publisher=a4480423-aeea-4d80-9ac2-4c9ef94422fb"></script>
<script type="text/javascript">stLight.options({publisher: "a4480423-aeea-4d80-9ac2-4c9ef94422fb", doNotHash: false, doNotCopy: false, hashAddressBar: true});</script> --}}

        @yield('footer')
        @include('footer')
    </body>
</html>