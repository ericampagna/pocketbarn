@extends('layouts.mainlayout')

@section('title', 'My Conversation with ')

@section('content')
<section class="container account-page" id="pb-app-conversations">
 @if(session()->has('status'))
        <alert {{ session('status_type') }}>{{ session('status') }}</alert>
    @endif
	<div class="sidebar">
		<ul>
			<li><a href="/my-profile" >My Profile</a></li>
			<li><a href="/account-settings" >Account Settings</a></li>
			<li><a href="/conversations">Conversations</a></li>
			@if($user->store_id)
				<li><a href="/store" >My Marketplace</a></li>
			@else
				<li><a href="store/create">Start my Marketplace</a></li>
			@endif
			<li><a href="/logout">Logout</a>
		</ul>
	</div>
	<div class="Profile">
	<a href="/conversations"><i class="pe-7s-angle-left"></i> Back to Conversations</a>
			<messages :conversation-id="{{ $id }}" :user="{{ $user }}"></messages>

	</div>
</section>

@endsection

@section ('footer')
<script>
	// Accordian
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
    }
}
</script>
@endsection