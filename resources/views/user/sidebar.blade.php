<div class="sidebar">
		<ul>
			<li><a href="/my-profile" ><i class="pb-icon user ico-small"></i> My Profile</a></li>
			{{-- <li><a href="/account-settings" ><i class="pe-7s-config"></i> Account Settings</a></li> --}}
			<li><a href="/conversations"><i class="pb-icon comment ico-small"></i> Conversations</a><notification type="user"></notification></li>
			@if($user->store_id)
				<li><a href="/store" ><i class="pb-icon shopbag ico-small"></i> My Marketplace</a></li>
			@else
				<li><a href="store/create"><i class="pb-icon shopbag ico-small"></i> Start My Marketplace</a></li>
			@endif
			<li><a href="/logout"><i class="pb-icon back ico-small"></i> Logout</a>
			<li><a href="/support"><i class="pb-icon help1 ico-small"></i> Support</a></li>
		</ul>
	</div>