@extends('layouts.mainlayout')

@section('title', 'pocketbarn - My Pocket')

@section('content')
<div id="pb-app-pocket">
	<div class="Pocket" v-cloak>
		<alert v-ref:alert-child  v-if="alert.show" :type="alert.type">@{{alert.message}}</alert>
			<h3>My Pocket</h3>

			@if(Auth::guest())
				<p class="text-center">You must be logged in to save items to your pocket.</p>
		  		<br>
		  		<p class="text-center">Please <a href="/login">Login</a></p>
		  		<p class="text-center">Or</p>
		  		<p class="text-center">You can <a href="/register">Register</a>
		  		</p>
			@else


				<div class="Pocket--sidebar">
					<p @click="switchView('items')"><i class="pe-s7-tag"></i> Items</p>
					<p @click="switchView('stores')"><i class="pe-s7-tag"></i> Marketplaces</p>
				</div>
				<div class="Pocket--wrapper">
					<div v-show="view == 'items'" transition="fade">

						<h3>Items in your pocket</h3>
						<h5 v-if="noItems" class="text-center">You have not saved any items to your pocket.</h5>
						<div v-for="card in pocket.items">
							<div class="Card pocketCard animated fadeInUpBig" transition="slideUp">
								
						    		<div class="Card--image">
						    			<a :href="'/' + card.store.slug + '/' + card.id + '/' + card.slug" class="Card--link" >
						      			<img :src="card.featured_image">
						      			</a>
						      		</div>
						     	
						    	<div class="Card--action">
						      		<a :href="'/' + card.store.slug + '/' + card.id + '/' + card.slug" class="Card--link" >
						      			<span class="Card--title">@{{ card.title }}</span>
						      		</a>
						      		<a href="#" class="Card--location"><i class="pe-7s-map-marker"></i>@{{ card.store.city }}, @{{ card.store.state }}</a>
						    	</div>
						    	<div class="Card__Over">
							    	<span @click="removeFromPocket('item', card.id)">
							    		<em>Remove from Pocket</em>
							    		<img class="pocket-ico" style="left: 0;" src="/assets/images/removefrompocket.svg">
							    	</span>
							    	<span @click="goToItem( card.store.slug, card.id, card.slug )">
							    		<em>Go To Item</em>
							    		<i class="pe-7s-right-arrow"></i>
							    	</span>
						    	</div>
						  	</div>
					  	</div>
					  </div>
					  <div v-show="view == 'stores'" transition="fade">
					  	<h3>Marketplaces in your pocket</h3>
					  	<h5 v-if="noStores" class="text-center">You have not saved any marketplaces to your pocket.</h5>
					  	<div v-for="store in pocket.stores">
					  		<div class="Card pocketCard animated fadeInUpBig" transition="slideUp">
								
						    		<div class="Card--image">
						    			<a :href="'/marketplace/' + store.id + '/' + store.slug" class="Card--link" >
						      			<img :src="store.profile_img">
						      			</a>
						      		</div>
						     	
						    	<div class="Card--action">
						      		<a :href="'/marketplace/' + store.id + '/' + store.slug" class="Card--link" >
						      			<span class="Card--title">@{{ store.name }}</span>
						      		</a>
						      		<a href="#" class="Card--location"><i class="pe-7s-map-marker"></i>@{{ store.city }}, @{{ store.state }}</a>
						    	</div>
						    	<div class="Card__Over">
							    	<span @click="removeFromPocket('store', store.id)">
							    		Remove <em>from Pocket</em>
							    		<img class="pocket-ico" src="/assets/images/removefrompocket.svg">
							    	</span>
							    	<span @click="goToStore(store.slug, store.id)">
							    		<em>Go To Marketplace</em>
							    		<i class="pe-7s-right-arrow"></i>
							    	</span>
						    	</div>
						  	</div>
					  	</div>
					  </div>
				</div>
			 </div>
			@endif
			<modal v-show="showModal" v-cloak>
		    <!--
		      you can use custom content here to overwrite
		      default content
		    -->
		  	<h3 slot="header">Send Seller A Message</h3>

		  	<div slot="body">
		  		<p class="text-center">You must be logged in to start a conversation.</p>
		  		<br>
		  		<p class="text-center">Please <a href="/login">Login</a></p>
		  		<p class="text-center">Or</p>
		  		<p class="text-center">You can <a href="/register">Register</a>
		  		</p>
		  	</div>
		  	<div slot="footer">
	          <button class="btn btn-tan" @click="$root.showModal = false">
	                Ok
	              </button>
	        </div>
		  </modal>
	</div>
</div>
@endsection

@section ('footer')
<script>
	// Accordian
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
    }
}
</script>
@endsection

