@extends('layouts.mainlayout')

@section('title', 'My Conversations')

@section('content')
<section class="container account-page" id="pb-app-conversations">
 @if(session()->has('status'))
        <alert {{ session('status_type') }}>{{ session('status') }}</alert>
    @endif
	@include('user.sidebar')
	<div class="Profile" v-cloak>
		<ul class="Conversations--tabs">
			<li v-bind:class="{ active: userView }" @click="switchView()">
				My Inbox
			<span v-if="userUnread" class="Notification--badge">@{{ userUnread }}</span>
			</li>
			@if($user->store_id)
			<li v-bind:class="{ active: storeView }" @click="switchView()">
				{{$user->store->name}} Inbox
				<span v-if="storeUnread" class="Notification--badge">@{{ storeUnread }} new</span>
			</li>
			@endif
		</ul>

		<div class="Conversations--wrapper">
			<div v-show="userView">
				<h4>My Conversations</h4>
				<conversations :user="{{$user->id}}" ></conversations>
			</div>
			@if($user->store_id)
				<div v-show="storeView">
					<h4> My Marketplace Conversations</h4>
					<conversations  :user="{{$user->id}}" :store="true" ></conversations>
				</div>
			@endif
		</div>

	</div>
</section>

@endsection

@section ('footer')
<script>
	// Accordian
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
    }
}
</script>
@endsection