@extends('layouts.mainlayout')

@section('title', 'My Profile')

@section('content')
<section class="container account-page" id="pb-app-profile">
 @if(session()->has('status'))
        <alert {{ session('status_type') }}>{{ session('status') }}</alert>
    @endif
	@include('user.sidebar')
	<div class="Profile">
		<h4>My Profile</h4>
		<form method="POST" action="/my-profile" class="Form" id="profileForm" enctype="multipart/form-data">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}
			<div class="Form--input-group">
				<input class="Form--input" type="text" name="name" placeholder="Full Name" value="{{(old('name')) ?: $user->name }}">
				<span class="Form__error">{{ $errors->first('name') }}</span>
			</div>
			<div class="Form--input-group">
				<input class="Form--input" type="email" name="email" placeholder="Full Name" value="{{(old('email')) ?: $user->email }}">
				<span class="Form__error">{{ $errors->first('email') }}</span>
			</div>
			<div class="Form--input-group">
				<input class="Form--input" type="text" name="location" placeholder="Location (City, State or Zipcode)" value="{{ (old('location')) ?: $user->location }}">
				<span class="Form__error">{{ $errors->first('location') }}</span>
			</div>
			<div class="Form--input-group">
			<h5>Gender</h5>
				Male
				<input class="Form--radio" type="radio" name="gender" value="male" {{ ($user->gender == 'male' ? 'checked' : '' ) }} >
				Female
				<input class="Form--radio" type="radio" name="gender" value="female" {{ ($user->gender == 'female' ? 'checked' : '') }}  >
				Other
				<input class="Form--radio" type="radio" name="gender" value="other" {{ ($user->gender == 'other' ? 'checked' : '' ) }} >
				<span class="Form__error">{{ $errors->first('gender') }}</span>
			</div>
			<div class="Form--input-group Form--file">
				<single-image-upload dbimage="{{$user->avatar}}" shape="circle", width=300 height=300 place="profile"></single-image-upload>
				<p>An image 300px X 300px will work the best</p>
			</div>
			<div class="Form--input-group">
				<button class="btn" type="submit" v-show="imageSaved">Update Profile</button>
			</div>
		</form>
	</div>
</section>

@endsection

@section ('footer')
<script>
	/*Croppie for croping uploaded photos*/

// 	$uploadCrop = $('#upload').croppie({
//     enableExif: true,
//     viewport: {
//         width: 200,
//         height: 200,
//         type: 'circle'
//     },
//     boundary: {
//         width: 300,
//         height: 300
//     }
// });
</script>
@endsection