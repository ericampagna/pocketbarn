@extends('layouts.mainlayout')

@section('title', 'About Pocketbarn')

@section('content')
<section class="video-wrapper">
<div class="video">
		<video id="homepageVideo" autoplay muted="" poster="https://s3-us-west-2.amazonaws.com/pocketbarn/assets/welcome_to_pocketbarn.png" >
			<source src="https://s3-us-west-2.amazonaws.com/pocketbarn/assets/welcome_to_pocketbarn.webm" type="video/webm">
			<source src="https://s3-us-west-2.amazonaws.com/pocketbarn/assets/welcome_to_pocketbarn.mp4" type="video/mp4">
			Your browser does not support video.
		</video>
			<div class="playbutton" id="playFullScreen">
				<i class="pe-7s-play"></i>
			</div>
			<div class="mutebutton" id="muteVideo">
				<i class="pe-7s-volume2"></i>
			</div>
	</div>
</section>
<section class="container about-page" id="pb-app" v-cloak>
	<h1 id="top">Welcome to Pocketbarn!</h1>
	
	<p>We are so excited that you are here visiting our site!  We are inviting all makers and buyers to join us in bringing good ole southern hospitality back to online shopping and selling.  
Created by a mother/daughter team, Pocketbarn is an online platform that exists to help makers and buyers connect to offer handmade and homegrown products to people in their local communities and beyond.  </p>

<h2>Here is our story:</h2>
<img src="/assets/images/Pocketbarn_creators.jpg" alt="Shelly Richardson and Ashley Kanipe, creators of Pocketbarn" title="Shelly Richardson and Ashley Kanipe, creators of Pocketbarn" />
<p>When we developed a vision for Pocketbarn we were running a baking business from our homes and selling baked goods to the residents of our small town through our Facebook page, In the Kitchen Baking.  We loved the connections and conversations that we were having with our customers, but we saw a need to expand beyond our community.  We soon realized that God was leading us to encourage others in their own communities who owned similar, small or home-based businesses.  That’s when the idea for Pocketbarn was formed!  We started researching, dreaming, and praying, and we set about baking and baking to earn enough money to launch our website!  We launched mypocketbarn.com a few short months ago and we are so excited with the response we have received!</p>
<p>Our site is a platform for entrepreneurs just like you!  It is also a place for people to shop and purchase handcrafted and homegrown products that they want and need!  We love that our makers and buyers connect through our messaging system to place orders.  We also love the local aspect of the site.  People can search a certain city or zip code and find things that people make near them.  They can also search by topic to find products they are interested in.  We are striving to have top quality items posted on the site.  We want people to come to Pocektbarn and love everything they see!</p>
<p>Through Pocketbarn, you can advertise completely FREE and keep 100% of your profits.  We allow people to post 3 items for free that can be changed as often as you want at no charge.  Again, you keep 100% of your profits.  If you would like to feature more items in your marketplace, we do offer paying packages for $4.99 and $14.99 monthly.  However, it is completely fine to use this site for FREE, no strings attached.  Since our launch, there have been marketplaces created in all 50 states and Canada!
</p>
<p>We would love for you to set up a marketplace for your creations and products, or just shop around.  Join us today!  We love Pocketbarn, and we know you will too!
</p>
<div class="more">
 	<h4>More Information</h4>
 	<a class="btn" href="/support">FAQs</a>
 	<a class="btn" href="/privacy-policy">Privacy Policy</a>
 </div>

</section>

@endsection

@section ('footer')
@endsection