<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Muli:300,400" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/assets/css/main.css">
        <script src="https://use.typekit.net/ywq2vat.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>
    </head>
    <body>
    <header>
    <div class="logo-wrapper">
        <a class="logo" href="/">
            <img src="/assets/images/pocketbarn-icon.svg">
            <span>pocketbarn</span>
        </a>
    </div>
    <nav class="icon-menu">
        <div class="menu-wrapper">
            <a href="#"><span class="pe-7s-home"></span><span class="menu-text">Home</span></a>
            <a href="#"><span class="mypocket-ico"><img src="/assets/images/mypocket.svg"></span><span class="menu-text">My Pocket</span</a>
            <a href="#"><span class="pe-7s-search"></span><span class="menu-text">Search</span</a>
            <a href="#"><span class="pe-7s-user"></span><span class="menu-text">My Profile</span</a>
            <a href="#">
                <span class="menu-ico">
                    <i class="line"></i>
                    <i class="line"></i>
                    <i class="line"></i>
                </span>
                <span class="menu-text">More</span
            </a>
        </div>
    </nav>
    </header>
    <nav class="desktop-menu">
        <ul>
            <li>From the ... </li>
            <li><a href="#">Kitchen</a></li>
            <li><a href="#">Garden</a></li>
            <li><a href="#">Woodshop</a></li>
            <li><a href="#">Craft Table</a></li>
            <li><a href="#">Studio</a></li>
            <li><a href="#">Closet</a></li>
        </ul>
    </nav>
        <h1>Heading 1</h1>
        <h2>Heading 2</h2>
        <h3>Heading 3</h3>
        <h4>Heading 4</h4>
        <h5>Heading 5</h5>
        <h6>Heading 6</h6>
        <p>Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec rutrum congue leo eget malesuada. Nulla quis lorem ut libero malesuada feugiat. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Pellentesque in ipsum id orci porta dapibus. Cras ultricies ligula sed magna dictum porta. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus.</p>

        <a href="#">This is a link</a>
        <br>
        <a href="#" class="btn">Link Button</a>
        <br>
        <button>Button</button>
        <br>
        <form>
        <input type="text" name="" placeholder="I am looking for..">
        <div class="input-icon">
        <i class="pe-7s-search pe-fw"></i>
        <input placeholder="I am looking for" name="">
        </div>
        <code>
        Icon Input<br>
        &lt;div class="input-icon"&gt;<br>
        &lt;i class="pe-7s-search pe-fw">&lt;/i&gt;<br>
        &lt;input placeholder="I am looking for" name=""&gt;<br>
        &lt;/div&gt;
        </code>
        <input class="input-inline" type="" value="">
        <input class="btn" type="submit" name="" value="Submit">
        </form>
    </body>
</html>
