@extends('layouts.mainlayout')

@section('title', $item->title)
@section('meta_desc', $item->description)
@section('meta_keywords', $item->tags)
@section('ogUrl', secure_url($item->store->slug .'/'.$item->slug))
@section('ogImage', $item->shareImage)

@section('content')
<div id="pb-app" v-cloak>
<div class="go-back">
	<a class="btn" href="#" v-on:click.prevent="goBack()">Back to Search</a>
</div>
	<alert v-ref:alert-child  v-if="alert.show" :type="alert.type" >@{{alert.message}}</alert>
	<div class="container single-item-head">
	
		<div class="left">
			<h3>{{ $item->title }}</h3>
				</div>
		<div class="right item-icons"  @mouseover="iconMeanings = true" @mouseleave="iconMeanings = false" @click="iconMeanings = true">
			@if($item->delivery)<span class="pb-icon truck"></span>@endif
			@if($item->shipping)<span class="pb-icon box1"></span>@endif
			@if($item->pickup)<span class="pb-icon shoe"></span>@endif
			@if($item->hasStore)<span class="pb-icon storefront"></span>@endif
			{{-- <span class="pb-icon pocket-ico"></span> --}}

		</div>
		<div class="icon-meanings animated" v-show="iconMeanings" transition="fade">
			<button  @click="iconMeaning = false"><i class=" pb-icon close-circle"></i></button>
			<h5>Icon Key</h5>
			<p><span class="pb-icon truck"></span> Local Delivery</p>
			<p><span class="pe-7s-box1 pe-fw"></span> Shipping Available</p>
			<p><span class="pb-icon shoe"></span> Local Pickup</p>
			<p><span class="pb-icon storefront"></span> Brick and Mortar Store</p>
		</div>
	</div>
	<section class="container Item--page">
		<Slider></Slider>
		<div class="seller-box">
			<div class="top-seller-box">
				<div class="price">
					<h4>${{ $item->price }}</h4>
					<p>@if($item->quantity){{ $item->quantity }} Available @endif</p>
				</div>
				<div class="send-message">
					@if($item->contact_button == 'Buy on Our Website')
						<a href="{{$item->item_url}}" target="_blank" class="btn">Buy on Our Website</a>
					@else
						<a class="btn" @click="showModal = true">Contact to Purchase</a>
					@endif
				</div>
			</div>
			<div class="bottom-seller-box">
				<div class="store-profile-pic">
					@if($item->store->profile_img)
						<img src="{{$item->store->profile_img}}" >
					@else
						<img src="/assets/images/icons/pocketbarn_icons_shopbag.svg" />
					@endif
				</div>
				<div class="store-contact-info">
					<h5><a href="/marketplace/{{$item->store->id}}/{{ $item->store->slug}}">{{ $item->store->name }}</a></h5>
					<i class="pb-icon marker ico-small" style="float: left;"></i><p>{{ $item->store->city }}, {{ $item->store->state }}</p><br />

				</div>
			</div>
			@if($item->store->phone)
				<div class="phone"><i class="pb-icon phone ico-small" style="float: left;"></i><p>{{ $item->store->phone }}</p></div>
				<br>
			@endif
			@if($item->store->website)
				<div class="website"><i class="pb-icon browser ico-small" style="float: left; "></i><p style="line-height: 1.2rem;">{{ $item->store->website }}</p></div>
			@endif

			<a class="addToPocket" @click="addToPocket('item', {{$item->id}})"><span class="mypocket-ico" ><img class="pocket-ico" src="/assets/images/addtopocket_gray.svg"></span> Save to my pocket</a>
			<div class="social-icons">
				{{-- <div class="fb-share-button" data-href="{{secure_url($item->store->slug .'/'.$item->slug)}}" data-layout="button" data-size="small" data-mobile-iframe="false"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div> --}}
				<!-- AddToAny BEGIN -->
				<!-- AddToAny BEGIN -->
				<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
				<a class="a2a_dd" href="https://www.addtoany.com/share?linkurl=www.mypocketbarn.com&amp;linkname="></a>
				<a class="a2a_button_facebook"></a>
				<a class="a2a_button_twitter"></a>
				<a class="a2a_button_google_plus"></a>
				<a class="a2a_button_pinterest"></a>
				<a class="a2a_button_email"></a>
				</div>
				<script>
				var a2a_config = a2a_config || {};
				a2a_config.linkurl = "{{secure_url('/item/'.$item->store->slug.'/'.$item->id.'/'.$item->slug)}}";
				a2a_config.onclick = 1;
				</script>
				<script async src="https://static.addtoany.com/menu/page.js"></script>
						<!-- AddToAny END -->
			</div>
		</div>

		<div class="item-details">
			<h4>Description</h4>
			<hr>
			  {!! nl2br(e($item->description)) !!}
			<div class="get-to-know">
				<div class="gtk-text">
					<h5>Get to know the sellers</h5>
					<a href="/marketplace/{{$item->store->id}}/{{ $item->store->slug}}" class="btn btn-tan">Visit Marketplace</a>
				</div>
				<div class="seller-info">
					<div class="sellers">
						@foreach($item->sellers as $seller)
							<div class="seller">
								@if($seller->avatar)
									<img width="75" src="{{ $seller->avatar }}" />
								@else
									<i class="pb-icon user-white ico-xlarge ico-center avatar-filler"></i>
								@endif
								<p>{{ $seller->name }}</p>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>

		<div class="other-seller-items">
			<h4>More from this Marketplace</h4>
			<more-items store="{{ $item->store->id }}" item="{{ $item->id }}"></more-items>
		</div>
		<div class="container similar-items">
			<h5>Similar items near you</h5>
			<more-items category="{{ $item->category_id }}" item="{{ $item->id }}"></more-items>
		</div>
	</section>



	@if(Auth::user())
		<modal v-show="showModal" send-message="sendMessage()">
		    <!--
		      you can use custom content here to overwrite
		      default content
		    -->
		  	<h3 slot="header">Send Seller A Message</h3>

		  	<div slot="body">
		  		<p>What whould you like to say to the seller?</p>
		  		<textarea v-model.sync="messageToSeller"></textarea>
		  	</div>
		  	<div slot="footer">
		  		<button class="btn" @click="sendMessage">
	            Send
	          </button>
	          <button class="btn btn-tan" @click="$root.showModal = false">
	                Cancel
	           </button>
	        </div>
		  </modal>
	@else
		<modal v-show="showModal">
	    <!--
	      you can use custom content here to overwrite
	      default content
	    -->
	  	<h3 slot="header">Send Seller A Message</h3>

	  	<div slot="body">
	  		<p class="text-center">You must be logged in to start a conversation.</p>
	  		<br>
	  		<p class="text-center">Please <a href="/login">Login</a></p>
	  		<p class="text-center">Or</p>
	  		<p class="text-center">You can <a href="/register">Register</a>
	  		</p>
	  	</div>
	  	<div slot="footer">
          <button class="btn btn-tan" @click="$root.showModal = false">
                Ok
              </button>
        </div>
	  </modal>
	@endif
</div>
@endsection

@section ('footer')
<script>
	// Accordian
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
    }
}
</script>
@endsection

