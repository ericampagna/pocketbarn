@extends('layouts.mainlayout')

@section('title', '500 Server Error')
@section('meta_description', 'Find handmade and homegrown goods local and close to home or sell your items online and keep 100% of the profits.')
@section('meta_keywords', 'pocketbarn,mypocketbarn,pocketbarn.com,mypocketbarn.com,handmade,local,shop local,shopping,shop small,local goods,sell online,sell local,sell goods,online store,woodworking,farm goods,food')

@section('content')

<section class="text-center">
	<h2>500</h2>
	<h5>Something seems to have gone wrong. Please go back a try again.</h5>

	  <section class="Issue" id="pb-app-issue" style="background-color: #d8d3cd; margin: 0 auto; float: none;">
		<h3><i class="pe-7s-attention"></i> Report an Issue</h3>
        <p>Date: @{{issue.date}}</p>
        <p>What page is the issue on?</p>
        <input type="text" v-model.sync="issue.page">
        <p>What type of issue are you experiencing?</p>
        <select v-model.sync="issue.type">
            <option value="">Choose a type...</option>
            <option value="design">Design Issue</option>
            <option value="form">Form not working</option>
            <option selected="selected" value="error">Error Screen</option>
            <option value="other">Other</option>
        </select>
        <p>Please describe the issue...</p>
        <textarea hidden v-model.sync="issue.stacktrace">{{$error}}</textarea>
        <textarea v-model.sync="issue.description"></textarea>
        <button class="btn" @click="submitIssue()">
        Submit
      </button>
	</section>
</section>





@endsection

@section('footer')


@endsection
