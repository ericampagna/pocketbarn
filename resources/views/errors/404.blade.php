@extends('layouts.mainlayout')

@section('title', 'Page not Found')
@section('meta_description', 'Find handmade and homegrown goods local and close to home or sell your items online and keep 100% of the profits.')
@section('meta_keywords', 'pocketbarn,mypocketbarn,pocketbarn.com,mypocketbarn.com,handmade,local,shop local,shopping,shop small,local goods,sell online,sell local,sell goods,online store,woodworking,farm goods,food')

@section('content')

<section class="text-center">
	<h2>404</h2>
	<h4>We're sorry. The page you are looking for is not here.</h4>
	<h5>You can searching for something else...</h5>
	<form action="/search" method="get">
		<div class="input-icon home-search-input">
			<input style="min-width: 30vw; text-indent: 0;" placeholder="I am looking for" name="terms">
		</div>
		<div class="input-icon home-location">
			<input style="min-width: 30vw; text-indent: 0;" placeholder="near.. (city and state, zipcode)" name="location">
		</div>
        <input class="btn" type="submit" name="" value="Search">
    </form>
</section>





@endsection

@section('footer')


@endsection
