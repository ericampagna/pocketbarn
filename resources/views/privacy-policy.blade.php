@extends('layouts.mainlayout')

@section('title', 'Privacy Policy')

@section('content')
<section class="container about-page" id="pb-app" v-cloak>
	<h1 id="top">Pocketbarn Privacy Policy</h1>
	<section class="privacy">
	<p>
		At Pocketbarn we are committed to being transparent about how we handle your personal
		information. This policy explains our privacy practices for mypocketbarn.com and the
		Pocketbarn mobile application.
	</p>
	<p>
		This policy is a part of our Acceptable Use Policy. By visiting mypocketbarn.com or
		using the Pocketbarn App, you consent to our collection, transfer, manipulation, storage,
		disclosure and other uses of your information as described in this policy. This includes
		any information you choose to provide that is deemed sensitive under applicable law. We
		are not responsible for the content or the privacy policies of any of our member’s
		websites that are linked to a Pocketbarn marketplace.
	</p>
	<p>
		Pocketbarn’s Acceptable Use Policy requires all account owners to be at least 18 years of
		age (or have the permission and supervision of a responsible parent or legal guardian)
		therefore, this policy does not discuss use of our site, our app or services by minors.
	</p>
	<h4>Data Use</h4>
	<p>
	By using our site, you authorize mypocketbarn.com to use your information in the United
	States, and any other country where mypocketbarn.com operates. Please be aware that the
	privacy laws and standards in certain countries, including the rights of authorities to
	access your personal information, may differ from those applying in the country in which
	you reside. We will only transfer personal data to these countries where permitted to do
	so by law and we will take steps to ensure that your personal information continues to
	enjoy appropriate protections.
	</p>
	<h4>Your Information</h4>
	<p>
	To use our platform, you will need to provide a valid email address to us. If setting up a
	marketplace you will be able to choose a marketplace name. Depending on the services
	you choose, we may collect additional information, for example: information in private
	channels like Conversations, billing or payment information, a telephone number, and a
	physical address. Please note that for security and performance reasons, such content may
	need to be accessed by certain Pocketbarn personnel from time to time.
	</p>
	<p>
	Your marketplace name is publicly displayed and connected to your activity on the site
	and the app. Other people may see the date you joined; ratings, reviews and related
	photos for items you are selling or sold; your profile information; items you listed for
	sale; your marketplace pages and policies. You may optionally choose to provide and
	publicly display your full name and other personal information (such as birthday, gender,
	and location).
	</p>
	<h4>Uses and Sharing</h4>
	<p>
	We use technologies such as cookies, log files, and flash cookies for several purposes,
	including helping understand how you interact with our site and services, in order to
	provide a better experience.
	</p>
	<br>
	<p>
	We use the information that we gather from our users to improve our services; we use
	data analytics and work with certain third-party service providers for targeted online and
	offline marketing; and use your email address to communicate with you. We also partner
	with service providers and other third parties that collect, use, promote, and analyze
	information about Pocketbarn.
	</p>
	<br>
	<p>
	We rely on various authorized methods, including your consent, to enable us to transfer
	your personal data to the US (if you do not live in the US) and to other countries. In some
	countries, the privacy laws and standards, including the rights of certain authorities to
	access your personal information, may differ from those that apply in your home country.
	</p>
	<p>
	You may review and change your information in your account settings, and you may
	remove certain optional pieces of information. You may also limit certain tracking by
	disabling cookies in your web browser.
	</p>
	<p>
	Pocketbarn will not sell or disclose your personal information to third parties without
	your consent, except as specified in our Acceptable Use Policy.
	</p>
	<h4>Information Collected and Received</h4>
	<p>
	If you set up a Pocketbarn account at mypocketbarn.com you are required to provide a
	valid email address at registration. Your username and/or marketplace represent your
	identity on the site and the app.
	<p>
	<br>
	<p>
	Depending on which services you choose to use, Pocketbarn may require additional information to provide the service, such as a shop name, billing and payment information (including billing address, telephone number, and credit card information), a telephone number, and/or a physical postal address. Occasionally Pocketbarn may contact individual marketplace owners to confidentially request more information about their store or items listed on mypocketbarn.com, or to ensure compliance with our rules and applicable law. If you choose not to have additional information displayed publicly in your marketplace, it will be your responsibility as a marketplace representative to provide your customers with information necessary to complete a transaction. 
	</p>
	<br>
	<p>
	Your username, is publicly displayed and connected to your activity on the site and the app. Other people may see the date you joined, reviews, ratings, profile information and items listed for sale in your marketplace. Pocketbarn automatically receives and records information from your browser or your mobile device when you visit the Site or use the Your IP address or unique device identifier, cookies and data about which marketplaces you visit on the site or on the apps is stored in log files. We may combine this automatically collected information with other information we collect about you. This information helps us to prevent fraud and keep the site and app secure, analyze and understand how the site and the app work for members and visitors. 
	</p>
	<br>
	<p>
	When you use the Pocketbarn app, you may choose to share your geo-location details with Pocketbarn in order to use our location-based services. If you have provided your location details but would no longer like to share that information, you may opt out of those services through the settings on the app or on your mobile device. We will only share your geo-location details with third parties (like our mapping providers) for the sole purpose of providing you with those services. You may also enable the app to access your mobile device’s camera to upload photographs to mypocketbarn.com.
	</p>
	<br>
	<p>
	Connecting your Pocketbarn Marketplace to third-party applications or services is optional, and when you connect your account to an external app, you will be granting Pocketbarn permission to receive information from the third-party applications. You can also choose to share some of your activity on Pocketbarn on certain social media networks which are connected to your Pocketbarn Marketplace.
	Etsy also collects certain information from both members and non-members who visit websites hosted by the site. This information may include your IP address or unique device identifier, and cookies and data from the pages you visit. 
	</p>

	<h4>Limitations</h4>
	<p>We know that members of our community value having control over their own information, and therefore Pocketbarn gives you the choice of providing, editing or removing certain information, as well as choice over how we contact you.</p>
	<br>
	<p>You may change or correct your account information (except your username) on the site. You may also remove certain optional information on the site that you no longer wish to be publicly visible, such as your full name.</p>
	<br>
	<p>For various reasons, Pocketbarn may send you messages about our services or your activity. Some of these messages are required, service-related messages for members (such as transactional messages or legal notices). Other messages are not required, (such as newsletters), and you can control which optional messages you choose to receive by changing your settings in your account. </p>
	<br>
	<p>If you have chosen to connect your account to an external, third-party application, such as Facebook or Etsy, you can change your settings and remove permission for the app by changing your account settings.</p>
	<br>
	<p>If you no longer wish to use the site or the app or receive service-related messages, then you may close your account.</p>
	<h4>Messages from Pocketbarn</h4>
	<p>On occasion, Pocketbarn will contact you. Primarily, these messages are delivered by email. That is why every account is required to keep a valid email address on file to receive messages. Pocketbarn may contact you by telephone to provide member support or related purposes if you give us permission to call you.</p> 
	<br>
	<p>Some messages from Pocketbarn are service-related and required for members. Examples of service-related messages include an email address confirmation/welcome email when you register your account, notifications and correspondence with the Pocketbarn support team. You may not opt out of receiving service-related messages from Pocketbarn, unless you close your account.</p>
	<br>
	<p>Pocketbarn may send you news or updates about changes to our site, app, or services. From time to time, we may develop new types of messages and share them with you to inform you about our services. Primarily, members will receive these messages via email or through the app. </p>
	<h4>The Pocketbarn Community</h4>
	<p>The Pocketbarn community consists of buyers and sellers in communities across the U.S. and abroad. We have established a Facebook closed group that allow members to connect and communicate in a semi-public space. Please use common sense and good judgment when posting in to this group. Be aware that any personal information you submit there can be read, collected, or used by others, or could be used to send you unsolicited messages. The group administrator generally does not remove content from the group unless unauthorized content is posted. You are responsible for the personal information you choose to post in community spaces through the site, the app and community spaces such as Facebook.</p>

	<h4>Information Uses, Sharing & Disclosure</h4>
	<p>We respect your privacy. Pocketbarn will not disclose your name, email address or other personal information to third parties without your consent, except as specified in this policy. We generally use the information to provide and improve our services and products, for billing and payments, for identification and authentication, for targeted online and offline marketing, to contact members or interested parties, and for general research and aggregate reporting.</p>
	<br>
	<p>Pocketbarn limits access to your personal information by Pocketbarn personnel to those whom we believe reasonably needs that information to provide products or services to you or in order to do their jobs. </p>
	<br>
	<p>If you reside outside of North America please be aware that the privacy laws and standards in certain jurisdictions may differ from those applying in the country in which you reside.
	</p> 
	<br>
	<p>Pocketbarn is a platform to facilitate the sharing of information between buyers and sellers. We expect you to respect the privacy of the Pocketbarn members whose information you received. You have a limited license to use that information only for Pocketbarn-related communications. Pocketbarn has not granted you a license to use the information for unsolicited commercial messages or unauthorized transactions. Without express consent from that person, you must not add any Pocketbarn member to your email or physical mailing list or misuse any member’s personal information. Remember that you need to comply with all relevant legal rules when you use the site or the app. This includes applicable data protection and privacy laws which govern the ways in which you can use another Pocketbarn member’s information.</p>
	<br>
	<p>Pocketbarn may release your personal information to a third party in the following circumstances: to protect, establish, or exercise our legal rights or defend against legal claims; to comply with a subpoena, court order, legal process, or other legal requirement; or when we believe in good faith that such disclosure is necessary to comply with the law, prevent imminent physical harm or financial loss, or investigate, prevent, or take action regarding illegal activities, suspected fraud, threats to our property, or violations of Pocketbarn's Acceptable Use Policy.</p>
	<p>Pocketbarn is affiliated with a variety of businesses and works closely with them. These businesses may offer promotions (including email promotions) with our direct consent. We may also provide services or advertise Pocketbarn Marketplace products jointly with affiliated businesses. When an affiliated business is associated with your transaction, we may share information related to the transaction with that affiliated business to facilitate your transaction. </p>
	<br>
	<p>Pocketbarn may share demographic information with business partners, but it will always be aggregated and anonymized, so personally-identifiable information is not revealed.</p>
	<br>
	<p>Pocketbarn may engage third party companies and individuals (such as payment processors, research companies, analytics and security providers) to help provide our services. These third parties have limited access to your information only to perform these tasks on our behalf and are obligated to Pocketbarn not to disclose or use your information for other purposes.</p>
	<br>
	<p>If Pocketbarn would ever choose to buy or sell assets such as a sale, merger, liquidation, receivership or transfer of all or substantially all of our assets, member information is typically one of the business assets that is transferred. If Pocketbarn intends to transfer information about you, we will notify you by email or by putting a prominent notice on the site and the app, and you will be afforded an opportunity to opt out before information about you becomes subject to a different privacy policy.</p>
	<br>
	<p>Third-party plug-ins also may collect information about other sites you visit. For example, when you link your Pocketbarn Marketplace to your personal website or other selling platform, that has a social plug-in from a third-party site or service, you are also loading content from that third-party site. That site may request cookies directly from your browser. These interactions are subject to the privacy policy of the third-party site. In addition, certain cookies and other tracking mechanisms on our site are used by third parties for targeted online marketing and other purposes. These technologies allow a partner to recognize your computer or mobile device each time you visit the site or use the app, but do not allow access to personal information from Pocketbarn. Pocketbarn does not have access or control over these third-party technologies, and they are not covered by our Privacy Policy. If you prefer to prevent third parties from setting and accessing cookies on your computer, you may set your browser to block cookies. You may also opt out of the use of cookies by Google Analytics. Although our website currently does not respond to “do not track” browser headers, you can limit tracking by taking these steps.</p>
	<br>
	<p>We can only speak for ourselves; this policy does not apply to the practices of third parties that Pocketbarn does not own or control, or individuals that Pocketbarn does not employ or manage. If you provide your information to others, different practices may apply to the use or disclosure of the information you provide to them. Pocketbarn does not control the privacy policies of third parties, including other Pocketbarn members. Pocketbarn is not responsible for the privacy or security practices of its members or other websites on the Internet, even those linked to or from the site or app. We encourage you to ask questions before you disclose your personal information to others. </p>
	<h4>Security</h4>
	<p>The security of your personal information is important to us. Your Pocketbarn account information is protected by a password. It is important that you protect against unauthorized access to your account and information by choosing your password carefully, and keeping your password and computer secure, such as by signing out after using our services. </p>
	<br>
	<p>Pocketbarn encrypts sensitive information (such as credit card numbers) using secure socket layer technology (SSL). We follow generally accepted industry standards to protect the personal information submitted to us, both during transmission and once we receive it. Unfortunately, no method of transmission over the Internet, or method of electronic storage, is 100% secure. Therefore, while we strive to protect your personal information, we cannot guarantee its absolute security.</p> 
	<h4>Data Retention</h4>
	<p>Pocketbarn will retain your information only for as long as is necessary for the purposes set out in this policy, for as long as your account is active, or as needed to provide you services. If you no longer want Pocketbarn to use your information to provide you services, you may close your account. Pocketbarn will retain and use your information to the extent necessary to comply with our legal obligations, resolve disputes, and enforce our agreements. Please note that closing your account may not free up your email address, username, or Marketplace name for reuse on a new account.</p>
	<br>
	<p>You may inform us of any changes or requests regarding your personal data, and in accordance with our obligations under applicable data protection law, we may update or delete your personal data accordingly. We'll respond to your request as soon as possible.</p>
	<h4>Your Rights</h4>
	<p>If you are a Pocketbarn member, you can access, correct, change, and delete certain personal information associated with your account by visiting account settings. If you need further assistance, you can contact Pocketbarn through our Pocketbarn Support Team. </p>

	<h4>Transactions</h4>
	<p>All business and sales transactions between Pocketbarn member buyers and sellers will take place outside of the Pocketbarn website and app. Problems associated with the exchange of payment and related products advertised for sale on a Marketplace are the sole responsibility of the buyer and seller. Pocketbarn is also not responsible or liable for  damaged products delivered through a third party or products received that may be misrepresented. Pocketbarn trusts that its members will be respective of one another and engage in good business practice. Problems related to buying and selling should be addressed explicitly between the buyer and seller.</p>

	<h4>Privacy Policy Changes</h4>
	<p>We may update this policy from time to time. If we believe that the changes are material, we’ll definitely let you know by doing one (or more) of the following: (1) posting the changes on the site and app, (2) sending you an email or message about the changes, or (3) posting an update in the version notes on the app platform. That way you can decide whether you want to continue using the site or app. Changes will be effective upon the posting of the changes. You are responsible for reviewing and becoming familiar with any changes. Your use of the site or app following the changes constitutes your acceptance of the updated policy.</p>

	<h4>Contact Pocketbarn</h4>
	<p>If you have questions or suggestions about personal information you can contact the Pocketbarn Support Team via our website or app by sending an email to support@pocketbarn.com. </p>
	<br>
	<i>April 1, 2017</i>


	</section>

@endsection

@section ('footer')
@endsection