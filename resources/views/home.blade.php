@extends('layouts.mainlayout')

@section('title', 'Pocketbarn')
@section('meta_desc', 'Find handmade and homegrown goods local and close to home or sell your items online and keep 100% of the profits.')
@section('meta_keywords', 'pocketbarn,mypocketbarn,pocketbarn.com,mypocketbarn.com,handmade,local,shop local,shopping,shop small,local goods,sell online,sell local,sell goods,online store,woodworking,farm goods,food')

@section('content')
<section class="Home--about">
	<div class="Home--about-photo"></div>
	<a href="/about" class="Home--about-story">
		<h1>Read the story <br>behind Pocketbarn <br><i class="pe-7s-up-arrow pe-rotate-90"></i></h1>

	</a>
</section>
<div class="homesearch-wrapper">
	{{-- div class="video">
		<video id="homepageVideo" autoplay muted="" poster="https://s3-us-west-2.amazonaws.com/pocketbarn/assets/welcome_to_pocketbarn.png" >
			<source src="https://s3-us-west-2.amazonaws.com/pocketbarn/assets/welcome_to_pocketbarn.webm" type="video/webm">
			<source src="https://s3-us-west-2.amazonaws.com/pocketbarn/assets/welcome_to_pocketbarn.mp4" type="video/mp4">
			Your browser does not support video.
		</video>
			<div class="playbutton" id="playFullScreen">
				<i class="pe-7s-play"></i>
			</div>
			<div class="mutebutton" id="muteVideo">
				<i class="pe-7s-volume2"></i>
			</div>
	</div> --}}

	<div class="home-search-inline">
		<form action="/search" method="get">
			<div class="input-icon home-search-input">
				<i class="pb-icon search-icon"></i>
				<input placeholder="I am looking for" name="terms">
			</div>
			<div class="input-icon home-location">
				<i class="pb-icon map-marker"></i>
				<input placeholder="near.. (city and state, zipcode)" name="location">
			</div>
	        <input class="btn" type="submit" name="" value="Search">
	    </form>
	</div>
</div>
{{-- <section class="home-heading text-center">
<h3>What is Pocketbarn?</h3>
</section> --}}
<section class="container" id="pb-app" v-cloak>
	<alert v-ref:alert-child  v-if="alert.show" :type="alert.type">@{{alert.message}}</alert>
	<cards></cards>

	<modal v-show="showModal">

	  	<h3 slot="header">Welcome to Pocketbarn!</h3>

	  	<div slot="body">
	  		<h4>Thanks for signing up with Pocketbarn!</h4>
	  		<h5>What would you like to do next?</h5>
	  		<div class="modal-flex">
	  		<div><a class="btn btn-purple" @click="$root.showModal = false"><i class="pe-7s-search"></i> Start Browsing</a></div>
	  		<div><a class="btn" href="/store/create" ><i class="pe-7s-shopbag"></i> Start My Marketplace</a></div>
	  		</div>
	  	</div>
	  	<div slot="footer">
        </div>
	  </modal>
</section>




@endsection

@section('footer')


@endsection
