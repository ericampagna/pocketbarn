@extends('layouts.mainlayout')

@section('title', 'mypocket')

@section('content')

<div class="MobileListing">

	<div class="MobileListing--Image">
		<div class="card">
			<div class="card-image">
				<img src="/assets/images/example3.jpg" >
			</div>
		</div>
	</div>

	<div class="MobileListing--Info">
		<h5 class="MobileListing--Title">This is an Item Title</h5>
		<h5 class="MobileListing--Price">$100</h5>
		<div class="MobileListing--More-Info">
			<i class="pe-7s-comment pe-fw" ></i>
		</div>
		<div class="MobileListing--Profile-Pic">
			<img src="/assets/images/example1.jpg" >
		</div>

		<div class="MobileListing--Contact-Info">
			<h6>Store Name</h6>
		</div>
	</div>
</div>

@endsection