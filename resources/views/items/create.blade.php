@extends('layouts.mainlayout')

@section('title', 'New Item')

@section('content')
<section class="container account-page" id="pb-app-item" v-cloak>
	{{-- @include('store.sidebar') --}}

	<alert v-ref:alert-child  v-if="alert.show" :type="alert.type">@{{alert.message}}</alert>
		<div class="Item">
		<div v-if="limitReached">
			<div class="Item--limit-reached">
				<p><i class="pe-7s-attention"></i></p>
				<div>
					<h4>You have reached your item Limit.</h4>
					<h5>To add more items, please <a href="/store/plan">upgrade your account</a></h5>
					<a href="/store/items" class="btn"><i class="pe-7s-angle-left pe-fw"></i> Go back to my items</a>
				</div>
			</div>
		</div>
		<h3>Add a new Item</h3>
			<div class="recomended"><i class="pe-7s-info"></i> We recomend you use images that are at least 1000px wide.<br><br>The first image will be used as your featured image.</div>
			<div class="Item--Images">
				<image-upload></image-upload>
			</div>
			<span v-if="errors.formImage1" class="Form__error">@{{ errors.formImage1 }}</span>
			<section class="Form">
			<h4>Item Description</h4>
				<form method="POST" action="/item/store" class="">

				{{ csrf_field() }}
					<div v-for="image in images">
						<input v-if="image.src != ''" type="hidden"  v-model="form['formImage'+ image.num]" value="@{{ image.src }}">
					</div>

					<div class="Form--input-group">
						<h5>* Item Title</h5>
						<p class="Form--hints">Be descriptive and use keywords the buyers might search for.</p>
						<input class="Form--input" type="text" name="title" v-model="form.title" placeholder="* Item Title">
						<span v-if="errors.title" class="Form__error">@{{ errors.title }}</span>
					</div>
					<div class="Form--input-group">
					<h5>* Item Price</h5>
						<input class="Form--input" type="text" name="price" v-model="form.price" placeholder="* Item Price">
						<span v-if="errors.price" class="Form__error">@{{ errors.price }}</span>
					</div>
					<div class="Form--input-group">
					<h5>* Item Category</h5>
					*<select class="Form--input" type="text" name="category" v-model="form.category">
						<option value="">Select Category</option>
						@foreach($categories as $cat)
							<option value="{{ $cat->id }}">{{ $cat->name }}</option>
						@endforeach
					</select>
					<span v-if="errors.category" class="Form__error">@{{ errors.category }}</span>
				</div>
					<div class="Form--input-group">
						<h5>Quantity</h5>
						<p class="Form--hints">If you keep more than one of these items in stock enter the quantity. Otherwise you can leave this blank.</p>
						<input class="Form--input" type="text" name="quantity" v-model="form.quantity" placeholder=" Quantity">
						<span v-if="errors.quantity" class="Form__error">@{{ errors.quantity }}</span>
					</div>
					<div class="Form--input-group">
						<h5>Available Options...</h5>
						<p class="Form--hints">Choose the options that apply to this item</p>
						<p><input class="Form--input" type="checkbox" name="delivery" v-model="form.delivery">Local Delivery <i class="pb-icon truck"></i></p>
						<span v-if="errors.delivery" class="Form__error">@{{ errors.delivery }}</span>
						<p><input class="Form--input" type="checkbox" name="shipping" v-model="form.shipping">Shipping Available <i style="font-size: 30px;" class="pe-7s-box1 pe-fw"></i></p>
						<span v-if="errors.shipping" class="Form__error">@{{ errors.shipping }}</span>
						<p><input class="Form--input" type="checkbox" name="pickup" v-model="form.pickup">Local Pickup <i class="pb-icon shoe"></i></p>
						<span v-if="errors.pickup" class="Form__error">@{{ errors.pickup }}</span>
						<p><input class="Form--input" type="checkbox" name="hasStore" v-model="form.hasStore">Brick &amp; Mortar Location <i class="pb-icon storefront"></i></p>
						<span v-if="errors.hasStore" class="Form__error">@{{ errors.hasStore }}</span>
					</div>
					<div class="Form--input-group">
						<h5>Contact Button Type</h5>
						<p class="Form--hints">Choose how you would like your customers to contact you if they are interested in this item.</p>
						*<select class="Form--input" type="text" name="contact" v-model="form.contact">
							<option value="">Select type</option>
							@foreach($contact_types as $type)
								<option value="{{ $type }}">{{ $type }}</option>
							@endforeach
						</select>
					</div>
					<span v-if="errors.contact" class="Form__error">@{{ errors.contact }}</span>
					<div v-if="form.contact === 'Buy on Our Website'" class="Form--input-group">
						<h5>Item Url</h5>
						<p class="Form--hints">Enter the url for item's page on your website</p>
						<input class="Form--input Form--input" type="text" name="item_url" v-model="form.item_url" placeholder="http://mywebsite.com/my-item">
						<span v-if="errors.item_url" class="Form__error">@{{ errors.item_url }}</span>
					</div>

					<div class="Form--input-group">
						<h5>* What do you want buyers to know about your item?</h5>
						<p class="Form--hints">Give a brief overview to describe your item.</p>
						<span v-if="errors.description" class="Form__error">@{{ errors.description }}</span>
						<textarea rows="10" cols="200" class="Form--textarea" name="description" v-model="form.description"></textarea>

					</div>
					<div class="Form--input-group">
						<h5>Short Description</h5>
						<p class="Form--hints">This descption will be used in places where space is limited</p>
						<span v-if="errors.short_description" class="Form__error">@{{ errors.short_description }}</span>
						<textarea rows="10" cols="200" class="Form--textarea--small" name="short_description" v-model="form.short_description"></textarea>

					</div>
					<div class="Form--input-group">
						<h5>Tags </h5>
						<p class="Form--hints">Help people discover your items using words or phrases. Seperate tags by commas</p>
						<span v-if="errors.tags" class="Form__error">@{{ errors.tags }}</span>
						<textarea rows="5" cols="100" class="Form--textarea--small" name="tags" v-model="form.tags"> </textarea>

					</div>
					{{-- <div class="Form--input-group last">
						<h5>Item Status</h5>
						<select class="Form--input" type="text" name="status" v-model="form.status">
							<option selected value="" >Choose a Status</option>
							<option value="active">Active</option>
							<option value="in-active">In Active</option>
						</select>
						<span v-if="errors.status" class="Form__error">@{{ errors.status }}</span>
					</div> --}}
					<div class="Form--input-group Form--submit">
					<a class="btn btn-tan Form__cancel"  @click="cancel('/store/items',$event)"><i class="pe-7s-close"></i></a>
					<button class="btn Form__submit" @click="saveItem('active',$event)"><i class="pe-7s-plus"></i>Add Item</button>
					<button class="btn btn-purple Form__submit" @click="saveItem('in-active',$event)"><i class="pe-7s-diskette"></i>Save Draft</button>

				</div>
				</form>
			</section>
		</div>
		<div v-if="submitted">
			<div class="loading_overlay">
				<i class="pe-7s-refresh pe-spin"></i>
			</div>
		</div>
</section>


@endsection

@section ('footer')
@endsection