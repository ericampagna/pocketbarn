$(document).ready( function() {

	//Show card-over on hover woth cards
	//Click toggle replacement
  $.fn.clickToggle = function(a, b) {
      return this.each(function() {
          var clicked = false;
          $(this).click(function() {
              if (clicked) {
                  clicked = false;
                  return b.apply(this, arguments);
              }
              clicked = true;
              return a.apply(this, arguments);
          });
      });
  };
  // var vh = document.body.clientHeight;
  // var vw = document.body.clientWidth;

    $(document).ready( function() {

        // Cards Flip for mobile
        $('.Card').clickToggle(
          function() {
            $(this).find('.card-over').addClass('active');
          },function() {
            $(this).find('.card-over').removeClass('active');
          });

        // Card Flip for Desktop
        $('.Card').hoverIntent(
          function() {
            $(this).find('.card-over').addClass('active');
          },function() {
            $(this).find('.card-over').removeClass('active');
          });
    }); //Close doc.ready

}); //oocument.ready close