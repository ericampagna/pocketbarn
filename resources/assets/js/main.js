// browserify entrypoint

var Vue = require('vue');
var Resource = require('vue-resource');


import Alert from './components/Alert.vue';
import Cards from './components/Cards.vue';
//import SearchMap from './components/SearchMap.vue';
import Search from './components/Search.vue';
import Slider from './components/Slider.vue';
import MoreItems from './components/MoreItems.vue';
import ImageUpload from './components/ImageUpload.vue';
import SingleImageUpload from './components/SingleImageUpload.vue';
import FormError from './components/FormError.vue';
import Modal from './components/Modal.vue';
import Conversations from './components/Conversations.vue';
import Messages from './components/Messages.vue';
import HomeVideo from './components/HomeVideo.vue';
import Notification from './components/Notification.vue';

Vue.use(Resource);
//Submit the csrf token with every AJAX request
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
Vue.url.options.root = "https://www.mypocketbarn.com/"

//Directive send form though ajax instead of page refresh
Vue.directive('ajax', {
	//All paramaters this directive
	params: ['complete'],

	bind() {
		//Listen for the Form to be submitted,then pass along to the onFormSubission function
		this.el.addEventListener('submit', this.onFormSubmission.bind(this));
	},
	update() {

	},
	onFormSubmission(e) {
		 //Stop Submiting form
		e.preventDefault();
		//Get the request type of the form, ie. GET, POST, DELETE... also get the form action
		this.vm
			.$http[this.getRequestType()](this.el.action)
			.then(this.onComplete.bind(this))
			.catch(this.onError.bind(this))
		;
	},
	onComplete() {
			if (this.params.complete) {
				alert(this.params.complete)
			}
	},

	onError(response) {
		console.log(response);
	},

	getRequestType() {
		//Get submit method type from method input
		var method = this.el.querySelector('input[name="_method"]');
		//Check method to see id mehtod input exsited, if not get method from the form
		return (method ? method.value : this.el.method).toLowerCase();
	},

});
if(typeof store === 'undefined')
{
	var store = ''
}
if(typeof itemName === 'undefined')
{
	var itemName = ''
}
if(typeof limitReached === 'undefined')
{
	var limitReached = ''
}
if(typeof showModal === 'undefined')
{
	var showModal = false
}
// var store = {
// 	user: {
// 		lat: '',
// 		lng: ''
// 	},
// };
// Vue.transition('fade', {
// 	enterClass: 'fadeInLeft',
// 	leaveClass: 'fadeOutRight'
// })
Vue.transition('slideUp', {
	enterClass: 'fadeInUpBig',
	leaveClass: 'fadeOutDownBig'
})
Vue.transition('modal', {
	enterClass: 'slideInDown',
	leaveClass: 'slideOutUp'
})
//Check for element
if(document.getElementById('pb-app-header')) {
new Vue({
	el: '#pb-app-header',

	components: {Notification},
	data: {

	},

	methods: {

	}


}) 
}// End if

//Check for element
if(document.getElementById('pb-app-issue')) {
new Vue({
	el: '#pb-app-issue',

	components: {Modal},
	data: {
		issueModal: false,
		issue: {
			date: new Date,
			page: window.location.href,
			user_agent: navigator.userAgent,
			type: '',
			description: '',
			stacktrace: '',
		}
	},

	methods: {
		submitIssue() {
			this.issueModal = false;
			this.issue.description = this.issue.description + "\n\n\n StackTrace" + this.issue.stacktrace;
			this.$http.post('/api/v1/issues/new', this.issue).then((response) => {
				this.issue.type = '';
				this.issue.description = '';
			}, (response) => {

			});
		}
	}


})
} // End if

if(document.getElementById('pb-app-profile')) {
new Vue({
	el: '#pb-app-profile',

	components: {Modal, Alert, SingleImageUpload},
	data: {
		cropModal: false,
		images: {
            img1:{num: 1, src: ''},

        },
        form: {
        	name: '',
        	email: '',
        	location: '',
        	gender: '',
        	avatar: '',

        },
        imageSaved: true,
        submitted: false,
        errors: [],
        alert: {
			show: false,
			type: '',
			message: '',
		},
	},

	methods: {
	}


})
} // End if

//Check for element
if(document.getElementById('pb-app')) {
new Vue({
	el: '#pb-app',

	components: { Alert, Cards, Search, Slider, MoreItems, Modal, Notification},
	data: {
		showModal: window.showModal,
		sellerEmail: '',
		messageToSeller: '',
		store: window.store,
		itemName: window.itemName,
		plan: 'free_new',
		iconMeanings: false,
		alert: {
			show: false,
			type: '',
			message: '',
		},
	},

	methods: {
		sendMessage() {
			if(!this.messageToSeller)
			{
					this.alert.message = 'No Message Entered';
					this.alert.type = 'error';
					if(this.$refs.alertChild) this.$refs.alertChild.show = true;
					this.alert.show = true;
			}
			else
			{
				this.showModal = false;

				 var message_data = {
				 	store: this.store,
				 	content: 'Item: <b>' + this.itemName +'</b><br>'+this.messageToSeller
				}
				this.$http.post('/api/v1/messages/new', message_data).then((response) => {
						this.alert.message = response.body;
						this.alert.type = 'success';
						if(this.$refs.alertChild) this.$refs.alertChild.show = true;
						this.alert.show = true;
						this.messageToSeller = '';

					}, (response) => {
						this.alert.message = response.body;
						this.alert.type = 'error';
						if(this.$refs.alertChild) this.$refs.alertChild.show = true;
						this.alert.show = true;
					});
			}
		},
		addToPocket(type, id)
		{
			this.$http.get('/api/v1/mypocket/addToPocket/'+type+ '/'+id).then((response) =>{
					this.alert.message = 'Item has been added to your pocket';
					this.alert.type = 'success';
					this.alert.show = true;
					$("#mypocket").removeClass('bounce').addClass('bounce');
				} ,(response) =>  {
					this.alert.message = response.body;
					this.alert.type = 'error';
					this.alert.show = true;
				});
		},
		goBack() {
			window.history.back();
		},
		createStore() {
			Stripe.setPublishableKey(window.stripeKey);

		            var $form = $('#marketplace-form');
		            $form.find('button').prop('disabled', true);
		            if(this.plan != 'free_new')
		            {
		            	Stripe.card.createToken($form, function(status, response) {
			            	if (response.error) {
					            $form.find('.payment-errors').text(response.error.message).addClass('flash');
					            $form.find('button').prop('disabled', false);
					        } else {
					            var token = response.id;
					            $form.append($('<input type="hidden" name="stripeToken" />').val(token));
					            $form.get(0).submit();
					        }
		            	});
		            }
		            else
		            {
		            	$form.get(0).submit();
		            }

		},
	}


})
} // End if

//Check for element
if(document.getElementById('pb-app-store')) {
new Vue({
	el: '#pb-app-store',
	components: {Alert, Modal, Notification, SingleImageUpload},
	data: {
		store: window.store,
		errors: '',
		submitted: false,
		showModal: false,
		sellerEmail: '',
		alert: {
			show: false,
			type: '',
			message: '',
		},
		item: '',
		days: 1,
		imageSaved: true,
		boostBtn: {
			default: '<i class="pe-7s-star"></i> Boost Item',
			loading: '<i class="pe-7s-refresh pe-spin"></i> Saving...',
			button: '<i class="pe-7s-star"></i> Boost Item'
		},
	},

	methods: {
		makeOwner() {
			alert('make owner');
		},
		addSeller() {
			if(!this.sellerEmail)
			{

					this.alert.message = 'No Email Entered';
					this.alert.type = 'error';
					if(this.$refs.alertChild) this.$refs.alertChild.show = true;
					this.alert.show = true;
			}
			else
			{
				this.showModal = false;
				this.$http.get('/api/v1/seller/invite/'+this.sellerEmail).then((response) => {
						this.alert.message = response.body;
						this.alert.type = 'success';
						if(this.$refs.alertChild) this.$refs.alertChild.show = true;
						this.alert.show = true;

				}, (response) => {
						this.alert.message = response.body;
						this.alert.type = 'error';
						if(this.$refs.alertChild) this.$refs.alertChild.show = true;
						this.alert.show = true;

				});
			}
		},
		removeSeller(id){
			this.submitted = true;
			this.$http.get('/api/v1/seller/remove/'+id).then((response) => {
				window.location.reload();
			}, (response) => {
				this.submitted = false;
				this.alert.message = response.body;
				this.alert.type = 'error';
				//this.$refs.alertChild.show = true;
				this.alert.show = true;
			});
		},
		boostItemModal($item)
		{
			this.item = $item;
			this.showModal = true;

		},
		boostItem(){
			if(this.submitted == true)
			{
				return;
			}

			this.boostBtn.button = this.boostBtn.loading;
			this.alert.show = false;
			this.submitted = true;

			if(this.days <= 0)
			{
				this.alert.message = 'Have have enter an invaild amount of days';
				this.alert.show = true;
				this.boostBtn.button = this.boostBtn.default;
				this.submitted = false;
				return;
			}

			var data = {
				item: this.item.id,
				days: this.days,
			}
			this.$http.put('/api/v1/item/boost/', data).then((response) => {
				window.location = '/store/boosted-items';
			}, (response) => {
				this.alert.message = response.body;
				this.alert.show = true;
				this.boostBtn.button = this.boostBtn.default;
				this.submitted = false;
			});


		},
		cancelModal(e) {
			e.preventDefault;
			this.showModal = true;
		},
		deleteMarketplace() {
			this.$http.delete('/store/' + this.store).then((response) => {
				window.location = '/my-profile'
			}, (response) => {

			});
		},
		goToFree() {
			var data = {
				plan: 'free'
			}
			this.$http.patch('/store/plan/' + this.store, data).then((response) => {
				window.location = '/store/items'
			}, (response) => {

			});
		}
	},
	computed: {
		totalCost: function() {
			return this.days * 3;
		}
	}


})
} // End if

//Check for element
if(document.getElementById('pb-app-item')) {
new Vue({
	el: '#pb-app-item',

	components: {ImageUpload, FormError, Alert, Modal},
	data: {
		limitReached: limitReached,
		item_id: '',
		images: {
            img1:{num: 1, src: ''},
            img2:{num: 2, src: ''},
            img3:{num: 3, src: ''},
        },
        form: {
        	title : '',
        	price: '',
        	category: '',
        	quantity: '',
        	contact: '',
        	item_url: '',
        	description: '',
        	short_description: '',
        	tags: '',
        	formImage1: '',
        	formImage2: '',
        	formImage3: '',
        	formImage4: '',
        	formImage5: '',
        	status: '',
        	delivery: '',
        	shipping: '',
        	pickup: '',
        	hasStore: ''
        },
        submitted: false,
        errors: [],
        alert: {
			show: false,
			type: '',
			message: '',
		},
		showDeleteModal: false,
	},

	methods: {
		cancel(location, e){
			e.preventDefault();
			var r = confirm('Are you sure? If you cancel you will lose any changes you have made.');
			if(r){
				window.location = location;
			}
		},
		saveItem(status, e) {
			e.preventDefault();
			this.submitted = true;
			this.form.status = status;
			var form = this.form;

			this.$http.post('/item', form).then((response) => {

			window.location = '/store/items'
			}, (response) => {
				this.submitted = false;
				this.$set('errors', response.body)
				this.alert.message = JSON.stringify(this.errors);
				this.alert.type = 'error';
				if(this.$refs.alertChild) this.$refs.alertChild.show = true;
				this.alert.show = true;
			});

		},
		updateItem(status, e) {
			e.preventDefault();
			this.submitted = true;
			this.form.status = status;
			var form = this.form;

			this.$http.patch('/item/'+ this.item_id, form).then((response) => {
				window.location = '/store/items'
			}, (response) => {
				this.$set('errors', response.body);
				this.submitted = false;
				this.alert.message = response.body;
				this.alert.type = 'error';
				if(this.$refs.alertChild) this.$refs.alertChild.show = true;
				this.alert.show = true;
			});

		},
		deleteModal(e) {
			e.preventDefault();
			this.showDeleteModal = true;
		},
		deleteItem() {
			this.$http.delete('/item/'+ this.item_id).then((response) => {
				window.location = '/store/items'
			}, (response) => {
				this.$set('errors', response.body);
				this.submitted = false;
				this.alert.message = response.body;
				this.alert.type = 'error';
				if(this.$refs.alertChild) this.$refs.alertChild.show = true;
				this.alert.show = true;
			});
		}
	}


})
} // End if

//Check for element
if(document.getElementById('pb-app-conversations')) {
new Vue({
	el: '#pb-app-conversations',

	components: { Alert, Modal, Conversations, Messages},
	data: {
		showModal: false,
		message: '',
		store: '',
		userView: true,
		storeView: false,
		userUnread: 0,
		storeUnread: 0,
		reload: false,
		queuedConversation: '',
		alert: {
			show: false,
			type: '',
			message: '',
		},
	},
	watch: {
		reload: function(){
			if(this.reload)
			{
				this.getUnreadCount();
				this.reload = false;
			}
		}
	},
	created() {
		this.getUnreadCount();
	},
	ready() {
		var vm = this;
		setInterval( function() {

			vm.getUnreadCount()
		}, 60000);
	},
	methods: {
		switchView()
		{
			if(this.storeView)
			{
				this.storeView = false;
				this.userView = true;
			}
			else if(this.userView)
			{
				this.storeView = true;
				this.userView = false;
			}

		},
		getUnreadCount()
		{
			this.$http.get('/api/v1/conversations/getUnreadCount').then((response) =>{
					this.userUnread = response.body.userUnread;
					this.storeUnread = response.body.storeUnread;

				} ,(response) =>  {

				});
		},


	}


})
} // End if

//Check for element
if(document.getElementById('pb-app-pocket')) {
new Vue({
	el: '#pb-app-pocket',

	components: { Alert, Modal},
	data: {
		pocket: {
			items: [],
			stores: []
		},
		noItems: false,
		noStores: false,
		showModal: false,
		alert: {
			show: false,
			type: '',
			message: '',
		},
		view: 'items',
	},
	watch: {

	},
	created() {
		this.fetchPocket();
	},

	methods: {

		fetchPocket()
		{
			this.$http.get('/api/v1/mypocket/getPocket').then((response) =>{
					if(response.body.items)
					{
						this.pocket.items = response.body.items;
						this.noItems = false;
					}
					else
					{
						this.pocket.items = [];
						this.noItems = true;
					}
					if(response.body.stores)
					{
						this.pocket.stores = response.body.stores;
						this.noStores = false;
					}
					else
					{
						this.pocket.stores = '';
						this.noStores = true;
					}

				} ,(response) =>  {
				});
		},
		switchView(view)
		{
			this.view = view;
		},
		goToItem(storeSlug, itemId, itemSlug)
		{
			window.location = '/item/' + storeSlug + '/' + itemId + '/' + itemSlug;;
		},
		goToStore(storeSlug, storeId)
		{
			window.location = '/marketplace/' + storeId + '/' + storeSlug;
		},
		removeFromPocket(type, id)
		{
			this.$http.get('/api/v1/mypocket/removeFromPocket/'+type+ '/'+id).then((response) =>{
					this.fetchPocket();

				} ,(response) =>  {

				});
		},


	}


})
} // End if

//Check for element
if(document.getElementById('pb-app-support')) {
new Vue({
	el: '#pb-app-support',

	components: {Modal, Alert},
	data: {
		issue: {
			date: new Date,
			page: '',
			user_agent: navigator.userAgent,
			type: '',
			description: '',
		},
		pageView: 'faq',
		alert: {
			show: false,
			type: '',
			message: '',
		},
	},

	methods: {
		submitIssue() {
			this.issueModal = false;
			this.$http.post('/api/v1/issues/new', this.issue).then((response) => {
				this.issue.type = '';
				this.issue.description = '';
				this.issue.page = '';
				this.alert.message = 'Thank you for submitting your issue. Your feedback with help to make Pocketbarn better!';
				this.alert.show = true;
			}, (response) => {

			});
		}
	}


})
}//End if
/*
***********************************
*Non Vue related
************************************
*/
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[id=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

$(function(){
    var ink, d, x, y;
    $(".menu-wrapper a").click(function(e){
        if($(this).find(".ink_menu").length === 0){
            $(this).prepend("<span class='ink_menu'></span>");
        }

        ink = $(this).find(".ink_menu");
        ink.removeClass("animate");

        if(!ink.height() && !ink.width()){
            d = Math.max($(this).outerWidth(), $(this).outerHeight());
            ink.css({height: d, width: d});
        }

        x = e.pageX - $(this).offset().left - ink.width()/2;
        y = e.pageY - $(this).offset().top - ink.height()/2;

        ink.css({top: y+'px', left: x+'px'}).addClass("animate");
     });
});
$(function(){
    var ink, d, x, y;
    $(".btn").click(function(e){
        if($(this).find(".ink").length === 0){
            $(this).prepend("<span class='ink'></span>");
        }

        ink = $(this).find(".ink");
        ink.removeClass("animate");

        if(!ink.height() && !ink.width()){
            d = Math.max($(this).outerWidth(), $(this).outerHeight());
            ink.css({height: d, width: d});
        }

        x = e.pageX - $(this).offset().left - ink.width()/2;
        y = e.pageY - $(this).offset().top - ink.height()/2;

        ink.css({top: y+'px', left: x+'px'}).addClass("animate");
     });
});
$(function(){
    var ink, d, x, y;
    $(".Card__over span").click(function(e){
        if($(this).find(".ink").length === 0){
            $(this).prepend("<span class='ink'></span>");
        }

        ink = $(this).find(".ink");
        ink.removeClass("animate");

        if(!ink.height() && !ink.width()){
            d = Math.max($(this).outerWidth(), $(this).outerHeight());
            ink.css({height: d, width: d});
        }

        x = e.pageX - $(this).offset().left - ink.width()/2;
        y = e.pageY - $(this).offset().top - ink.height()/2;

        ink.css({top: y+'px', left: x+'px'}).addClass("animate");
     });
});
//Homepage Video Controller
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		var video = $('#homepageVideo');
		if(video.length > 0){
			console.log(video);
			var vid = video[0];
		 	vid.pause();
		 	if (vid.exitFullScreen) {
			  vid.exitFullScreen();
			} else if (vid.mozCancelFullscreen) {
			  vid.mozCancelFullscreen();
			} else if (vid.webkitExitFullscreen) {
			  vid.webkitExitFullscreen();
			}
		}
		
	}
$(document).ready( function() {

	var video = $('#homepageVideo');

	$('#playFullScreen').on('click', function(event) {
	  	event.preventDefault();
	  	var vid = video[0];
	  	vid.play();
	  	if(vid.muted == true){
	  		vid.muted = false;
	  		vid.currentTime = 0;

	  	}
		if (vid.requestFullscreen) {
		  vid.requestFullscreen();
		} else if (vid.mozRequestFullScreen) {
		  vid.mozRequestFullScreen();
		} else if (vid.webkitRequestFullscreen) {
		  vid.webkitRequestFullscreen();
		}
	});
	$('#muteVideo').on('click', function(event) {
		event.preventDefault();
		var vid = video[0];
		if(vid.muted == true)
		{
			vid.muted = false;
			$('#muteVideo > i').removeClass('pe-7s-volume2').addClass('pe-7s-volume1');
		}
		else{
			vid.muted = true;
			$('#muteVideo > i').removeClass('pe-7s-volume1').addClass('pe-7s-volume2');
		}
	})

	video.on('ended', function () {
		var vid = video[0];
		if (vid.exitFullScreen) {
		  vid.exitFullScreen();
		} else if (vid.mozCancelFullscreen) {
		  vid.mozCancelFullscreen();
		} else if (Document.exitFullScreen) {
		  Document.exitFullScreen();
		}
	});
});
var InputMask = function ( opts ) {
  if ( opts && opts.masked ) {
    // Make it easy to wrap this plugin and pass elements instead of a selector
    opts.masked = typeof opts.masked === string ? document.querySelectorAll( opts.masked ) : opts.masked;
  }

  if ( opts ) {
    this.opts = {
      masked: opts.masked || document.querySelectorAll( this.d.masked ),
      mNum: opts.mNum || this.d.mNum,
      mChar: opts.mChar || this.d.mChar,
      error: opts.onError || this.d.onError
    }
  } else {
    this.opts = this.d;
    this.opts.masked = document.querySelectorAll( this.opts.masked );
  }

  this.refresh( true );
};

var inputMask = {

  // Default Values
  d: {
    masked : '.masked',
    mNum : 'XdDmMyY9',
    mChar : '_',
    onError: function(){}
  },

  refresh: function(init) {
    var t, parentClass;

    if ( !init ) {
      this.opts.masked = document.querySelectorAll( this.opts.masked );
    }

    for(i = 0; i < this.opts.masked.length; i++) {
      t = this.opts.masked[i]
      parentClass = t.parentNode.getAttribute('class');

      if ( !parentClass || ( parentClass && parentClass.indexOf('shell') === -1 ) ) {
        this.createShell(t);
        this.activateMasking(t);
      }
    }
  },

  // replaces each masked t with a shall containing the t and it's mask.
  createShell : function (t) {
    var wrap = document.createElement('span'),
        mask = document.createElement('span'),
        emphasis = document.createElement('i'),
        tClass = t.getAttribute('class'),
        pTxt = t.getAttribute('placeholder'),
        placeholder = document.createTextNode(pTxt);

    t.setAttribute('maxlength', placeholder.length);
    t.setAttribute('data-placeholder', pTxt);
    t.removeAttribute('placeholder');


    if ( !tClass || ( tClass && tClass.indexOf('masked') === -1 ) ) {
      t.setAttribute( 'class', tClass + ' masked');
    }

    mask.setAttribute('aria-hidden', 'true');
    mask.setAttribute('id', t.getAttribute('id') + 'Mask');
    mask.appendChild(emphasis);
    mask.appendChild(placeholder);

    wrap.setAttribute('class', 'shell');
    wrap.appendChild(mask);
    t.parentNode.insertBefore( wrap, t );
    wrap.appendChild(t);
  },

  setValueOfMask : function (e) {
    var value = e.target.value,
        placeholder = e.target.getAttribute('data-placeholder');

    return "<i>" + value + "</i>" + placeholder.substr(value.length);
  },

  // add event listeners
  activateMasking : function (t) {
    var that = this;
    if (t.addEventListener) { // remove "if" after death of IE 8
      t.addEventListener('keyup', function(e) {
        that.handleValueChange.call(that,e);
      }, false);
    } else if (t.attachEvent) { // For IE 8
        t.attachEvent('onkeyup', function(e) {
        e.target = e.srcElement;
        that.handleValueChange.call(that, e);
      });
    }
  },

  handleValueChange : function (e) {
    var id = e.target.getAttribute('id');

    if(e.target.value == document.querySelector('#' + id + 'Mask i').innerHTML) {
      return; // Continue only if value hasn't changed
    }

    document.getElementById(id).value = this.handleCurrentValue(e);
    document.getElementById(id + 'Mask').innerHTML = this.setValueOfMask(e);

  },

  handleCurrentValue : function (e) {
    var isCharsetPresent = e.target.getAttribute('data-charset'),
        placeholder = isCharsetPresent || e.target.getAttribute('data-placeholder'),
        value = e.target.value, l = placeholder.length, newValue = '',
        i, j, isInt, isLetter, strippedValue;

    // strip special characters
    strippedValue = isCharsetPresent ? value.replace(/\W/g, "") : value.replace(/\D/g, "");

    for (i = 0, j = 0; i < l; i++) {
        isInt = !isNaN(parseInt(strippedValue[j]));
        isLetter = strippedValue[j] ? strippedValue[j].match(/[A-Z]/i) : false;
        matchesNumber = this.opts.mNum.indexOf(placeholder[i]) >= 0;
        matchesLetter = this.opts.mChar.indexOf(placeholder[i]) >= 0;
        if ((matchesNumber && isInt) || (isCharsetPresent && matchesLetter && isLetter)) {
                newValue += strippedValue[j++];
          } else if ((!isCharsetPresent && !isInt && matchesNumber) || (isCharsetPresent && ((matchesLetter && !isLetter) || (matchesNumber && !isInt)))) {
                //this.opts.onError( e ); // write your own error handling function
                return newValue;
        } else {
            newValue += placeholder[i];
        }
        // break if no characters left and the pattern is non-special character
        if (strippedValue[j] == undefined) {
          break;
        }
    }
    if (e.target.getAttribute('data-valid-example')) {
      return this.validateProgress(e, newValue);
    }
    return newValue;
  },

  validateProgress : function (e, value) {
    var validExample = e.target.getAttribute('data-valid-example'),
        pattern = new RegExp(e.target.getAttribute('pattern')),
        placeholder = e.target.getAttribute('data-placeholder'),
        l = value.length, testValue = '';

    //convert to months
    if (l == 1 && placeholder.toUpperCase().substr(0,2) == 'MM') {
      if(value > 1 && value < 10) {
        value = '0' + value;
      }
      return value;
    }
    // test the value, removing the last character, until what you have is a submatch
    for ( i = l; i >= 0; i--) {
      testValue = value + validExample.substr(value.length);
      if (pattern.test(testValue)) {
        return value;
      } else {
        value = value.substr(0, value.length-1);
      }
    }

    return value;
  }
};

for ( var property in inputMask ) {
  if (inputMask.hasOwnProperty(property)) {
    InputMask.prototype[ property ] = inputMask[ property ];
  }
}

//  Declaritive initalization
(function(){
  var scripts = document.getElementsByTagName('script'),
      script = scripts[ scripts.length - 1 ];
  if ( script.getAttribute('data-autoinit') ) {
    new InputMask();
  }
})();





