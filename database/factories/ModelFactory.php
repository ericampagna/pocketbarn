<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(pocketbarn\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
        'avatar' => $faker->imageUrl(300,300,'cats'),
        'store_id' => $faker->numberBetween(1, 48),
    ];
});

$factory->define(pocketbarn\Item::class, function (Faker\Generator $faker) {
      return [
        'title' => $faker->name,
        'short_desc' => $faker->paragraph(4),
        'description' => $faker->paragraph(10),
        'store_id' => $faker->numberBetween(1, 48),
        'category_id' => $faker->numberBetween(1, 6)
      ];

});

$factory->define(pocketbarn\Store::class, function (Faker\Generator $faker) {
	return [
    	'name' => $faker->company(2),
      	'story' => $faker->paragraph(10),
      	'policies' => $faker->paragraph(10),
    	'phone' => $faker->phoneNumber(),
    	'street' => $faker->streetAddress(),
    	'city' => 'Mount Carmel',
    	'state' => 'IL',
   		'zip' => '62863',
    	'country' => 'US',
  	 	'lat' => $faker->latitude(38, 39),
   		'lng' => $faker->longitude(-88, -87),
    	'profile_img' => 'http://fakeimg.pl/300/?text=Profile',
    	'cover_img' => 'http://fakeimg.pl/1200x400/?text=Cover_Photo',
	];
});

$factory->define(pocketbarn\Image::class, function (Faker\Generator $faker) {
      return [
        'title' => $faker->name,
        'file' => 'http://fakeimg.pl/1200x400/?text='. $faker->word(),
        'caption' => $faker->paragraph(4),
        'is_featured' => 'false',
        'item_id' => $faker->numberBetween(1, 96)
      ];

});