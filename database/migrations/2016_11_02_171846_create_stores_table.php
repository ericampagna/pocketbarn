<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('name');
            $table->longText('story');
            $table->longText('policies');
            $table->string('phone');
            $table->string('street');
            $table->string('city');
            $table->string('state');
            $table->integer('zip');
            $table->string('country');
            $table->float('lat');
            $table->float('lng');
            $table->string('profile_img');
            $table->string('cover_img');
            $table->integer('owned_by');
            $table->integer('show_address');
            $table->string('website');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stores');
    }
}
